-- phpMyAdmin SQL Dump
-- version 3.5.7
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Mer 04 Juin 2014 à 18:28
-- Version du serveur: 5.5.29
-- Version de PHP: 5.4.10

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+01:00";

--
-- Base de données: `umanteam_terreplurielle`
--

-- --------------------------------------------------------

--
-- Structure de la table `acl_classes`
--

CREATE TABLE `acl_classes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class_type` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_69DD750638A36066` (`class_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `acl_entries`
--

CREATE TABLE `acl_entries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class_id` int(10) unsigned NOT NULL,
  `object_identity_id` int(10) unsigned DEFAULT NULL,
  `security_identity_id` int(10) unsigned NOT NULL,
  `field_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ace_order` smallint(5) unsigned NOT NULL,
  `mask` int(11) NOT NULL,
  `granting` tinyint(1) NOT NULL,
  `granting_strategy` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `audit_success` tinyint(1) NOT NULL,
  `audit_failure` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_46C8B806EA000B103D9AB4A64DEF17BCE4289BF4` (`class_id`,`object_identity_id`,`field_name`,`ace_order`),
  KEY `IDX_46C8B806EA000B103D9AB4A6DF9183C9` (`class_id`,`object_identity_id`,`security_identity_id`),
  KEY `IDX_46C8B806EA000B10` (`class_id`),
  KEY `IDX_46C8B8063D9AB4A6` (`object_identity_id`),
  KEY `IDX_46C8B806DF9183C9` (`security_identity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `acl_object_identities`
--

CREATE TABLE `acl_object_identities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_object_identity_id` int(10) unsigned DEFAULT NULL,
  `class_id` int(10) unsigned NOT NULL,
  `object_identifier` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `entries_inheriting` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_9407E5494B12AD6EA000B10` (`object_identifier`,`class_id`),
  KEY `IDX_9407E54977FA751A` (`parent_object_identity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `acl_object_identity_ancestors`
--

CREATE TABLE `acl_object_identity_ancestors` (
  `object_identity_id` int(10) unsigned NOT NULL,
  `ancestor_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`object_identity_id`,`ancestor_id`),
  KEY `IDX_825DE2993D9AB4A6` (`object_identity_id`),
  KEY `IDX_825DE299C671CEA1` (`ancestor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `acl_security_identities`
--

CREATE TABLE `acl_security_identities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `username` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8835EE78772E836AF85E0677` (`identifier`,`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `Activite`
--

CREATE TABLE `Activite` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `DateDebut` datetime NOT NULL,
  `DateFin` datetime NOT NULL,
  `Statut` tinyint(1) NOT NULL,
  `RefPictogramme` int(11) DEFAULT NULL,
  `Titre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_410337431ED6FEAE` (`RefPictogramme`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=46 ;

--
-- Contenu de la table `Activite`
--

INSERT INTO `Activite` (`id`, `DateDebut`, `DateFin`, `Statut`, `RefPictogramme`, `Titre`) VALUES
(3, '2014-03-13 00:00:00', '2014-03-14 00:00:00', 0, 1, 'test activite'),
(4, '2014-03-14 00:00:00', '2014-03-19 00:00:00', 0, 2, '2nd activite'),
(5, '2014-03-10 00:00:00', '2014-04-14 00:00:00', 0, 4, 'PersO!'),
(10, '2014-03-24 00:00:00', '2014-03-24 00:00:00', 0, 18, 'another'),
(16, '2014-03-24 00:00:00', '2014-03-24 00:00:00', 0, 1, 'boire de l''eau'),
(17, '2014-03-25 00:00:00', '2014-03-25 00:00:00', 0, 1, 'boire de l''eau'),
(18, '2014-03-25 00:00:00', '2014-03-27 00:00:00', 1, 1, 'separate act'),
(19, '2014-03-25 00:00:00', '2014-03-25 00:00:00', 0, 1, 'boire de l''eau'),
(20, '2009-01-01 00:00:00', '2009-01-01 00:00:00', 0, 7, 'boire du jus d''orange'),
(21, '2014-04-04 00:00:00', '2014-04-04 00:00:00', 0, 1, 'boire du jus d''orange'),
(22, '2014-03-31 00:00:00', '2014-04-04 00:00:00', 0, 37, 'act Avril 0'),
(23, '2014-01-01 00:00:00', '2014-01-07 00:00:00', 0, 2, 'aaa'),
(24, '2014-01-01 00:00:00', '2014-01-01 00:00:00', 1, 34, 'ecouter une histoire'),
(25, '2014-04-07 00:00:00', '2014-04-07 00:00:00', 0, 32, 'faire un gateau'),
(28, '2014-04-23 15:00:00', '2014-04-23 16:30:00', 0, 8, 'dessin'),
(29, '2014-04-23 18:00:00', '2014-04-23 19:00:00', 1, 22, 'aller'),
(31, '2014-01-01 08:00:00', '2014-01-01 09:00:00', 0, 8, 'Dessin'),
(33, '2014-05-27 11:45:00', '2014-05-27 11:55:00', 0, 1, 'boire de l''eau'),
(34, '2014-05-27 12:00:00', '2014-05-27 13:00:00', 0, 60, 'déjeuner'),
(35, '2014-05-27 09:30:00', '2014-05-27 10:55:00', 0, 35, 'regarder un film'),
(36, '2014-05-27 16:15:00', '2014-05-27 16:30:00', 1, 33, 'soda time'),
(37, '2014-08-20 08:30:00', '2014-08-20 09:00:00', 0, 15, 'écoute'),
(38, '2014-05-13 08:30:00', '2014-05-13 09:30:00', 0, 15, 'écoute'),
(39, '2014-05-13 09:35:00', '2014-05-13 10:05:00', 0, 8, 'Dessin'),
(40, '2014-05-13 08:30:00', '2014-05-13 09:00:00', 0, 15, 'écoute'),
(41, '2014-05-13 09:00:00', '2014-05-13 10:00:00', 0, 8, 'Dessin'),
(42, '2014-05-14 08:03:00', '2014-05-14 09:30:00', 0, 8, 'Dessin'),
(43, '2014-05-27 14:00:00', '2014-05-27 16:00:00', 0, 113, 'Aller au parc'),
(45, '2014-06-04 14:00:00', '2014-06-04 15:00:00', 0, 33, 'soda yo');

-- --------------------------------------------------------

--
-- Structure de la table `Activite_Enfant`
--

CREATE TABLE `Activite_Enfant` (
  `activite_id` int(11) NOT NULL,
  `enfant_id` int(11) NOT NULL,
  PRIMARY KEY (`activite_id`,`enfant_id`),
  KEY `IDX_C2BCF36E9B0F88B1` (`activite_id`),
  KEY `IDX_C2BCF36E450D2529` (`enfant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `Activite_Enfant`
--

INSERT INTO `Activite_Enfant` (`activite_id`, `enfant_id`) VALUES
(3, 4),
(4, 5),
(5, 4),
(10, 9),
(16, 3),
(17, 3),
(18, 5),
(19, 10),
(20, 3),
(21, 3),
(22, 5),
(28, 5),
(29, 5),
(33, 5),
(34, 5),
(35, 5),
(36, 5),
(43, 5),
(45, 5);

-- --------------------------------------------------------

--
-- Structure de la table `educateur`
--

CREATE TABLE `educateur` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Prenom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `RefEnfant` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `Enfant`
--

CREATE TABLE `Enfant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Prenom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `DateDeNaissance` datetime NOT NULL,
  `Observation` longtext COLLATE utf8_unicode_ci,
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ref_niveau` int(11) DEFAULT NULL,
  `ref_parent` int(11) DEFAULT NULL,
  `ref_educateur` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_331B09943EC69B92` (`ref_niveau`),
  KEY `ref_niveau` (`ref_niveau`),
  KEY `IDX_331B09945660067` (`ref_educateur`),
  KEY `ref_educateur` (`ref_educateur`),
  KEY `ref_niveau_2` (`ref_niveau`),
  KEY `ref_parent` (`ref_parent`),
  KEY `IDX_331B0994489708B6` (`ref_parent`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=22 ;

--
-- Contenu de la table `Enfant`
--

INSERT INTO `Enfant` (`id`, `Nom`, `Prenom`, `DateDeNaissance`, `Observation`, `avatar`, `ref_niveau`, `ref_parent`, `ref_educateur`) VALUES
(1, 'Ammy', 'Damone', '2009-04-15 00:00:00', 'Lorem ipsum dolor sit amet, consectetur', '', 1, NULL, 1),
(2, 'Amy', 'Hanouna', '2009-11-16 00:00:00', 'Lorem ipsum dolor sit amet, consectetur', '', NULL, NULL, 2),
(3, 'toto', 'titi', '2009-12-08 00:00:00', NULL, '', 3, 4, 3),
(4, 'testy', 'testa', '2009-05-01 00:00:00', NULL, 'kangaroo_lay1.jpg', 4, 4, 5),
(5, 'aaa', 'bbb', '2009-09-02 00:00:00', NULL, '170001269801.png', 3, 4, 5),
(9, 'ta', 'ter', '2009-02-02 00:00:00', NULL, NULL, 1, NULL, 5),
(10, 'dupont', 'jean', '2009-10-01 00:00:00', NULL, NULL, 3, 4, 3),
(11, 'child', 'son', '2009-10-01 00:00:00', NULL, NULL, 1, 11, NULL),
(12, 'BENKHERRAT', 'ADAM', '2009-01-30 00:00:00', NULL, 'Adam.JPG', 3, 4, 12),
(13, 'ridene', 'rabaa', '2009-03-06 00:00:00', NULL, '41.jpg', 2, 4, 13),
(14, 'e', 'f', '2014-01-01 00:00:00', NULL, NULL, 1, 4, 5),
(16, 'MARTIN', 'Léonie', '2010-12-22 00:00:00', NULL, NULL, 1, 4, 17),
(17, 'PASQUER', 'Romain', '2002-09-12 00:00:00', NULL, NULL, 1, 18, 2),
(18, 'louis', 'jean', '2008-01-01 00:00:00', 'enfant test', NULL, 4, 4, 21),
(19, 'Rachidi', 'Adil', '2009-02-12 00:00:00', NULL, NULL, 2, 22, 3),
(20, 'DEMO', 'Slide', '2000-05-01 00:00:00', NULL, NULL, 4, 23, 2),
(21, 'titi', 'toto', '2014-01-01 00:00:00', 'à tester', 'cancel_icon.png', 1, 4, 5);

-- --------------------------------------------------------

--
-- Structure de la table `EnfantExercices`
--

CREATE TABLE `EnfantExercices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `observation` longtext COLLATE utf8_unicode_ci NOT NULL,
  `DateSaved` datetime NOT NULL,
  `ref_enfant` int(11) DEFAULT NULL,
  `ref_exercices` int(11) DEFAULT NULL,
  `evaluation` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_FE77DD6C41AE645B` (`ref_enfant`),
  KEY `IDX_FE77DD6C60DDEBA4` (`ref_exercices`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Contenu de la table `EnfantExercices`
--

INSERT INTO `EnfantExercices` (`id`, `observation`, `DateSaved`, `ref_enfant`, `ref_exercices`, `evaluation`) VALUES
(1, 'eshtfdv', '2014-03-06 00:00:00', 1, 1, 0),
(2, 'Vue', '2014-03-06 00:00:00', 3, 1, 0),
(3, 'très bonne réponse', '2014-03-13 12:15:55', 4, 2, 2),
(4, 'bonne évolution', '2014-03-24 09:20:55', 10, 1, 2),
(5, 'ttest child', '2014-03-24 10:10:07', 11, 2, 1),
(6, 'ujn', '2014-04-04 22:49:13', 16, 2, 0),
(7, 'trés bien', '2014-04-08 11:18:19', 10, 2, 2);

-- --------------------------------------------------------

--
-- Structure de la table `Enfant_Exercices`
--

CREATE TABLE `Enfant_Exercices` (
  `EnfantId` int(11) NOT NULL,
  `ExerciceId` int(11) NOT NULL,
  PRIMARY KEY (`EnfantId`,`ExerciceId`),
  UNIQUE KEY `UNIQ_53C5252537C261` (`ExerciceId`),
  KEY `IDX_53C525464C8753` (`EnfantId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `Enfant_Phrasecomposees`
--

CREATE TABLE `Enfant_Phrasecomposees` (
  `EnfantId` int(11) NOT NULL,
  `PhraseComposeeId` int(11) NOT NULL,
  PRIMARY KEY (`EnfantId`,`PhraseComposeeId`),
  UNIQUE KEY `UNIQ_DB0179DA3CE7F8E8` (`PhraseComposeeId`),
  KEY `IDX_DB0179DA464C8753` (`EnfantId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `Exercices`
--

CREATE TABLE `Exercices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Objectif` longtext COLLATE utf8_unicode_ci NOT NULL,
  `Question` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Reponse` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `RefPictogramme` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Contenu de la table `Exercices`
--

INSERT INTO `Exercices` (`id`, `Objectif`, `Question`, `Reponse`, `RefPictogramme`) VALUES
(1, 'Objectif : Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Lorem ipsum dolor sit amet, consectetur', 'Lorem ipsum dolor sit amet, consectetur Lorem ipsum dolor sit amet, consectetur', ''),
(2, 'Objectif de la question: connaitre l''image et répondre à la question.', 'Que ce que c''est?', 'jus d''orange - boisson', 'jus_d_orange.png');

-- --------------------------------------------------------

--
-- Structure de la table `fos_user_group`
--

CREATE TABLE `fos_user_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_583D1F3E5E237E06` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `fos_user_user`
--

CREATE TABLE `fos_user_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `locked` tinyint(1) NOT NULL,
  `expired` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  `confirmation_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `credentials_expired` tinyint(1) NOT NULL,
  `credentials_expire_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `date_of_birth` datetime DEFAULT NULL,
  `firstname` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastname` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `biography` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `locale` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timezone` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_data` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:json)',
  `twitter_uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter_data` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:json)',
  `gplus_uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gplus_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gplus_data` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:json)',
  `token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `two_step_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_C560D76192FC23A8` (`username_canonical`),
  UNIQUE KEY `UNIQ_C560D761A0D96FBF` (`email_canonical`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=24 ;

--
-- Contenu de la table `fos_user_user`
--

INSERT INTO `fos_user_user` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `locked`, `expired`, `expires_at`, `confirmation_token`, `password_requested_at`, `roles`, `credentials_expired`, `credentials_expire_at`, `created_at`, `updated_at`, `date_of_birth`, `firstname`, `lastname`, `website`, `biography`, `gender`, `locale`, `timezone`, `phone`, `facebook_uid`, `facebook_name`, `facebook_data`, `twitter_uid`, `twitter_name`, `twitter_data`, `gplus_uid`, `gplus_name`, `gplus_data`, `token`, `two_step_code`) VALUES
(1, 'admintest', 'admintest', 'test@exemple.com', 'test@exemple.com', 1, '5dlatpvxdhs80s8wkcogkkc4wswgw8s', 'WNozpeGF5JtaIJeyGDXncLQHkSUMoDw/SkfLm4FjO+3UcuXBxBO23SCNoRyUvX/03/pz3pbE6LEfwatV9o9s5A==', '2014-05-25 17:19:54', 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:16:"ROLE_SUPER_ADMIN";}', 0, NULL, '2014-01-29 13:52:30', '2014-05-25 17:19:54', NULL, NULL, NULL, NULL, NULL, 'u', NULL, NULL, NULL, NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL),
(2, 'Matt', 'matt', 'matt@gmail.com', 'matt@gmail.com', 1, '5j7ff1ys8zwo88w08cgc4g88g8cc04c', 'kAZXqwRQreep2AlWDp6yFwT8iHITuERMGBDE6iyut/GF8FB8V9e7Sz3E8DGA21WsOK4Wa4jpWJL3HAjPf+OjSQ==', NULL, 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:14:"ROLE_EDUCATEUR";}', 0, NULL, '2014-02-28 17:17:36', '2014-02-28 17:17:36', NULL, 'Matt', 'Damone', NULL, NULL, 'u', NULL, NULL, NULL, NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL),
(3, 'abdel', 'abdel', 'l.abdel@umanteam.com', 'l.abdel@umanteam.com', 1, '881asyr8jjswgcgkso88sg400kw4ccg', 'JrPUHO6dfShbdGvQwqoiGSPeFMu0FCLIKUFtsyYs0SzTzgwV5SGpC+qG8VsUo4Lv7opq94tZfh6AQY3pnM3j5Q==', '2014-05-16 17:53:59', 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:14:"ROLE_EDUCATEUR";}', 0, NULL, '2014-03-03 09:33:25', '2014-05-16 17:53:59', NULL, 'Abdel', 'Lahideb', NULL, NULL, 'f', NULL, NULL, NULL, NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL),
(4, 'jianp', 'jianp', 'g.jian@prnt.com', 'g.jian@prnt.com', 1, 'r737t3k73yso8ks40ogk44c0kk4koww', 'eCDNPV3YQvZ/Ijwc+GDMDdcCE2sM3F7+KDfz3OQ7G8601GUiy3Gt8+vGzk10MbvdEnTY9NZF1v0aBXjy8PQFgA==', '2014-03-19 17:09:34', 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:11:"ROLE_PARENT";}', 0, NULL, '2014-03-03 17:58:40', '2014-03-19 17:09:34', NULL, 'Jian', 'GU', NULL, NULL, 'u', NULL, NULL, NULL, NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL),
(5, 'jiane', 'jiane', 'g.jian@edu.com', 'g.jian@edu.com', 1, 'qdiql7pofusg0k0okkwo44wwwccg08w', '2B14zk6IoKbPbPI+YQw8OM2U8AdEwRRN72aTwSC5S/vpWUrqXJPWOjtRT3dQ8PgPa32RsRhdUyWotuaAqVfwSA==', '2014-06-04 15:36:19', 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:14:"ROLE_EDUCATEUR";}', 0, NULL, '2014-03-03 18:00:43', '2014-06-04 15:36:19', NULL, 'Jian', 'GU', NULL, NULL, 'm', NULL, NULL, NULL, NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL),
(6, 'test-register', 'test-register', 'jian@umanteam.com', 'jian@umanteam.com', 1, '2hedah5mr1ogcckwc4s8o04o8c8w8o4', '2eACCLhuDsBbZT0tbnaMR2HJG1CiydAZ0T9gaZWgaNdHPS9RxuSjCI3Xvrv6Fp31Ikit02SDKRUtXSJIo2Hx1w==', '2014-03-20 23:29:49', 0, 0, NULL, '4yhm2emhvvk0ogwwkwsogwosc0kg0s0wk8wckw4csss040gwk8', '2014-03-20 23:27:07', 'a:0:{}', 0, NULL, '2014-03-17 11:41:02', '2014-03-20 23:29:49', NULL, NULL, NULL, NULL, NULL, 'u', NULL, NULL, NULL, NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL),
(10, 'tokarev', 'tokarev', 'g.jian@umanteam.com', 'g.jian@umanteam.com', 1, 'j0ytydpvu9cswck4w04os0cgg88c4kk', 'Ih9cJ1uzWoiikhIeFIfvZYRMvdWCGACtGma3TIIv4yR+8F+RJJlvJHA44RQ4pADDmepwTQKOi3ZT/otNlNPDeQ==', '2014-03-20 23:44:08', 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:14:"ROLE_EDUCATEUR";}', 0, NULL, '2014-03-20 23:43:45', '2014-06-03 11:50:28', NULL, 'tokarev', 'toto', NULL, NULL, 'u', NULL, NULL, NULL, NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL),
(11, 'sana', 'sana', 'lahideb.abdel@gmail.com', 'lahideb.abdel@gmail.com', 1, 'tgnn5763uf448osg4c0wwkc40wsook0', 'KLBvaC5H8O+lHvxmsw61t1LYr8lGfUsqEuFdUsMScft3Rq26BA13OnbKaKUfkmEy1jMtjjEMRtA+0GamvrtoBQ==', '2014-04-04 18:20:01', 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:11:"ROLE_PARENT";}', 0, NULL, '2014-03-24 10:07:15', '2014-04-04 18:20:01', NULL, 'sana', 'sana', NULL, NULL, 'u', NULL, NULL, NULL, NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL),
(12, 'mbenkherrat', 'mbenkherrat', 'm.benkherrat@epmi.fr', 'm.benkherrat@epmi.fr', 1, 'rkceb4ktuzkgs040wwc0808k0o4gsc4', 'S1Ga9LatHGIhJ9p5exbD6/xgU2HUEyMrsx05jo99apCN+BDB1PFdVKF71DWyZdUX9XxMSjVNVnkAknMlKA/syA==', '2014-05-20 10:19:52', 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:14:"ROLE_EDUCATEUR";}', 0, NULL, '2014-03-25 12:45:56', '2014-05-20 10:19:52', NULL, 'MONCEF', 'BENKHERRAT', NULL, NULL, 'm', NULL, NULL, NULL, NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL),
(13, 'rabaa', 'rabaa', 'r.rabaa@umanteam.com', 'r.rabaa@umanteam.com', 1, 'c5w7nc10pf4sco80cc0w8gwk0o8w8os', 'Od4Z1HWrYtgWCkl47hXEQxnHIe5BnjpizW7HNdFO45xNBNyLE3NV7fo4LUyQvhlKQPmOtZll+BYtT+fAU8I0yg==', '2014-03-27 13:55:28', 0, 0, NULL, '45d9ofl760mc8kgo4wk0s8csssc4k8swgg040gsswccggs48c4', '2014-03-27 09:58:12', 'a:1:{i:0;s:14:"ROLE_EDUCATEUR";}', 0, NULL, '2014-03-25 15:49:37', '2014-03-27 13:55:28', NULL, 'rabaa', 'ridene', NULL, NULL, 'u', NULL, NULL, NULL, NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL),
(14, 'nassur', 'nassur', 'm.nassur@umanteam.com', 'm.nassur@umanteam.com', 1, '24a292da6e74cc8w4cok8wowows448w', 'NO6lhCQnJinubdy4lc/djye3jBlmGrZ6Vr51iNlSKSPCLe9TDgHxWSgvFjE94n9xL6hAPswzt9QHDzRvQgkTZA==', '2014-03-31 12:13:04', 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:14:"ROLE_EDUCATEUR";}', 0, NULL, '2014-03-31 12:09:48', '2014-03-31 12:13:04', '1977-05-03 00:00:00', 'Nassur', 'Mhoumadi', NULL, NULL, 'm', 'fr', NULL, NULL, NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL),
(17, 'Marie Perrier', 'marie perrier', 'marie.perrier22@orange.fr', 'marie.perrier22@orange.fr', 1, 'qaio1jecccg0wokk8ksogco0wcosk4s', 'by2Tf6rw6pvjUUFv8ZbN2RY9guuK8K89AwJPx8sffb7smVKPKkBKcWTQiu7FdycNoq+98xVVir8hsr9ww4rWTg==', '2014-04-07 21:31:55', 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:14:"ROLE_EDUCATEUR";}', 0, NULL, '2014-04-04 22:31:33', '2014-04-07 21:31:55', NULL, 'Marie', 'Perrier', NULL, NULL, 'f', 'fr', 'Europe/Paris', '0672450382', NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL),
(18, 'PASQUER Romain', 'pasquer romain', 'family.pasquer@free.fr', 'family.pasquer@free.fr', 1, 'r0r1ywnndjkcgkokso0c44sos8gog4o', 'POnx5lPPGmYwKL2Qgo56bsx1wCXW+iMy1S/FoeI8ZtBKQCxtPJDpN4BuMyIyujKQPz2eID048+Nj6TiUc2X0fA==', '2014-04-05 08:50:18', 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:11:"ROLE_PARENT";}', 0, NULL, '2014-04-05 00:19:11', '2014-04-05 08:50:18', NULL, 'Romain', 'PASQUER', NULL, NULL, 'm', 'fr', NULL, NULL, NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL),
(19, 'benkherrat', 'benkherrat', 'benkherrat@yahoo.fr', 'benkherrat@yahoo.fr', 1, 'pf19gf2yfascocko04c4404wocgskcs', 'gwnt6VlyvxJPq1ER4AeclHCqVQ5TK5WcKDIHPh3XKAOdtyDVqDhlEQqTVY9MChlqh/qzmzRBLOTkeH5n8p5H3A==', '2014-04-29 14:51:23', 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:11:"ROLE_PARENT";}', 0, NULL, '2014-04-29 14:35:27', '2014-04-29 15:13:28', NULL, 'Moncef', 'Benkherrat', NULL, NULL, 'm', NULL, NULL, NULL, NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL),
(20, 'mk', 'mk', 'benkherrat@epmi.fr', 'benkherrat@epmi.fr', 0, 'ihtykv3rw288o0ws8c80ck4ckwowokg', 'M85IcpQCY72oZMA+PMzy5ZyR/+azyw5e35nJsa9HTa4mYCnQcfBjmkDOl6ix+AGj9CZruzlBTSZbl8+qBVENzA==', NULL, 0, 0, NULL, '4ki8ld93zoqog8kkc4ksswccc4k84cwko48c4kw4wkc8ow88gw', NULL, 'a:1:{i:0;s:11:"ROLE_PARENT";}', 0, NULL, '2014-05-06 15:07:27', '2014-05-06 15:07:27', NULL, NULL, NULL, NULL, NULL, 'u', NULL, NULL, NULL, NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL),
(21, 'testumanteam', 'testumanteam', 'test@umanteam.com', 'test@umanteam.com', 1, 'qdr5eixlqcggokwg4s88k48wkg8cw88', 'mwBUp+ZSSfFB4DB6NXpjeM5wx2ywBwk7xFv0jU7O5zro7M1GETg9iAd7/OWfCakT3ZSLDBmjR5TiuECnQCBnsA==', '2014-05-13 21:15:10', 0, 0, NULL, '56bjed7bfcw04cs8sgs08soc4k8kswkcks0s0cgw0kkwgokwsw', NULL, 'a:1:{i:0;s:14:"ROLE_EDUCATEUR";}', 0, NULL, '2014-05-10 17:18:39', '2014-05-13 21:15:10', NULL, 'test', 'test', NULL, NULL, 'u', NULL, NULL, NULL, NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL),
(22, 'Adil', 'adil', 'rachidiadil00@gmail.com', 'rachidiadil00@gmail.com', 1, 'nlu2jqge528gg8gkscog8s4gsgcw0gk', 'hoSndE1hxikb79QEeEjM/e3kzOcUb3Kz39CA0KPG0IuSZfUZMTxru/PmyKtBYefUzSuhiH3I++LMAJFswnkTmw==', '2014-05-13 14:29:45', 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:11:"ROLE_PARENT";}', 0, NULL, '2014-05-13 14:26:32', '2014-05-13 14:29:45', NULL, 'Adil', 'Rachidi', NULL, NULL, 'u', NULL, NULL, NULL, NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL),
(23, 'palege', 'palege', 'p.legendre@bouygues-es.com', 'p.legendre@bouygues-es.com', 1, 'l0yv1qbsfdccock8w4w0k8gwk8wc0ws', 'L67HlLZGk8StCbYw1M+z28roQ64fAvf4LKsUbb8RGkdwZDXrYNX6WTAl/22nBMK0B1UYkmlR23BBsPquvVDgmg==', '2014-05-13 19:39:32', 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:11:"ROLE_PARENT";}', 0, NULL, '2014-05-13 18:43:25', '2014-05-13 19:39:32', NULL, 'Pascal', 'LEGENDRE', NULL, NULL, 'u', NULL, NULL, NULL, NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `fos_user_user_group`
--

CREATE TABLE `fos_user_user_group` (
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`group_id`),
  KEY `IDX_B3C77447A76ED395` (`user_id`),
  KEY `IDX_B3C77447FE54D947` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `Image`
--

CREATE TABLE `Image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ref_enfant` int(11) DEFAULT NULL,
  `ref_mot` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_4FC2B5B41AE645B` (`ref_enfant`),
  KEY `IDX_4FC2B5BCEBC87B2` (`ref_mot`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Contenu de la table `Image`
--

INSERT INTO `Image` (`id`, `Url`, `ref_enfant`, `ref_mot`) VALUES
(1, 'kangaroo_lay1.jpg', 4, 4),
(2, 'Abdel.pdf', 3, 4),
(3, 'abdel.jpg', 10, 4),
(4, '36- Boire.jpg', 16, 5);

-- --------------------------------------------------------

--
-- Structure de la table `Mot`
--

CREATE TABLE `Mot` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Titre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Categorie` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ref_theme` int(11) DEFAULT NULL,
  `titre_phrase` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `personne` int(11) DEFAULT NULL,
  `genre` int(11) DEFAULT NULL,
  `pps` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dps` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tps` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ppp` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dpp` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tpp` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ms` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mp` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fs` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fp` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ref_niveau` int(11) DEFAULT NULL,
  `pc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `plur` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_320E05CCE4BE82CA` (`ref_theme`),
  KEY `IDX_320E05CC3EC69B92` (`ref_niveau`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=104 ;

--
-- Contenu de la table `Mot`
--

INSERT INTO `Mot` (`id`, `Titre`, `Categorie`, `ref_theme`, `titre_phrase`, `personne`, `genre`, `pps`, `dps`, `tps`, `ppp`, `dpp`, `tpp`, `ms`, `mp`, `fs`, `fp`, `ref_niveau`, `pc`, `plur`) VALUES
(1, 'eau', '', 4, '', NULL, 3, '', '', '', '', '', '', '', '', '', '', 1, NULL, NULL),
(2, 'banane', '', 5, '', NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 'bananes'),
(3, 'toilettes', '', 6, '', NULL, 4, '', '', '', '', '', '', '', '', '', '', 1, NULL, NULL),
(4, 'être', '', 2, '', NULL, NULL, 'suis', 'es', 'est', 'somme', 'êtes', 'sont', '', '', '', '', 3, 'été', NULL),
(5, 'avoir', '', 2, '', NULL, NULL, 'ai', 'as', 'a', 'avons', 'avez', 'ont', '', '', '', '', 3, 'eu', NULL),
(6, 'boire', '', 3, '', NULL, NULL, 'bois', 'bois', 'boit', 'buvons', 'buvez', 'boivent', NULL, NULL, NULL, NULL, 3, 'bu', NULL),
(7, 'aller', '', 3, '', NULL, NULL, 'vais', 'vas', 'va', 'allons', 'allez', 'vont', NULL, NULL, NULL, NULL, 3, 'allé', NULL),
(8, 'je', '', 1, '', 1, NULL, '', '', '', '', '', '', '', '', '', '', 3, NULL, NULL),
(9, 'tu', '', 1, '', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL),
(10, 'nous', '', 1, '', 4, NULL, '', '', '', '', '', '', '', '', '', '', 4, NULL, NULL),
(11, 'vous', '', 1, '', 5, NULL, '', '', '', '', '', '', '', '', '', '', 4, NULL, NULL),
(12, 'jus d''orange', '', 4, '', NULL, 3, '', '', '', '', '', '', '', '', '', '', 1, NULL, NULL),
(13, 'dessiner', '', 3, '', NULL, NULL, 'dessine', 'dessines', 'dessine', 'dessinons', 'dessinez', 'dessinent', NULL, NULL, NULL, NULL, 3, 'dessiné', NULL),
(14, 'de l''', '', 8, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL),
(15, 'du', '', 8, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL),
(16, 'un', '', 8, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL),
(17, 'une', '', 8, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL),
(18, 'ne pas crier', '', 9, '', NULL, NULL, '', '', '', '', '', '', '', '', '', '', 3, NULL, NULL),
(19, 'rester assis', '', 9, '', NULL, NULL, '', '', '', '', '', '', '', '', '', '', 3, NULL, NULL),
(20, 'vouloir', '', 2, '', NULL, NULL, 'veux', 'veux', 'veut', 'voulons', 'voulez', 'veulent', NULL, NULL, NULL, NULL, 3, 'voulu', NULL),
(21, 'pouvoir', '', 2, '', NULL, NULL, 'peux', 'peux', 'peut', 'pouvons', 'pouvez', 'peuvent', '', '', '', '', 4, 'pu', NULL),
(22, '1', '', 7, '', NULL, NULL, '', '', '', '', '', '', '', '', '', '', 3, NULL, NULL),
(23, '2', '', 7, '', NULL, NULL, '', '', '', '', '', '', '', '', '', '', 3, NULL, NULL),
(24, 'grand', '', 10, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'grand', 'grands', 'grande', 'grandes', 3, NULL, NULL),
(25, 'petit', '', 10, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'petit', 'petits', 'petite', 'petites', 3, NULL, NULL),
(26, 'froid', '', 10, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'froid', 'froids', 'froide', 'froides', 3, NULL, NULL),
(27, 'écouter', NULL, 3, NULL, NULL, NULL, 'écoute', 'écoutes', 'écoute', 'écoutons', 'écoutez', 'écoutent', NULL, NULL, NULL, NULL, 3, 'écouté', NULL),
(28, 'regarder', NULL, 3, NULL, NULL, NULL, 'regarde', 'regardes', 'regarde', 'regardons', 'regardez', 'regardent', NULL, NULL, NULL, NULL, 3, 'regardé', NULL),
(29, 'soda', NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL),
(30, 'gateau', NULL, 12, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 'gateaux'),
(31, '3', NULL, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL),
(32, 'pomme', NULL, 5, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 'pommes'),
(33, 'vert', NULL, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'vert', 'verts', 'verte', 'vertes', 3, NULL, NULL),
(34, 'rouge', NULL, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'rouge', 'rouges', 'rouge', 'rouges', 3, NULL, NULL),
(35, 'bleu', NULL, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'bleu', 'bleus', 'bleue', 'bleues', 3, NULL, NULL),
(36, 'bain', NULL, 6, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL),
(37, 'jaune', NULL, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'jaune', 'jaunes', 'jaune', 'jaunes', 3, NULL, NULL),
(38, 'rose', NULL, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'rose', 'roses', 'rose', 'roses', 3, NULL, NULL),
(39, 'ranger', NULL, 9, NULL, NULL, NULL, 'range', 'ranges', 'range', 'rangeons', 'rangez', 'rangent', NULL, NULL, NULL, NULL, 3, 'rangé', NULL),
(40, 'glace', NULL, 12, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL),
(42, 'chocolat', NULL, 12, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL),
(43, 'chaud', NULL, 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'chaud', 'chauds', 'chaude', 'chaudes', 3, NULL, NULL),
(45, 'mal à la tête', NULL, 13, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL),
(46, 'mal au ventre', NULL, 13, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL),
(47, 'manger', NULL, 3, NULL, NULL, NULL, 'mange', 'manges', 'mange', 'mangeons', 'mangez', 'mangent', NULL, NULL, NULL, NULL, 1, NULL, NULL),
(48, 'dormir', NULL, 3, NULL, NULL, NULL, 'dors', 'dors', 'dort', 'dormons', 'dormez', 'dorment', NULL, NULL, NULL, NULL, 1, 'dormi', NULL),
(49, 'écrire', NULL, 3, NULL, NULL, NULL, 'écris', 'écris', 'écrit', 'écrivons', 'écrivez', 'écrivent', NULL, NULL, NULL, NULL, 1, 'écrit', NULL),
(50, 'laver les mains', NULL, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL),
(51, '4', NULL, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL),
(52, '5', NULL, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL),
(53, '6', NULL, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL),
(54, '7', NULL, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL),
(55, '8', NULL, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL),
(56, '9', NULL, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL),
(57, '10', NULL, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL),
(58, 'à l''', NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL),
(59, 'à la', NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL),
(60, 'à', NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL),
(61, 'au', NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL),
(62, 'aux', NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL),
(63, 'avec', NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL),
(64, 'ce', NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL),
(65, 'ces', NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL),
(66, 'cette', NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL),
(67, 'cet', NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL),
(68, 'd''', NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL),
(69, 'dans', NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL),
(70, 'des', NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL),
(71, 'école', NULL, 14, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 'écoles'),
(72, 'et', NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL),
(73, 'gris', NULL, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'gris', 'gris', 'grise', 'gris', 3, NULL, NULL),
(74, 'jardin', NULL, 14, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 'jardins'),
(75, 'l''', NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL),
(76, 'la', NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL),
(77, 'le', NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL),
(78, 'les', NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL),
(79, 'leur', NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL),
(80, 'leurs', NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL),
(81, 'ma', NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL),
(82, 'maison', NULL, 14, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 'maisons'),
(83, 'marron', NULL, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL),
(84, 'mauvre', NULL, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL),
(85, 'mes', NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL),
(86, 'mon', NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL),
(87, 'noir', NULL, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL),
(88, 'notre', NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL),
(89, 'orange', NULL, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL),
(90, 'piscine', NULL, 14, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 'piscines'),
(91, 'sa', NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL),
(92, 'ses', NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL),
(93, 'son', NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL),
(94, 'ta', NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL),
(95, 'tes', NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL),
(96, 'ton', NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL),
(99, 'vert foncé', NULL, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL),
(100, 'vos', NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL),
(101, 'votre', NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL),
(102, 'mer', NULL, 14, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL),
(103, 'parc', NULL, 14, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 'parcs');

-- --------------------------------------------------------

--
-- Structure de la table `Niveau`
--

CREATE TABLE `Niveau` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Titre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Contenu de la table `Niveau`
--

INSERT INTO `Niveau` (`id`, `Titre`) VALUES
(1, '1'),
(2, '2'),
(3, '3'),
(4, '4');

-- --------------------------------------------------------

--
-- Structure de la table `Nv3AutoFill`
--

CREATE TABLE `Nv3AutoFill` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ref_verbe` int(11) DEFAULT NULL,
  `ref_complement` int(11) DEFAULT NULL,
  `AutoFill` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_A7D1E6EFF2E5A04D` (`ref_verbe`),
  KEY `IDX_A7D1E6EFFE61F12A` (`ref_complement`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=19 ;

--
-- Contenu de la table `Nv3AutoFill`
--

INSERT INTO `Nv3AutoFill` (`id`, `ref_verbe`, `ref_complement`, `AutoFill`) VALUES
(1, 7, 3, 'aux'),
(2, 20, 1, 'de l'''),
(3, 6, 12, 'du'),
(4, 6, 1, 'de l'''),
(5, 20, 12, 'un'),
(6, 20, 30, 'un'),
(7, 20, 2, 'une'),
(8, 20, 32, 'une'),
(9, 6, 29, 'un'),
(10, 20, 29, 'un'),
(11, 47, 42, 'du'),
(12, 20, 42, 'du'),
(13, 47, 40, 'une'),
(14, 20, 40, 'une'),
(15, 7, 36, 'au'),
(16, 7, 90, 'à la'),
(17, 7, 103, 'au'),
(18, 7, 71, 'à l''');

-- --------------------------------------------------------

--
-- Structure de la table `parents`
--

CREATE TABLE `parents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Prenom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `RefEnfant` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `PhraseComposee`
--

CREATE TABLE `PhraseComposee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Contenu` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ref_enfant` int(11) DEFAULT NULL,
  `ref_niveau` int(11) DEFAULT NULL,
  `DateSaved` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ref_niveau` (`ref_niveau`),
  KEY `ref_enfant` (`ref_enfant`),
  KEY `IDX_771529AA41AE645B` (`ref_enfant`),
  KEY `IDX_771529AA3EC69B92` (`ref_niveau`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=46 ;

--
-- Contenu de la table `PhraseComposee`
--

INSERT INTO `PhraseComposee` (`id`, `Contenu`, `ref_enfant`, `ref_niveau`, `DateSaved`) VALUES
(1, 'je veux aller boire de l''eau', 4, 3, '2014-03-13 12:04:14'),
(2, 'je veux aller boire', 10, NULL, '2014-03-24 09:19:57'),
(3, 'banane', 11, NULL, '2014-03-24 10:09:38'),
(4, 'banane', 12, 1, '2014-03-25 12:52:32'),
(5, 'banane', 12, 1, '2014-03-25 12:54:38'),
(6, 'banane', 12, 1, '2014-03-25 12:54:42'),
(7, 'eau', 4, 1, '2014-03-28 16:14:55'),
(8, 'Je veux une pomme.', 5, 2, '2014-03-31 14:22:18'),
(9, 'Je veux une pomme.', 5, 2, '2014-03-31 14:22:22'),
(10, 'Je veux une pomme.', 5, 2, '2014-03-31 14:22:26'),
(11, 'banane', 5, 1, '2014-03-31 14:35:08'),
(12, 'banane', 5, 1, '2014-03-31 14:35:16'),
(13, 'banane', 5, 1, '2014-03-31 14:45:07'),
(14, 'Je veux de l''eau.', 5, 2, '2014-03-31 14:57:22'),
(15, 'J''ai une pomme', 5, 3, '2014-03-31 15:03:04'),
(16, 'nous avons une banane', 5, 4, '2014-03-31 15:04:26'),
(17, 'Je veux boire du jus d''orange.', 12, 2, '2014-03-31 15:45:31'),
(18, 'Je veux boire du jus d''orange.', 12, 2, '2014-03-31 15:45:35'),
(19, 'Je veux boire du jus d''orange.', 12, 2, '2014-03-31 15:45:39'),
(20, 'Je veux boire du jus d''orange.', 12, 2, '2014-03-31 15:45:41'),
(21, 'Je veux boire du jus d''orange.', 12, 2, '2014-03-31 15:45:42'),
(22, 'Je veux boire du jus d''orange.', 12, 2, '2014-03-31 15:45:48'),
(23, 'Je veux boire du jus d''orange.', 12, 2, '2014-03-31 15:45:50'),
(24, 'Je veux de l''eau.', 3, 2, '2014-03-31 22:07:50'),
(25, 'eau', 12, 4, '2014-04-02 18:09:00'),
(26, 'boire', 12, 3, '2014-04-04 11:21:15'),
(27, 'toilettes', 5, 1, '2014-04-04 18:29:06'),
(28, 'soda', 16, 1, '2014-04-04 22:42:33'),
(29, 'soda', 16, 1, '2014-04-04 22:42:40'),
(30, 'je veux boire du jus d''orange', 5, 3, '2014-04-07 13:25:49'),
(31, 'soda jus d''orange', 5, 4, '2014-04-07 14:35:27'),
(32, 'je', 12, 4, '2014-04-07 15:58:47'),
(33, 'je veux boire de l''eau', 10, 3, '2014-04-08 07:00:44'),
(34, '1', 5, 3, '2014-04-08 11:12:59'),
(35, '2 soda', 5, 3, '2014-04-08 11:18:59'),
(36, 'Vert Rouge', 5, 3, '2014-04-08 11:19:01'),
(37, 'Vert Rouge', 5, 3, '2014-04-08 11:19:05'),
(38, 'Vert Rouge', 5, 3, '2014-04-08 11:19:10'),
(39, 'Vert', 5, 3, '2014-04-08 11:19:33'),
(40, 'je veux dessiner gateau', 12, 3, '2014-05-06 16:50:58'),
(41, 'je veux dessiner gateau', 12, 3, '2014-05-06 16:51:08'),
(42, 'eau', 5, 3, '2014-05-07 15:12:46'),
(43, 'eau', 5, 3, '2014-05-07 15:12:49'),
(44, 'eau', 5, 3, '2014-05-07 15:12:51'),
(45, 'je suis allé à l''école', 5, 3, '2014-05-23 20:05:08');

-- --------------------------------------------------------

--
-- Structure de la table `PhraseProposee`
--

CREATE TABLE `PhraseProposee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Contenu` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=27 ;

--
-- Contenu de la table `PhraseProposee`
--

INSERT INTO `PhraseProposee` (`id`, `Contenu`) VALUES
(1, 'de l''eau.'),
(2, 'une banane.'),
(3, 'aller aux toilettes.'),
(4, 'un jus d''orange.'),
(5, 'un gâteau.'),
(6, 'du soda.'),
(7, 'une pomme.'),
(8, 'prendre un bain.'),
(9, 'Je range'),
(10, 'une glace.'),
(11, 'un chocolat.'),
(12, 'J''ai chaud'),
(13, 'J''ai froid'),
(14, 'J''ai besoin d''aide.'),
(15, 'J''ai mal à la tête.'),
(16, 'J''ai mal au ventre.'),
(17, 'manger.'),
(18, 'dormir.'),
(19, 'écrire.'),
(21, 'laver les mains.'),
(22, 'aller à la maison.'),
(23, 'aller à la mer.'),
(24, 'aller au parc.'),
(25, 'aller à la piscine.'),
(26, 'aller à l''école.');

-- --------------------------------------------------------

--
-- Structure de la table `Pictogramme`
--

CREATE TABLE `Pictogramme` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `PictoUrl` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ImgUrl` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ref_phrase` int(11) DEFAULT NULL,
  `ref_mot` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_B63C9910CEBC87B2` (`ref_mot`),
  UNIQUE KEY `UNIQ_B63C9910D7528EF5` (`ref_phrase`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=114 ;

--
-- Contenu de la table `Pictogramme`
--

INSERT INTO `Pictogramme` (`id`, `PictoUrl`, `ImgUrl`, `ref_phrase`, `ref_mot`) VALUES
(1, 'eau.png', '', 1, 1),
(2, 'banane.png', '', 2, 2),
(3, 'toilettes.png', '', 3, 3),
(4, 'je-icon.png', '', NULL, 8),
(5, 'boire.png', '', NULL, 6),
(6, 'tu.png', '', NULL, 9),
(7, 'jus_d_orange.png', '', 4, 12),
(8, 'dessiner.png', '', NULL, 13),
(10, 'de_l_.png', '', NULL, 14),
(11, 'du.png', '', NULL, 15),
(12, 'un.png', '', NULL, 16),
(13, 'une.png', '', NULL, 17),
(15, 'assis.png', '', NULL, 19),
(16, 'vouloir-icon.png', '', NULL, 20),
(18, '1.png', '', NULL, 22),
(19, '2.png', '', NULL, 23),
(20, 'etre-icon.png', '', NULL, 4),
(21, 'avoir-icon.png', '', NULL, 5),
(22, 'aller-icon.png', '', NULL, 7),
(23, 'grand.png', '', NULL, 24),
(24, 'petit.png', '', NULL, 25),
(29, 'froid.png', '', NULL, 26),
(30, 'nous-icon.png', '', NULL, 10),
(31, 'vous-icon.png', '', NULL, 11),
(32, 'gateau.png', NULL, 5, 30),
(33, 'soda.png', NULL, 6, 29),
(34, 'ecouter.png', NULL, NULL, 27),
(35, 'regarder.png', NULL, NULL, 28),
(36, '3.png', NULL, NULL, 31),
(37, 'pomme.png', NULL, 7, 32),
(44, 'vert.png', NULL, NULL, 33),
(45, 'rouge.png', NULL, NULL, 34),
(46, 'bleu.png', NULL, NULL, 35),
(47, 'bain.png', NULL, 8, 36),
(48, 'jaune.png', NULL, NULL, 37),
(49, 'rose.png', NULL, NULL, 38),
(50, 'ranger.png', NULL, 9, 39),
(51, 'glace.png', NULL, 10, 40),
(52, 'chocolat.png', NULL, 11, 42),
(53, 'chaud.png', NULL, 12, 43),
(58, 'mal_tete.png', NULL, 15, 45),
(59, 'mal_ventre.png', NULL, 16, 46),
(60, 'manger.png', NULL, 17, 47),
(61, 'dormir.png', NULL, 18, 48),
(62, 'ecrire.gif', NULL, 19, 49),
(64, 'laver-mains.png', NULL, 21, 50),
(65, '4.png', NULL, NULL, 51),
(66, '5.png', NULL, NULL, 52),
(67, '6.png', NULL, NULL, 53),
(68, '7.png', NULL, NULL, 54),
(69, '8.png', NULL, NULL, 55),
(70, '9.png', NULL, NULL, 56),
(71, '10.png', NULL, NULL, 57),
(72, 'a-l_.png', NULL, NULL, 58),
(73, 'a-la.png', NULL, NULL, 59),
(74, 'a.png', NULL, NULL, 60),
(75, 'au.png', NULL, NULL, 61),
(76, 'aux.png', NULL, NULL, 62),
(77, 'avec.png', NULL, NULL, 63),
(78, 'ce.png', NULL, NULL, 64),
(79, 'ces.png', NULL, NULL, 65),
(80, 'cette.png', NULL, NULL, 66),
(81, 'cet.png', NULL, NULL, 67),
(82, 'd_.png', NULL, NULL, 68),
(83, 'dans.png', NULL, NULL, 69),
(84, 'des.png', NULL, NULL, 70),
(85, 'ecole.png', NULL, 26, 71),
(86, 'et.png', NULL, NULL, 72),
(87, 'gris.png', NULL, NULL, 73),
(88, 'l_.png', NULL, NULL, 75),
(89, 'la.png', NULL, NULL, 76),
(90, 'le.png', NULL, NULL, 77),
(91, 'les.png', NULL, NULL, 78),
(92, 'leur.png', NULL, NULL, 79),
(93, 'leurs.png', NULL, NULL, 80),
(94, 'maison.png', NULL, 22, 82),
(95, 'marron.png', NULL, NULL, 83),
(96, 'mauve.png', NULL, NULL, 84),
(97, 'mes.png', NULL, NULL, 85),
(98, 'mon.png', NULL, NULL, 86),
(99, 'noir.png', NULL, NULL, 87),
(100, 'notre.png', NULL, NULL, 88),
(101, 'orange.png', NULL, NULL, 89),
(102, 'piscine.png', NULL, 25, 90),
(103, 'sa.png', NULL, NULL, 91),
(104, 'ses.png', NULL, NULL, 92),
(105, 'son.png', NULL, NULL, 93),
(106, 'ta.png', NULL, NULL, 94),
(107, 'tes.png', NULL, NULL, 95),
(108, 'ton.png', NULL, NULL, 96),
(109, 'vert-fonce.png', NULL, NULL, 99),
(110, 'vos.png', NULL, NULL, 100),
(111, 'votre.png', NULL, NULL, 101),
(112, 'mer.png', NULL, 23, 102),
(113, 'parc.png', NULL, 24, 103);

-- --------------------------------------------------------

--
-- Structure de la table `Responsable`
--

CREATE TABLE `Responsable` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Prenom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `RefEnfant` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `Theme`
--

CREATE TABLE `Theme` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Titre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `PictoUrl` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=15 ;

--
-- Contenu de la table `Theme`
--

INSERT INTO `Theme` (`id`, `Titre`, `PictoUrl`) VALUES
(1, 'Sujets', 'sujets.png'),
(2, 'Demandes', 'demande.png'),
(3, 'Actions', 'action.png'),
(4, 'Boissons', 'boissons.png'),
(5, 'Fruits', 'fruits.png'),
(6, 'Propreté', 'proprete.png'),
(7, 'Chiffres', 'chiffres.png'),
(8, 'Déterminants', 'determinant.png'),
(9, 'Comportements', 'comportement.png'),
(10, 'Adjectifs', 'adjectifs.png'),
(11, 'Couleurs', 'couleurs.png'),
(12, 'Dessert', 'dessert.png'),
(13, 'Etat physique', 'etat-physique.png'),
(14, 'Lieux', 'lieu.png');

-- --------------------------------------------------------

--
-- Structure de la table `User`
--

CREATE TABLE `User` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `locked` tinyint(1) NOT NULL,
  `expired` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  `confirmation_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `credentials_expired` tinyint(1) NOT NULL,
  `credentials_expire_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `date_of_birth` datetime DEFAULT NULL,
  `firstname` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastname` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `biography` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `locale` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timezone` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_data` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:json)',
  `twitter_uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter_data` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:json)',
  `gplus_uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gplus_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gplus_data` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:json)',
  `token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `two_step_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8D93D64992FC23A8` (`username_canonical`),
  UNIQUE KEY `UNIQ_8D93D649A0D96FBF` (`email_canonical`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `acl_entries`
--
ALTER TABLE `acl_entries`
  ADD CONSTRAINT `FK_46C8B8063D9AB4A6` FOREIGN KEY (`object_identity_id`) REFERENCES `acl_object_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_46C8B806DF9183C9` FOREIGN KEY (`security_identity_id`) REFERENCES `acl_security_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_46C8B806EA000B10` FOREIGN KEY (`class_id`) REFERENCES `acl_classes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `acl_object_identities`
--
ALTER TABLE `acl_object_identities`
  ADD CONSTRAINT `FK_9407E54977FA751A` FOREIGN KEY (`parent_object_identity_id`) REFERENCES `acl_object_identities` (`id`);

--
-- Contraintes pour la table `acl_object_identity_ancestors`
--
ALTER TABLE `acl_object_identity_ancestors`
  ADD CONSTRAINT `FK_825DE2993D9AB4A6` FOREIGN KEY (`object_identity_id`) REFERENCES `acl_object_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_825DE299C671CEA1` FOREIGN KEY (`ancestor_id`) REFERENCES `acl_object_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `Activite`
--
ALTER TABLE `Activite`
  ADD CONSTRAINT `FK_410337431ED6FEAE` FOREIGN KEY (`RefPictogramme`) REFERENCES `Pictogramme` (`id`);

--
-- Contraintes pour la table `Activite_Enfant`
--
ALTER TABLE `Activite_Enfant`
  ADD CONSTRAINT `FK_C2BCF36E450D2529` FOREIGN KEY (`enfant_id`) REFERENCES `Enfant` (`id`),
  ADD CONSTRAINT `FK_C2BCF36E9B0F88B1` FOREIGN KEY (`activite_id`) REFERENCES `Activite` (`id`);

--
-- Contraintes pour la table `Enfant`
--
ALTER TABLE `Enfant`
  ADD CONSTRAINT `FK_331B09943EC69B92` FOREIGN KEY (`ref_niveau`) REFERENCES `Niveau` (`id`),
  ADD CONSTRAINT `FK_331B0994489708B6` FOREIGN KEY (`ref_parent`) REFERENCES `fos_user_user` (`id`),
  ADD CONSTRAINT `FK_331B09945660067` FOREIGN KEY (`ref_educateur`) REFERENCES `fos_user_user` (`id`);

--
-- Contraintes pour la table `EnfantExercices`
--
ALTER TABLE `EnfantExercices`
  ADD CONSTRAINT `FK_FE77DD6C41AE645B` FOREIGN KEY (`ref_enfant`) REFERENCES `Enfant` (`id`),
  ADD CONSTRAINT `FK_FE77DD6C60DDEBA4` FOREIGN KEY (`ref_exercices`) REFERENCES `Exercices` (`id`);

--
-- Contraintes pour la table `fos_user_user_group`
--
ALTER TABLE `fos_user_user_group`
  ADD CONSTRAINT `FK_B3C77447A76ED395` FOREIGN KEY (`user_id`) REFERENCES `User` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_B3C77447FE54D947` FOREIGN KEY (`group_id`) REFERENCES `fos_user_group` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `Image`
--
ALTER TABLE `Image`
  ADD CONSTRAINT `FK_4FC2B5B41AE645B` FOREIGN KEY (`ref_enfant`) REFERENCES `Enfant` (`id`),
  ADD CONSTRAINT `FK_4FC2B5BCEBC87B2` FOREIGN KEY (`ref_mot`) REFERENCES `Pictogramme` (`id`);

--
-- Contraintes pour la table `Mot`
--
ALTER TABLE `Mot`
  ADD CONSTRAINT `FK_320E05CC3EC69B92` FOREIGN KEY (`ref_niveau`) REFERENCES `Niveau` (`id`),
  ADD CONSTRAINT `FK_320E05CCE4BE82CA` FOREIGN KEY (`ref_theme`) REFERENCES `Theme` (`id`);

--
-- Contraintes pour la table `Nv3AutoFill`
--
ALTER TABLE `Nv3AutoFill`
  ADD CONSTRAINT `FK_A7D1E6EFF2E5A04D` FOREIGN KEY (`ref_verbe`) REFERENCES `Mot` (`id`),
  ADD CONSTRAINT `FK_A7D1E6EFFE61F12A` FOREIGN KEY (`ref_complement`) REFERENCES `Mot` (`id`);

--
-- Contraintes pour la table `PhraseComposee`
--
ALTER TABLE `PhraseComposee`
  ADD CONSTRAINT `FK_771529AA3EC69B92` FOREIGN KEY (`ref_niveau`) REFERENCES `Niveau` (`id`),
  ADD CONSTRAINT `FK_771529AA41AE645B` FOREIGN KEY (`ref_enfant`) REFERENCES `Enfant` (`id`);

--
-- Contraintes pour la table `Pictogramme`
--
ALTER TABLE `Pictogramme`
  ADD CONSTRAINT `FK_B63C9910D7528EF5` FOREIGN KEY (`ref_phrase`) REFERENCES `PhraseProposee` (`id`),
  ADD CONSTRAINT `pictogramme_ibfk_1` FOREIGN KEY (`ref_mot`) REFERENCES `Mot` (`id`);

