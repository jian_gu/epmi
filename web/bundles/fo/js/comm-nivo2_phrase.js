
    $(function() {
    // there's the gallery and the box
        var $gallery = $("#gallery"),
                $box = $("#box");
        // let the gallery items be draggable
        $(".gallery li").draggable({
            cancel: "a.ui-icon", // clicking an icon won't initiate dragging
            revert: "invalid", // when not dropped, the item will revert back to its initial position
            containment: ".content",
            helper: "clone",
            cursor: "move",
            snap: true,
            snapMode: 'inner',
            drag: function() {
                if ($(this).parent().parent().attr('id') === 'box') {
                    galleryDroppable($(this).attr('themeid'));
                }
            },
            stop: function() {
                // when the drop movement is done, however the galery container is not visible -- wrong theme
                if ($('#div-theme-toggle-' + $(this).attr('themeid')).css('display') === 'none') {
                    playSentence('No, essaie encore.');
                }

            }
        });
        // make the box be droppable, accepting the gallery items
        $box.droppable({
            accept: ".theme-pictos > .gallery > li",
            activeClass: "ui-state-highlight",
            snap: true,
            snapMode: 'inner',
            drop: function(event, ui) {
                dropPicto(ui.draggable);
            }
        });
        /**
         * make the gallery be droppable as well, accepting items from the box
         * @param {type} $themeId
         * @returns {undefined}
         */
        function galleryDroppable($themeId) {
            $('div.theme-pictos ul.gallery[themeid="' + $themeId + '"]').droppable({
                activeClass: "ui-state-highlight",
                accept: "#box ul li[themeid='" + $themeId + "']",
                drop: function(event, ui) {
                    restorerPicto(ui.draggable);
                }
            });
        }
        ;
        /*
         * fonction mettre les pictos dans la box / la 2ere partie
         * @returns {undefined}
         */
        function dropPicto($item) {
            $item.fadeOut(function() {
                var $list = $("ul", $box).length ?
                        $("ul", $box) :
                        $("<ul class='gallery ui-helper-reset'  style='position:relative; z-index:1;'/>").appendTo($box);
                // Animation pour append
                $item.appendTo($list).fadeIn(function() {
                    $item
                            .animate({width: "auto"})
                            .find("img")
                            .animate({height: "120px"});
                    // desactiver le drag des pictos, 
                    $("ul#gallery li").draggable('disable');
                    // Recuperer tous les mots des items dans la box
                    var $textArray = '';
                    $textArray = $('#box > ul > li > .picto-unit > .picto-div > .picto-phrase').text();
                    // console.log($textArray);
                    $textArray = $.trim($textArray);
                    // Afficher le texte des pictos
                    /*$('#zone-text')
                     .find('p')
                     .html($textArray); */
                    $('#div-phrase-composee')
                            .find('p')
                            .html($textArray);
                });
            });
        }

        /*
         * Fonction restorer / re-mettre les pictos dans la 1ere partie
         * @returns {undefined}
         */
        function restorerPicto($item) {
            var themeId = $item.find('div').attr('themeId');
            //console.log(themeId);
            $item.fadeOut(function() {
                $item
                        .appendTo($('#div-theme-toggle-' + themeId + ' ul'))
                        .fadeIn();
                // Recuperer tous les mots des items dans la box
                var $textArray = '';
                $('#box > ul > li > .picto-unit > .picto-div > .picto-phrase').each(function() {
                    $textArray += ' ' + $(this).text();
                });
                // re-activer le drag des pictos, 
                $("ul#gallery li").draggable('enable');
                // Afficher le texte du picto
                /*$('#zone-text')
                 .find('p')
                 .html($textArray); */
                $('#div-phrase-composee')
                        .find('p')
                        .html($textArray);
                // activer le drag des pictos, 
                //$( "li", $gallery ).draggable('enable');

                // Emettre "Bravo" si le picto est bien remis
                playSentence('Bravo!');
            });
            
            // Clean text zone
//            $('#zone-text')
//                .find('p')
//                .html('');
        }

        // ============= Reading & highlight ===================
        $('#btn-play').click(function() {
            //// Copier texte vers la zone de lecture
//            $('#zone-text')
//                .find('p')
//                .html($.trim($('#div-phrase-composee').find('p').text()));
            var myPhrase = $('#div-phrase-composee').find('p').text();
            // desactiv le boutton play si un mot existe
            if(myPhrase.length !== 0) {
                // disable btn lirePhrase
                $('#btn-play').attr('disabled', true);
                $('#btn-play-phrase').attr('disabled', true);
            }
            audioPlay();
        });
        // ============= Read entaire sentence ===================
        $('#btn-play-phrase').click(function() {
            
            // Copier texte vers la zone de lecture
//            $('#zone-text')
//                    .find('p')
//                    .html($.trim($('#div-phrase-composee').find('p').text()));
            var myPhrase = $('#div-phrase-composee').find('p').text();
            // desactiv le boutton play si un mot existe
            if(myPhrase.length !== 0) {
                // disable btn lirePhrase
                $('#btn-play').attr('disabled', true);
                $('#btn-play-phrase').attr('disabled', true);
            }
            // Play audio, phrase entière
            playSentence(myPhrase);
        });
        // ============= sauvgarder phrase composée ===================
        $('#btn-save').click(function() {
            //// Copier texte vers la zone de lecture
            var myPhrase = $.trim($('#div-phrase-composee').find('p').text());
            if (!myPhrase.length) {
                //alert('Veuillez composer une phrase.');
                return;
            }
            savePhrase(myPhrase);
        });
        /*
         * Fonction de lire
         * @param {type} word
         * @returns {undefined}
         */
        function audioPlay() {
            // Diviser la phrase dans le textarea en mots
            var textArray = $('#div-phrase-composee p').text().split(" ");
            // Sil na pas de texte disponible, lancer une alerte et sarreter
            if (!$.trim(textArray).length) {
                //alert('Veuillez choisir un pictogramme');
                return;
            }
            //console.log(textArray);
            // Initier lelement audio (pas de balise "<audio>")
            //var audio = document.createElement('audio');
            var audio = new Audio();
            // Initier lindex mot (0 pour le primer)
            var index = 0;
            // Initier la duree de temps
            var timeSpan = 0;
            /*
             * Boucle de traitement des mots: LIRE, mettre en SURBRILLANCE
             * @param {type} word
             * @returns {undefined}
             */
            function myLoop() {
                // Boucler les action avec une pause indeterminee
                //setTimeout(function() {
                // Effacer tout surbrillance
                removeHighLight();
                // Return when reaches the end of array
                if (index === textArray.length) {
                    //console.log('-- reading finished --');

                    if ($('#btn-play').attr('disabled') === 'disabled') {
                        // active btn lirePhrase
                        $('#btn-play').removeAttr('disabled');
                    }
                    if ($('#btn-play-phrase').attr('disabled') === 'disabled') {
                        // active btn lirePhrase
                        $('#btn-play-phrase').removeAttr('disabled');
                    }

                    return;
                }

                // Recuperer un element(mot) de lArray
                var sWord = textArray[index];
                // Recuperer le longueur du mot (pour definir le temps)
                /*var sWordLength = sWord.length;
                 // Ici on va definir le temps de pronociation(time span) de mot
                 // Si le mot actuel est un non-chiffre
                 if (isNaN(sWord)) {
                 // ~12 characters will be read in 1 second
                 timeSpan = 500 * sWordLength / 6 + 800;
                 } else {
                 //pour les chiffre, on definit le temps: 1.8s
                 timeSpan = 1800;
                 }*/

                // Utiliser la methode de notre site, epecher denvoyer le referer header.
                audio.src = '/soundrequest?text_to_read=' + sWord;
                audio.type = 'audio/mpeg';
                // empecher denvoyer referer header
                audio.rel = 'noreferrer';
                audio.load();
                audio.play();
                // Mettre le mot actuel en surbrillance
                hightLightWord(sWord);
                // Quand un mot est lu, l'index s'incrémente, appele la function
                audio.onended = function() {
                    // incrementer lindex
                    index++;
                    myLoop();
                };
                /*// incrementer lindex
                 index++;
                 myLoop(); */
                //}, timeSpan);

            }
            myLoop();
        }

        /**
         * Sauvgarder la phrase composée
         * @param {type} myPhrase
         * @returns {undefined}         */
        function savePhrase(myPhrase) {
            $.ajax({
                url: "{{path('communication_save_phrase')}}",
                data: {
                    my_phrase: myPhrase
                },
                dataType: 'json',
                success: function(data)
                {
                    if (data.result === 'success') {
                        $('div#top-zone').prepend(
                                "<div class=\"alert alert-info fade in\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>Phrase enregistrée à {{'now'|date('H:i')}}.</div>"
                                );
                    }

                }
            });
        }

        /*
         * Mettre un mot en surbrillance
         * @param {type} word
         * @returns {undefined}
         */
        function hightLightWord(word) {
            // active the highlight
            $('#div-phrase-composee p').highlight(word, false);
        }

        /**
         * Effacer la surbrillance
         * @returns {undefined}         */
        function removeHighLight() {
            $('#div-phrase-composee p').removeHighlight();
        }


        // ============ part 2 ==============
        // vvvvvvvvvvvv ====== vvvvvvvvvvvvvv
        // Cacher le div pictos par default
        $('.theme-pictos').hide();
        var $visible = false;
        // Afficher les pictos quand cliquer sur un theme
        $("#ul-theme > li").click(function() {
            //$('#modal-pictos').modal('show');
            var currentThemeId = $(this).find('div').attr('itemid');
            console.log(currentThemeId);
            $('.theme-pictos').hide();
            $('#div-theme-toggle-' + currentThemeId).toggle('fast');
            return false;
        });
        /**
         * Lire une phrase entière
         * @param {type} $phrase
         * @returns {undefined}         */
        function playSentence($phrase) {
            var audio = new Audio();
            audio.src = '/soundrequest?text_to_read=' + $phrase;
            audio.type = 'audio/mpeg';
            // empecher denvoyer referer header
            audio.rel = 'noreferrer';
            audio.load();
            audio.play();
            // end of audio play
            audio.onended = function() {
                // re-active btn Lecture
                if ($('#btn-play').attr('disabled') === 'disabled') {
                    $('#btn-play').removeAttr('disabled');
                }
                if ($('#btn-play-phrase').attr('disabled') === 'disabled') {
                    $('#btn-play-phrase').removeAttr('disabled');
                }
            };
        }
        ;
    });
