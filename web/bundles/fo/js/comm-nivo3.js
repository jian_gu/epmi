/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * ********************************
 * **** Sortable is more complete than drag n drop. We use only sortable here ****
 * ********************************
 * ****  *****
 * ****  *****
 * ****  *****
 * ****  *****
 * ********************************
 */

$(function() {
// ======== Declaration of global variables for general elements =========
    /**
     * Liste voyelles
     * @type Array
     */
    var grammar_voyelles = ['a', 'e', 'i', 'o', 'u', 'y', 'é'],
            /**
             * Array pour nom du sujet
             * @type Array
             */
            grammar_sujets = ['sujet', 'sujets', 'Sujet', 'Sujets'],
            /**
             * Nom du groupe demande pour verbe part1
             * @type Array
             */
            grammar_v_demandes = ['demande', 'demandes', 'Demande', 'Demandes'],
            /**
             * Nom du groupe demande pour verbe part2
             * @type Array
             */
            grammar_v_actions = ['action', 'actions', 'Action', 'Actions'],
            /**
             * Array du nom du verbe AVOIR, peut utiliser pour détecter l'auxillaire
             * @type Array
             */
            grammar_v_avoir = ['avoir', 'Avoir'],
            /**
             * Array du nom du verbe ÊTRE, peut utiliser pour détecter l'auxillaire
             * @type Array
             */
            grammar_v_etre = ['etre', 'Etre', 'être', 'Être'],
            /**
             * Array pour nom des chiffres
             * @type Array
             */
            grammar_chiffres = ['chiffre', 'chiffres', 'Chiffre', 'Chiffres'],
            /**
             * Picto fade time in milsecond. 0 = fast
             * @type Integer
             */
            fadeSpeed = 0,
            /**
             * Picto & Picto Place holder size in px: [width height]
             * @type Array
             */
            pictoPlaceHolderSize = [142, 160],
            /**
             * Sentence to be played: Essayer encore
             * @type String
             */
            retrySentence = 'Non, essaie encore.',
            /**
             * Sentence to be played: Bravo
             * @type String
             */
            bravoSentence = 'Bravo!!',
            /**
             * Sentence for Help Button
             * @type String
             */
            aidezMoiSentence = 'Aidez-moi!';
// ======== Declaration of global variables for general elements =========
    /**
     * Elements that can be draggad
     * @type type
     */
    var $dragElement = $("ul.gallery > li"),
            /**
             * Emplacement où les pictos seront mis
             * @type type
             */
            $dropZonePhrase = $("#box"),
            /**
             * zone texte pour la phrase composée
             * @type type
             */
            $textePhraseComposee = $("div#div-phrase-composee > p#p-text-sentence"),
            /**
             * Theme id for Current showed pictos under one theme
             * @type Number
             */
            currentShowedTheme = 0,
            /**
             * Indicator button help: clicked 1, unclicked 0.
             * @type Integer
             */
            btnHelpClicked = 0;
    // ======= Declaration of gloabal variables for MOVEMENT =========
    /**
     * Sort indicator, 0: no sorting; 1: sorting.
     * @type Number
     */
    var move_sort = 0,
            /**
             * Drop indicator, 0: not being dragged or drapped; 1: dragging.
             * @type Number
             */
            move_drag_drop = 0,
            /**
             * Restore indicator, 0: no picto restored; 1: a picto is restored
             * @type Number
             */
            move_restore = 0;
    // ======= Declaration of gloabal variables for GRAMMAR =========
    /**
     * Is there a sujet in the sentence
     * @type Number
     */
    var grammar_hasSujet = 0,
            /**
             * Verbe counter DEMANDE
             * @type Number
             */
            grammar_hasVerbeD = 0,
            /**
             * Verbe counter ACTION
             * @type Number
             */
            grammar_hasVerbeA = 0,
            /**
             * Indicateur personne pour le sujet
             * @type Number
             */
            grammar_personne = 0,
            /**
             * Indicateur de conjuguation, 1: verbe conjuguée
             * @type Boolean
             */
            grammar_conjuguer = false,
            /**
             * Valeur du première verbe après le sujet
             * @type String
             */
            grammar_firstVerbe = '',
            /**
             * Indicateur de la genre du complément
             * @type Number
             */
            grammar_genreComplement = 0,
            /**
             * Array of verbes, length: 0 to 1
             * @type Array
             */
            grammar_verbesArray = [],
            /**
             * Chiffre est utilisée
             * @type Boolean
             */
            grammar_numberUsed = false,
            /**
             * Combient de complement se présent
             * @type Number
             */
            grammar_numComplement = 0,
            /**
             * Négation pr le 1er verbe
             * @type Integer
             */
            grammar_negation = 0;

    //**************************************
    //***** Functions should be called *****
    //**************************************


    // Call & run Functions
    themePictosToggle(); // Call function deal with click on theme

    pictoPhraseSortable(); // Call function deal with picto dropping in the zone for sentence, use sorting
    pictoThemeSortable(); // Call function deal with picto restoring useing SORTING method
    setStylesForSortable(); // Make sure use correct style for sorting (correction js bugs...)
    // themeIconDroppable();



    //*****************************
    //**** "Dynamic" functions ****
    //*****************************

    // ===========================
    // ==== Principal actions ====
    /**
     * Sortable pictos in the sentence zone
     * @returns {undefined}
     */
    function pictoPhraseSortable() {
        /**
         * define the action: sort or restore
         * @type String
         */
        var action = '';
        /**
         * the theme id of a picto restored
         * @type Number
         */
        var receiverThemeId = 0;
        /**
         * the theme name of a picto restored
         * @type String
         */
        var receiverThemeName = '';
        /**
         * 
         * @type element
         */
        var placeHolder;
        $('div#box > ol.gallery').sortable({
            items: "li",
            containment: "div.panel-body",
            helper: 'clone',
            placeholder: "ui-state-highlight",
            cursor: "-webkit-grabbing",
            tolerance: 'pointer',
            connectWith: "ul.gallery",
            start: function(event, ui) {
                console.log('sort starts');
                setStylesForSortable();
            },
            // connected lists concern
            activate: function(event, ui) {
                console.log('sort activate');
            },
            sort: function(event, ui) {
                setStylesForSortable();
            },
            // 
            beforeStop: function(event, ui) {
                console.log('sort beforeStop');
                if (placeHolder.closest('div').attr('id') === 'box') {
                    action = 'sort';
                } else if ($.inArray('theme-pictos', placeHolder.closest('div').attr('class'))) {
                    action = 'restore';
                    receiverThemeId = placeHolder.closest('div').attr('themeId');
                    receiverThemeName = placeHolder.closest('div').attr('themeName');
                }
            },
            stop: function(event, ui) {

                switch (action) {
                    case 'sort':
                        console.log('sort stop sorting');
                        break;
                    case 'restore':
                        console.log('sort stop restoring');
                        // receiver's theme
                        pictoMoved(ui.item, receiverThemeId, receiverThemeName, 'restore');
                        // break;
                    default:
                        break;
                }
                // reactive help button
                btnHelpClicked = 0;
                // reset variables
                action = '';
                placeHolder;
                receiverThemeId = 0;
                receiverThemeName = '';
                // Detect sujet
                grammar_hasSujet = hasSujet();
                // Detect verbe demande
                grammar_hasVerbeD = hasVerbeDemande();
                // Detect verbe action
                grammar_hasVerbeA = hasVerbeAction();
                // loop all pictos in the drop zone
                updateLoopingPictos();
                // Re-formuler la phrase
                formulateSentence();
            },
            deactivate: function(event, ui) {
                console.log('sort deactivate');
            },
            change: function(event, ui) {
                placeHolder = ui.placeholder;
                console.log('sort change');
            },
            create: function() {
                console.log('sort create');
            },
            out: function() {
                console.log('sort out');
            },
            over: function() {
                console.log('sort over');
            },
            receive: function() {
                console.log('sort receive');
            },
            remove: function() {
                console.log('sort remove -- restoring');
            },
            // order changed, here we may call the function(s) concern the grammar
            update: function(event, ui) {
                console.log('sort update');
                // we do something here for grammar
            }
        });
    }


    /**
     * Make the theme-pictos area sortable (we intend to use drop, but as the 
     * sort is forced, a rewards drag n drop will not word), inorder to receive
     * pictos from the under part (zone phrase)
     * @returns {undefined}
     */
    function pictoThemeSortable() {
        var action = '';
        var receiverThemeId = 0;
        var receiverThemeName = '';
        var placeHolder;
        $('div.theme-pictos > ul.gallery').sortable({
            items: "li",
            containment: "div.panel-body",
            helper: "clone",
            placeholder: "ui-state-highlight",
            cursor: "-webkit-grabbing",
            tolerance: 'pointer',
            connectWith: "ol.gallery",
            start: function(event, ui) {
                setStylesForSortable();
            },
            activate: function() {
                console.log('restore activate');
            },
            sort: function(event, ui) {
                console.log('restoring...');
                setStylesForSortable();
            },
            over: function() {
                console.log('restore over');
            },
            change: function(event, ui) {
                placeHolder = ui.placeholder;
            },
            beforeStop: function(event, ui) {
                if (placeHolder.closest('div').attr('id') === 'box') {
                    action = 'drop';
                    receiverThemeId = 0;
                    receiverThemeName = '';
                    // pictoMoved(ui.item, ui.placeholder, 'dropNSort');
                } else if ($.inArray('theme-pictos', placeHolder.closest('div').attr('class'))) {
                    action = 'moveInTheme';
                }
            },
            update: function() {
            },
            stop: function(event, ui) {
                switch (action) {
                    case 'drop':
                        console.log('sort stop dropping');
                        pictoMoved(ui.item, receiverThemeId, receiverThemeName, 'dropNSort');
                        // Play a sound when a picto is dropped into
                        playSimpleSound('sounds-1064-base.mp3');
                        break;
                    case 'moveInTheme':
                        console.log('sort stop internal moving');
                        break;
                    default:
                        break;

                }
                // reactive help button
                btnHelpClicked = 0;
                // reset variables
                action = '';
                placeHolder;
                receiverThemeId = 0;
                receiverThemeName = '';
                // detect sujet
                grammar_hasSujet = hasSujet();
                // Detect verbe demande
                grammar_hasVerbeD = hasVerbeDemande();
                // Detect verbe action
                grammar_hasVerbeA = hasVerbeAction();
                // loop all pictos in the drop zone
                updateLoopingPictos();
                // Re-formuler la phrase
                formulateSentence();
            }
        });
    }

//    /**
//     * Function allows restoring picto by dropping it on its theme icon
//     * @returns {undefined}
//     */
//    function themeIconDroppable() {
//        $("ul#ul-theme li", "div.panel-body").droppable({
//            // accept: "ol.gallery > li",
//            hoverClass: "ui-state-hover",
//            over: function() {
//                // Deactivate sort
//                $('div#box > ol.gallery').sortable('disable');
//            },
//            out: function() {
//                // Enable sort
//                $('div#box > ol.gallery').sortable('enable');
//            },
//            drop: function(event, ui) {
//                var $item = $(this);
//                var $themeId = $item.attr('themeId');
//
//
//                var $list = $('div#themes-pictos-pool > div.theme-pictos[themeId = ' + $themeId + ']')
//                        .find('ul.gallery[themeId = ' + $themeId + ']');
//
//                ui.draggable.hide(fadeSpeed, function() {
//                    $(this).appendTo($list).show(fadeSpeed);
//                });
//            }
//        });
//    }

    // =================================================
    // ==== Functions support the principal actions ====


    /**
     * Function deals with picto movements: DROP or RESTORE
     * @param {type} $itemFrom
     * @param {type} $themeIdTo
     * @param {type} $themeNameTo
     * @param {type} $action
     * @returns {undefined}
     */
    function pictoMoved($itemFrom, $themeIdTo, $themeNameTo, $action) {

        var $themeIdFrom = $itemFrom.find('div').attr('themeId');
        var $themeNameFrom = $itemFrom.find('div').attr('themeName');
        var $themeIdTo = $themeIdTo;
        var $themeNameTo = $themeNameTo;

        // Treatement for dropping a picto
        if ($action === "dropNSort") {
            aPictoIsDropped($themeIdFrom, $themeNameFrom);
        }
        if ($action === "restore") {
            aPictoIsRestored($themeIdFrom, $themeIdTo, $themeNameTo);
        }

    }


    /**
     * Actions to perform according to picto RESTORED
     * @param {type} themeIdFrom
     * @param {type} themeIdTo
     * @param {type} themeNameTo
     * @returns {undefined}
     */
    function aPictoIsRestored(themeIdFrom, themeIdTo, themeNameTo) {
        // If the container to which the picto is going to be dropped has an attribute
        // "themeId", and the themeId not equals to that of the picto, that's
        // means the picto not belongs to this theme, we do a reverse
        if (themeIdTo && themeIdFrom !== themeIdTo) {
            $('div#box > ol.gallery').sortable('cancel');
            // play the sound "non essaie encore"
            playSentence(retrySentence);
        } else if (themeIdTo && themeIdFrom === themeIdTo) {

            // If the picto is a >>>SUJET<<< and the sujet pool is disabled, we re-enable it
            if ($.inArray(themeNameTo, grammar_sujets) !== -1) {
                $('#div-theme-pictos-toggle-' + themeIdTo)
                        .find('ul[themeid="' + themeIdTo + '"]')

                        .removeClass('ui-state-disabled');
            }
            if ($.inArray(themeNameTo, grammar_v_demandes) !== -1) {
                $('#div-theme-pictos-toggle-' + themeIdTo)
                        .find('ul[themeid="' + themeIdTo + '"]')

                        .removeClass('ui-state-disabled');
            }
            if ($.inArray(themeNameTo, grammar_v_actions) !== -1) {
                $('#div-theme-pictos-toggle-' + themeIdTo)
                        .find('ul[themeid="' + themeIdTo + '"]')

                        .removeClass('ui-state-disabled');
            }


            // play the sound "bravo"
            playSentence(bravoSentence);
        }

    }

    /**
     * Actions to perform according to picto DROPPED
     * @param {type} themeIdFrom
     * @param {type} themeNameFrom
     * @returns {undefined}
     */
    function aPictoIsDropped(themeIdFrom, themeNameFrom) {

        // Desactiver le drag des pictos, si le theme du picto est >>>SUJET<<<
        if (grammar_hasSujet !== 0 && $.inArray(themeNameFrom, grammar_sujets) !== -1) {
            // Set global variable for sujet(1, 2, 3, 4, 5, 6)
            // grammar_hasSujet = $.trim($item.find('.picto-div > .picto-personne').text());
            // Disable the drag of other sujets once a sujet is selected
            $('div#div-theme-pictos-toggle-' + themeIdFrom)
                    .find('ul[themeId="' + themeIdFrom + '"]')
                    .sortable().sortable('cancel');
        }
        // S'il y a déjà un verbe >>>DEMANDE<<< dans la drop zone
        if (grammar_hasVerbeD !== 0 && $.inArray(themeNameFrom, grammar_v_demandes) !== -1) {
            // 
            $('div#div-theme-pictos-toggle-' + themeIdFrom)
                    .find('ul[themeId="' + themeIdFrom + '"]')
                    .sortable().sortable('cancel');
        }
        // S'il y a déjà un verbe >>>ACTION<<< dans la drop zone
        if (grammar_hasVerbeA !== 0 && $.inArray(themeNameFrom, grammar_v_actions) !== -1) {
            // 
            $('div#div-theme-pictos-toggle-' + themeIdFrom)
                    .find('ul[themeId="' + themeIdFrom + '"]')
                    .sortable().sortable('cancel');
        }
    }

    function updateLoopingPictos() {
        // meta value which will be assigned to the text zone (String)
        var metaPhrase = '';
        $('ol.gallery > li.ui-widget-content').each(function(index, element) {

            if ($(element).find('img')) {
                var themeId = $(element).find('div').attr('themeId');
                var themeName = $(element).find('div').attr('themeName');
                var motId = $(element).find('div.picto-unit').attr('motId');
                var mot = $(element).find('div').find('p.picto-mot').text();

                var previousPicto = $(element).prev('li.ui-widget-content');
                var previousPictoThemeName = previousPicto.find('div').attr('themeName');
                var previousPictoMotId = previousPicto.find('div.picto-unit').attr('motId');

                var nextPicto = $(element).next('li.ui-widget-content');
                var nextPictoThemeName = nextPicto.find('div').attr('themeName');
                var nextPictoMotId = nextPicto.find('div.picto-unit').attr('motId');
                // there is / are pictos in the phrase zone
                eachPicto();
            }

            function eachPicto() {
                if ($.inArray(themeName, grammar_sujets) !== -1) {
                    // this one is a >>>SUJET<<<
                    metaPhrase += '<span class="sujet" id="' + motId + '" personne=' + grammar_hasSujet + '>' + mot + ' </span>';
                } else if ($(element).find('div').find('p.picto-mot-conjugue').text().length > 0) {
                    // this is a >>>VERBE<<<
                    var arrayConjug = $(element).find('div').find('p.picto-mot-conjugue').text().split(',');
                    // Test if it's the >>>FIRST VERBE<<< here: mot précédent est un sujet
                    if ($.inArray(previousPictoThemeName, grammar_sujets) !== -1) {
                        sujetGenreInt = parseInt(previousPicto.find('div').find('p.picto-personne').text());
                        motConjug = arrayConjug[sujetGenreInt - 1];
                        metaPhrase += '<span class="verbe" id="' + motId + '" first-verbe="1">' + motConjug + ' </span>';
                        // Reset the négation indicator to 0, incase the négation was already used
                        grammar_negation = 0;
                    } else if ($.inArray(previousPicto.find('p.picto-mot').text(), grammar_v_avoir) !== -1
                            || $.inArray(previousPicto.find('p.picto-mot').text(), grammar_v_etre) !== -1) {
                        // This is a second verb, and it should use its form PASSÉ COMPOSÉ (after voir or être)
                        if ($(element).find('div').find('p.picto-mot-conjugue-passe-compose').text().length > 0) {
                            motPC = $.trim($(element).find('div').find('p.picto-mot-conjugue-passe-compose').text());
                            metaPhrase += '<span class="verbe" id="' + motId + '" passe-compose="1">' + motPC + ' </span>';
                        }
                    } else {
                        metaPhrase += '<span class="verbe" id="' + motId + '">' + mot + ' </span>';
                    }
                } else if ($(element).find('div').find('p.picto-mot-genre-complement').text().length > 0) {
                    // this is a >>>COMPLÉMENT<<<
                    var genreInt = parseInt($(element).find('div').find('p.picto-mot-genre-complement').text());
                    // if there is a number before it, and the number la larger than 1:
                    if (parseInt($.trim(previousPicto.find('p').text())) > 1) {
                        // form pluriel is defined
                        if ($(element).find('div').find('p.picto-mot-genre-complement-pluriel').text().length > 0) {
                            motPlur = $.trim($(element).find('div').find('p.picto-mot-genre-complement-pluriel').text());
                            metaPhrase += '<span class="complement" id="' + motId + '" genre=' + genreInt + ' pluriel ="1">' + motPlur + ' </span>';
                        }
                    } else {
                        metaPhrase += '<span class="complement" id="' + motId + '" genre=' + genreInt + '>' + mot + ' </span>';
                    }
                } else if ($(element).find('div').find('p.picto-mot-adj').text().length > 0) {
                    // this is an >>>ADJECTIF<<<
                    var arrayAdj = $(element).find('div').find('p.picto-mot-adj').text().split(',');
                    // Test if there is a complément before or after this adjectif
                    if (nextPicto.find('div').find('p.picto-mot-genre-complement').text().length > 0) {
                        // there is a complément AFTER
                        var genreInt = parseInt(nextPicto.find('div').find('p.picto-mot-genre-complement').text());
                        motAdj = arrayAdj[genreInt - 1];
                        metaPhrase += '<span class="adjectif" id="' + motId + '" genre=' + genreInt + '>' + motAdj + ' </span>';
                    }
                    else if (previousPicto.find('div').find('p.picto-mot-genre-complement').text().length > 0) {
                        // there is a complément BEFORE
                        var genreInt = parseInt(previousPicto.find('div').find('p.picto-mot-genre-complement').text());
                        motAdj = arrayAdj[genreInt - 1];
                        metaPhrase += '<span class="adjectif" id="' + motId + '" genre=' + genreInt + '>' + motAdj + ' </span>';
                    }
                    else {
                        metaPhrase += '<span class="adjectif" id="' + motId + '">' + mot + ' </span>';
                    }

                } else {
                    metaPhrase += '<span class="normal" id="' + motId + '">' + mot + ' </span>';
                }

                // Get Nv3 >>>AUTO-FILL<<< word
                if (previousPictoMotId) {
                    nv3AutoFillWord = getNv3AutoFill(previousPictoMotId, motId);
                    console.log('-1');
                    // if the auto-fill not triggered,
                    // because there is an other word between these two word
                    var autoFillWord = $('div#div-phrase-composee > p#p-text-sentence')
                            .find('span.auto-fill').text();
                    
                    if (!autoFillWord && !$.trim(autoFillWord).length > 0) {
                        console.log('--1')
                        var prePreviousPicto = $(element)
                                .prev('li.ui-widget-content')
                                .prev('li.ui-widget-content');
                        if (prePreviousPicto) {
                            var prePreviousPictoMotId = prePreviousPicto
                                    .find('div.picto-unit')
                                    .attr('motId');
                            nv3AutoFillWord = getNv3AutoFill(prePreviousPictoMotId, motId);
                        }
                    }
                }


            }

        });
        // show the phrase faite dans la zone text
        $textePhraseComposee.html(metaPhrase);
    }
    /**
     * Function tests if there is a >>>SUJET<<< in the picto drop zone
     * @returns {integer}
     */
    function hasSujet() {
        var sujetExiste = 0;
        $('ol.gallery > li.ui-widget-content').each(function(index, element) {
            if ($(element).find('img')) {
                var themeId = $(element).find('div').attr('themeId');
                var themeName = $(element).find('div').attr('themeName');

                if ($.inArray(themeName, grammar_sujets) !== -1) {
                    var personne = $(element).find('p.picto-personne').text();
                    sujetExiste = parseInt(personne);
                    $('div#div-theme-pictos-toggle-' + themeId)
                            .find('ul[themeId="' + themeId + '"]')
                            .addClass("ui-state-disabled");
                } else {
                    sujetExiste = sujetExiste !== 0 ? sujetExiste : 0;
                    $('div#div-theme-pictos-toggle-' + themeId)
                            .find('ul[themeId="' + themeId + '"]')
                            .removeClass('ui-state-disabled');
                }
            } else {
                sujetExiste = sujetExiste !== 0 ? sujetExiste : 0;
                $('div#div-theme-pictos-toggle-' + themeId)
                        .find('ul[themeId="' + themeId + '"]')
                        .removeClass('ui-state-disabled');
            }
        });
        return sujetExiste;
    }

    /**
     * Function tests if there is a >>>DEMANDE<<< in the picto drop zone
     * @returns {integer}
     */
    function hasVerbeDemande() {
        var verbeDExiste = 0;
        $('ol.gallery > li.ui-widget-content').each(function(index, element) {
            if ($(element).find('img')) {
                var themeId = $(element).find('div').attr('themeId');
                var themeName = $(element).find('div').attr('themeName');

                if ($.inArray(themeName, grammar_v_demandes) !== -1) {
                    verbeDExiste = 1;
                    $('div#div-theme-pictos-toggle-' + themeId)
                            .find('ul[themeId="' + themeId + '"]')
                            .addClass('ui-state-disabled');
                } else {
                    verbeDExiste = verbeDExiste !== 0 ? verbeDExiste : 0;
                    $('div#div-theme-pictos-toggle-' + themeId)
                            .find('ul[themeId="' + themeId + '"]')
                            .removeClass('ui-state-disabled');
                }
            } else {
                verbeDExiste = verbeDExiste !== 0 ? verbeDExiste : 0;
                $('div#div-theme-pictos-toggle-' + themeId)
                        .find('ul[themeId="' + themeId + '"]')
                        .removeClass('ui-state-disabled');
            }
        });
        return verbeDExiste;
    }

    /**
     * Function tests if there is a >>>ACTION<<< in the picto drop zone
     * @returns {Integer}
     */
    function hasVerbeAction() {
        var verbeAExiste = 0;
        $('ol.gallery > li.ui-widget-content').each(function(index, element) {
            if ($(element).find('img')) {
                var themeId = $(element).find('div').attr('themeId');
                var themeName = $(element).find('div').attr('themeName');

                if ($.inArray(themeName, grammar_v_actions) !== -1) {
                    verbeAExiste = 1;
                    $('div#div-theme-pictos-toggle-' + themeId)
                            .find('ul[themeId="' + themeId + '"]')
                            .addClass('ui-state-disabled');
                } else {
                    // no action verb exists, remove style
                    verbeAExiste = verbeAExiste !== 0 ? verbeAExiste : 0;
                    $('div#div-theme-pictos-toggle-' + themeId)
                            .find('ul[themeId="' + themeId + '"]')
                            .removeClass('ui-state-disabled');
                }
            } else {
                verbeAExiste = verbeAExiste !== 0 ? verbeAExiste : 0;
                $('div#div-theme-pictos-toggle-' + themeId)
                        .find('ul[themeId="' + themeId + '"]')
                        .removeClass('ui-state-disabled');
            }
        });
        return verbeAExiste;
    }

    /**
     * Function treats with the négation
     * @param {Ingeger} $indicator
     * @returns {undefined}
     */
    function doNegation($indicator) {
        var ne = '<span class="negation">ne </span>',
                pas = '<span class="negation">pas </span>',
                firstVerb = $('p#p-text-sentence').find('span[first-verbe=1]');
        switch ($indicator) {
            // button négation is clicked, and the indicator shows TRUE
            case 1:
                // If in the sentence there is <j'>
                if ($('p#p-text-sentence').has('span.sujet')
                        && $.trim($('p#p-text-sentence > span.sujet').text()) === 'j\'') {
                    $('p#p-text-sentence > span.sujet').text('je ');
                }
                // wrap the verb with "ne" et "pas"
                $(ne).insertBefore(firstVerb);
                $(pas).insertAfter(firstVerb);
                break;
                // button négation is clicked, and the indicator show FALSE
            case 0:
                $('p#p-text-sentence').find('span.negation').remove();
                break;
        }
    }

    function formulateSentence() {
        // If there isn't any word in the zone, abort
        if (!$('p#p-text-sentence').text().length > 0) {
            return;
        }
        // Call thoses functions deal with different type of apostrophe
        // Iterate all the word node in the zone (spans)
        $('p#p-text-sentence').find('span').each(function(index, element) {
            var nextElement = $(element).next('span'),
                    firstWordNextElement = nextElement.text().split('')[0];
            // If there is "Je + voyelle"
            if ($(element).attr('class') === 'sujet'
                    && $.trim($(element).text()) === 'je'
                    && $.inArray(firstWordNextElement, grammar_voyelles) !== -1) {
                // utilise l'apostrophe
                $(element).text('j\'');
            }
            // If there is "ne + voyelle"
            if ($(element).attr('class') === 'negation'
                    && $.trim($(element).text()) === 'ne'
                    && $.inArray(firstWordNextElement, grammar_voyelles) !== -1) {
                // utilise l'apostrophe
                $(element).text('n\'');
            }
            // Si la fin d'un mot contient une apostrophe lui même
            if ($(element).text().slice(-2) === '\' '
                    && $(element).text().slice(-1) === ' ') {
                // supprimer l'espace
                $(element).text($.trim($(element).text()));
            }
        });
    }

    /**
     * Function replace 1 par une pour la lecture si le complément est un mot féminin singulier
     * @param {type} $phrase
     * @returns {unresolved}
     */
    function lireUnePrFemininSingulier($phrase) {
        // get drop-zone elements array
        var phrasePictoArray = $('div#box > ol.gallery > li'),
                number1exist = 0,
                femininSingulier = 0;

        // Iterate all pictos in the drop-zone
        phrasePictoArray.each(function(index, element) {
            // dectect if there is numbre: 1
            if ($.inArray($(element).find('.picto-unit').attr('themename'), grammar_chiffres) !== -1
                    && $.trim($(element).find('p.picto-mot').text()) === '1') {
                number1exist = 1;
            }
            // detect if there is a complément before or after number 1
            // 1 ms, 2 mp, 3 fs, 4 fp
            if ($(element).next().find('p.picto-mot-genre-complement').text() === '3') {
                femininSingulier = 1;
            }
        });

        // return re-formed sentence
        if (number1exist === 1 && femininSingulier === 1) {
            return $phrase.replace('1', 'une');
        } else {
            return $phrase;
        }
    }

    //****************************
    //**** "Stable" functions ****
    //****************************

    /**
     * Function deal with hiding and display of pictos when click on a theme
     * @returns {undefined}
     */
    function themePictosToggle() {
        // Hide the pictos under one theme by default
        $('.theme-pictos').hide();
        // Afficher les pictos quand cliquer sur un theme
        $("#ul-theme > li").click(function() {
            // get current theme id
            var currentThemeId = $(this).find('div').attr('itemid');
            if (currentShowedTheme === currentThemeId) {
                currentShowedTheme = 0;
            }
            else {
                currentShowedTheme = currentThemeId;
                $('.theme-pictos').hide();
            }
            $('#div-theme-pictos-toggle-' + currentThemeId).toggle(400);
            return false;
        });
    }
    ;
    /*
     * Fonction de lire
     * @param {type} word
     * @returns {undefined}
     */
    function playWordByWord($phrase) {
        // Do the replacement of number 1 with une
        var $texts = $phrase;

        // Diviser la phrase dans le textarea en mots
        var textString = $.trim($texts).split(" ");

        // Sil na pas de texte disponible, lancer une alerte et sarreter
        if (textString.length <= 0) {
            //alert('Veuillez choisir un pictogramme');
            return;
        }
        //console.log(textString);
        // Initier lelement audio (pas de balise "<audio>")
        //var audio = document.createElement('audio');
        var audio = new Audio();
        // Initier lindex mot (0 pour le primer)
        var index = 0;
        // Call the loop function
        myLoop();


        /*
         * Boucle de traitement des mots: LIRE, mettre en SURBRILLANCE
         * @param {type} word
         * @returns {undefined}
         */
        function myLoop() {
            // Effacer tout surbrillance
            removeHighLight();
            // Return when reaches the end of array
            if (index === textString.length) {
                //console.log('-- reading finished --');
                if ($('#btn-play-words').attr('disabled') === 'disabled') {
                    // active btn lirePhrase
                    $('#btn-play-words').removeAttr('disabled');
                }
                if ($('#btn-play-phrase').attr('disabled') === 'disabled') {
                    // active btn lirePhrase
                    $('#btn-play-phrase').removeAttr('disabled');
                }
                return;
            } else {
                // Recuperer un element(mot) de lArray
                var sWord = textString[index];

                // Utiliser la methode de notre site, epecher denvoyer le referer header.
                audio.src = '/soundrequest?text_to_read=' + sWord;
                audio.type = 'audio/mpeg';
                // empecher denvoyer referer header
                audio.rel = 'noreferrer';
                audio.load();
                audio.play();
                // Mettre le mot actuel en surbrillance
                hightLightWord(sWord);
                // Quand un mot est lu, l'index s'incrémente, appele la function
                audio.onended = function() {
                    // incrementer lindex
                    index += 1;
                    // loop this function
                    myLoop();
                };
            }
        }
    }

    /**
     * Lecture d'une phrase entière
     * @param {type} $phrase
     * @returns {undefined}          */
    function playSentence($phrase) {
        var audio = new Audio();
        audio.src = '/soundrequest?text_to_read=' + $phrase;
        audio.type = 'audio/mpeg';
        // empecher denvoyer referer header
        audio.rel = 'noreferrer';
        audio.load();
        audio.play();
        // end of audio play
        audio.onended = function() {
            // re-active btn Lecture
            if ($('#btn-play').attr('disabled') === 'disabled') {
                $('#btn-play').removeAttr('disabled');
            }
            if ($('#btn-play-phrase').attr('disabled') === 'disabled') {
                $('#btn-play-phrase').removeAttr('disabled');
            }
        };
    }
    ;


    /**
     * Function plays simple sound
     * @param {string} $soundFile
     * @returns {undefined}
     */
    function playSimpleSound($soundFile) {
        var audio = new Audio();
        audio.src = '/uploads/sounds/' + $soundFile;
        audio.type = 'audio/mpeg';
        // empecher denvoyer referer header
        audio.rel = 'noreferrer';
        audio.load();
        audio.play();
    }


    /*
     * Mettre un mot en surbrillance
     * @param {type} word
     * @returns {undefined}
     */
    function hightLightWord(word) {
        // quand prononce 'une', hightlight '1'
        if ($.trim(word) === 'une' && $('#div-phrase-composee > p#p-text-sentence').find('span').text().indexOf('1')) {
            word = '1';
        }
        // active the highlight
        $('#div-phrase-composee > p#p-text-sentence').highlight(word, false);
    }
    ;

    /**
     * Enlever la surbrillance
     * @returns {undefined}
     */
    function removeHighLight() {
        $('#div-phrase-composee p').removeHighlight();
    }
    ;


    /**
     * function ranger tous
     * @returns {undefined}
     */
    function revertAllPictos() {
        // Si il y en n'a pas de picto dans la drop-zone
        if ($('#box > ol > li.ui-widget-content').length < 1) {
            // And s'il n'y pas de mot dans la texte zone, return
            if ($('p#p-text-sentence').children().length < 1) {
                return;
            }
            // Vider la zone texte
            $textePhraseComposee.empty();
        }

        // Restaurer tous les sortable de la zone droppable vers leurs thèmes
        $('ol.gallery > li.ui-widget-content').each(function() {
            $themeId = $(this).attr('themeId');
            $list = $('div#themes-pictos-pool > div.theme-pictos[themeId = ' + $themeId + ']')
                    .find('ul.gallery[themeId = ' + $themeId + ']');
            // Iterate all pictos dans la zone phrase and resotre them 
            $(this).fadeOut(fadeSpeed, function() {
                $(this).appendTo($list).fadeIn(fadeSpeed);
            });
        });

        // Envlever style css "disabled" des thèmes (we do not care which one)
        $('div#themes-pictos-pool >div.theme-pictos > ul.gallery').each(function() {
            $(this).removeClass('ui-state-disabled');
        });

        // Vider la zone texte
        $textePhraseComposee.empty();

        // Initiate all indicators for SUJET, VERBES
        grammar_hasSujet = 0;
        grammar_hasVerbeA = 0;
        grammar_hasVerbeD = 0;
        grammar_negation = 0;
        btnHelpClicked = 0;
    }

    /**
     * Function makes sure that the sort uses correct styles
     * @returns {undefined}
     */
    function setStylesForSortable() {
        // Modify style to recover js sort offset bug
        $('body').css('overflow-y', 'visible');

        // Highlight placeholder
        $('li.ui-state-highlight')
                .css('width', '' + pictoPlaceHolderSize[0] + 'px')
                .css('height', '' + pictoPlaceHolderSize[1] + 'px')
                .css('overflow', 'hide');
    }


    //**********************************
    //***** Button click Functions *****
    //**********************************
    //
    // ============= Help Button ===================
    $('a#btn-help').click(function() {
        // If the button is already clicke, return
        if (btnHelpClicked === 1) {
            return;
        } else {
            // set indicator to 1
            btnHelpClicked = 1;
            // clean all the pictos and phrase
            revertAllPictos();
            $('p#p-text-sentence').html('<span class="help-me">' + aidezMoiSentence + '</span>');
        }
    });

    // ============= Négation pr le premier verbe ===================
    $('button#btn-negation').click(function() {
        var firstVerbe = $('p#p-text-sentence').find('span.verbe').attr('first-verbe');
        if (firstVerbe && firstVerbe === '1') {
            switch (grammar_negation) {
                case 0:
                    grammar_negation = 1;
                    break;
                case 1:
                    grammar_negation = 0;
                    break;
            }
            // Call the function négation 
            doNegation(grammar_negation);
            // Re-formuler la phrase
            formulateSentence();
        } else {
            return;
        }
    });

    // ============= Reading & highlight ===================
    $('button#btn-play-words').click(function() {
        var myPhrase = $('#div-phrase-composee').find('p#p-text-sentence').text();
        // Si le texte est vide
        if (myPhrase.length <= 0) {
            return;
        }
        // desactiv le boutton play si un mot existe
        if (myPhrase.length > 0) {
            // disable btn lirePhrase
            $('#btn-play').attr('disabled', true);
            $('#btn-play-phrase').attr('disabled', true);

            // Do the replacement of number 1 with une
            myPhrase = lireUnePrFemininSingulier(myPhrase);
            // Plan audio
            playWordByWord(myPhrase);
        }
    });
    // ============= Read entaire sentence ===================
    $('button#btn-play-phrase').click(function() {
        var myPhrase = $('#div-phrase-composee').find('p#p-text-sentence').text();
        // Si le texte est vide
        if (myPhrase.length <= 0) {
            return;
        }
        // desactiv le boutton play si un mot existe
        if (myPhrase.length > 0) {
            // disable btn lirePhrase
            $('#btn-play').attr('disabled', true);
            $('#btn-play-phrase').attr('disabled', true);

            // Do the replacement of number 1 with une
            myPhrase = lireUnePrFemininSingulier(myPhrase);
            // Play audio, phrase entière
            playSentence(myPhrase);
        }
    });
    // ============= sauvgarder phrase composée ===================
    $('#btn-save').click(function() {
        //// Copier texte vers la zone de lecture
        var myPhrase = $.trim($('#div-phrase-composee').find('p').text());
        if (!myPhrase.length) {
            //alert('Veuillez composer une phrase.');
            return;
        }
        savePhrase(myPhrase);
    });
    // ============= Ranger tout pictogrammes ===================
    $('#btn-reset').click(function() {
        revertAllPictos();
    });
});
