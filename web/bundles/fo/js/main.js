$(document).ready(function() {
    $('#file-upload').find('label')
            .removeClass().addClass('btn btn-default btn-block')
            .html("<i class='fa fa-upload'></i> Séléctionner une image");
    $('#file-upload').find('input')
            .hide()
            .removeAttr('required')
            .change(function() {
                var uploadFileName = $('#file-upload').find('input').val().split('/').pop().split('\\').pop();
                if (uploadFileName.length > 0) {
                    $('#file-upload').find('label')
                            .html("<i class='fa fa-upload'></i> " + uploadFileName);
                }
            });

    $('#hide-show-menu')
            .css({
                "background-color": "#222",
                "border": "1px solid rgba(0,0,0,0.1)",
                "padding": "7px",
                "font-size": "1em",
                "cursor": "pointer",
                "display": "block"
            })
            .click(function() {
                $('.menu-enfant').slideToggle('slow');
                console.log("/web")
            });
    $('#ul-theme li').click(function() {
        var nextEl = $('#themes-pictos-pool');
        var pos = $(nextEl).offset();
        $('html,body').animate({
            scrollTop: pos.top - 50
        }, 1000);
        return false;
    });
    $('.sonata-user-show .cms-block-element').find('.list-group').removeClass().addClass('btn-group');
    $('.sonata-user-show .cms-block-element').find('.list-group-item').removeClass('list-group-item').addClass('btn btn-default');
});