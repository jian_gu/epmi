$(function() {
    /* 
     * To change this license header, choose License Headers in Project Properties.
     * To change this template file, choose Tools | Templates
     * and open the template in the editor.
     */

// there's the gallery and the box
    $box = $("#box"), // emplacement où les pictos seront mis
            $personne = 0, // Indicateur personne pour le sujet
            $conjuguer = 0, // Indicateur de conjuguation: 1 verbe conjuguée
            $firstVerbe = '', // valeur du première verbe après le sujet
            $genreComplement = 0, // Indicateur de la genre du complément
            $voyelles = ['a', 'e', 'i', 'o', 'u', 'y', 'é'], // Liste voyelles
            $activIndicator = 1, // 1: sujet, 2: verbes, 3: others
            $verbeCounterS = 0, // verbe counter SENTIMENT
            $verbeCounterA = 0, // verbe counter ACTION
            $verbesArray = [], // Array of verbes, length: 0 to 1
            $numberUsed = 0, // chiffre est utilisée
            $numComplement = 0;
    // make the gallery items be draggable
    $(".gallery li").draggable({
        cancel: "a.ui-icon", // clicking an icon won't initiate dragging
        revert: "invalid", // when not dropped, the item will revert back to its initial position
        containment: ".content",
        helper: "clone",
        cursor: "-webkit-grabbing",
        snap: true,
        snapMode: 'inner',
        drag: function() {
            if ($(this).parent().parent().attr('id') === 'box') {
                galleryDroppable($(this).attr('themeid'));
            }
        },
        stop: function() {
            // when the drop movement is done, however the galery container is not visible -- wrong theme
            if ($('#div-theme-toggle-' + $(this).attr('themeid')).css('display') === 'none') {
                playSentence('No, essaie encore.');
            }

        }
    });
    // make the box be droppable, accepting the gallery items
    $box.droppable({
        accept: ".theme-pictos > .gallery > li",
        activeClass: "ui-state-highlight",
        snap: true,
        snapMode: 'inner',
        drop: function(event, ui) {
            dropPicto(ui.draggable);
        }
    });
    // make the gallery be droppable as well, accepting items from the box
    function galleryDroppable($themeId) {
        $('div.theme-pictos ul.gallery[themeid="' + $themeId + '"]').droppable({
            activeClass: "ui-state-highlight",
            accept: "#box ul li[themeid='" + $themeId + "']",
            drop: function(event, ui) {
                restorerPicto(ui.draggable, true);
            }
        });
    }
    ;
    /*
     * fonction mettre les pictos dans la box / la 2ere partie
     * @returns {undefined}
     */
    function dropPicto($item) {
        $item.fadeOut(function() {
            var $themeId = $item.find('div').attr('themeId');
            var $themeName = $item.find('div').attr('themeName');
            var $list = $("ul", $box).length ?
                    $("ul", $box) :
                    // définir le style du ul afin de travailler avec bootstrap, et l'append to box
                    $("<ul class='gallery ui-helper-reset' id='ul-drop-box' style='position:relative; z-index:1;'/>").appendTo($box);
            // Animation pour append
            $item.appendTo($list).fadeIn(10, function() {
                $item
                        .animate({width: "auto"})
                        .find("img")
                        .animate({height: "120px"});

                // desactiver le drag des pictos, si le theme est SUJET
                if ($.inArray($themeName, ['sujet', 'sujets', 'Sujet', 'Sujets']) !== -1) {
                    $('#div-theme-toggle-' + $themeId + ' .gallery li').draggable('disable');
                }
                // desactiver le drag des pictos, si deux Verbes SENTIMENT sont déposées
                if ($.inArray($themeName, ['sentiment', 'sentiments', 'Sentiment', 'Sentiments']) !== -1) {
                    if ($verbeCounterS < 2) {
                        $verbeCounterS++;
                    }

                    if ($verbeCounterS === 2) {
                        $('#div-theme-toggle-' + $themeId + ' .gallery li').draggable('disable');
                    }
                }
                // desactiver le drag des pictos, si deux Verbes ACTION sont déposées
                if ($.inArray($themeName, ['action', 'actions', 'Action', 'Actions']) !== -1) {
                    if ($verbeCounterA < 2) {
                        $verbeCounterA++;
                    }

                    if ($verbeCounterA === 2) {
                        $('#div-theme-toggle-' + $themeId + ' .gallery li').draggable('disable');
                    }
                }

                // Si le picto est un sujet
                if ($.inArray($themeName, ['chiffre', 'chiffres', 'Chiffre', 'Chiffres']) !== -1) {
                    $numberUsed = 1;
                }

                // Si ce picto est un COMPLEMENT qui a une genre: soit masculin s/p; féminin s/p
                if ($item.find('.picto-div > .picto-mot-genre-complement').text()) {
                    $numComplement++;
                    $item.find('.picto-unit').attr('complement', $numComplement);
                    // Set l'identifier à la genre du complément, de sorte que on peut décider avec quel adjectif à utiliser
                    $genreComplement = $item.find('.picto-unit > .picto-div > .picto-mot-genre-complement').text();
                }

                // Recuperer tous les mots des items dans la box
                var $textString = '';
                // Traitement du picto dropped dans la partie inférieur
                $('#box > ul > li > .picto-unit').each(function(key, value) {
                    var isAdjectif = $(this).find('.picto-div > .picto-mot-adj').text(),
                            $parentArray = $('#box > ul > li > .picto-unit');
                    // Get le mot de ce pictogramme
                    var motPicto = $(this).find('.picto-div > .picto-mot').text();
                    // Si ce picto actuel porte un SUJET, c'est le quel; affecter le valeur à variable "personne"
                    if ($(this).find('.picto-div > .picto-personne').text()) {
                        $personne = $(this).find('.picto-div > .picto-personne').text();
                        $(this).attr('picto-personne', $personne);
                        var mot = motPicto;
                    }
                    // Si le picto actuel porte une VERBE, et c'est le PREMIERE VERBE, ajouter l'attribut "first-verbe"
                    if ($(this).find('.picto-div > .picto-mot-conjugue').text() && $conjuguer !== 1) {
                        $(this).attr('first-verbe', 1);
                        // Set l'identifiant à vrai, de sorte que on ne va pas conjuguer la deuxième et d'autres verbes
                        $conjuguer = 1;
                        $verbesArray[0] = $(this).attr('motId');
                    }
                    // Si le picto actuel porte une VERBE, mais c le DEUXIEME VERBE, ajouter l'attribut "second-verbe"
                    if ($(this).find('.picto-div > .picto-mot-conjugue').text() && $conjuguer === 1 && !$(this).attr('first-verbe')) {
                        $(this).attr('second-verbe', 1);
                        $verbesArray[1] = $(this).attr('motId');
                    }
                    // Si ce picto est le PREMIERE VERBE,conjuguer le selon la variable "personne"
                    if ($(this).attr('first-verbe')) {
                        var $arrayConjugue = $(this).find('.picto-div > .picto-mot-conjugue').text().split(',');
                        if ($personne !== 0) {
                            // Recuperer la bonne forme conjuguée
                            mot = $arrayConjugue[$personne - 1];
                        } else {
                            mot = motPicto;
                        }

                        // Affecter ce valeur à variable "firstVerb"
                        $firstVerbe = mot;
                        // cast <spand> afin d'utiliser le bouton négation
                        mot = '<span id=\'spn-fv\'>' + mot + '</span>';
                    }

                    // D'autres cas
                    else {
                        mot = motPicto;
                    }

                    // === we work here 08022014===

                    if (isAdjectif) {
                        var $arrayAdj = $(this).find('.picto-div > .picto-mot-adj').text().split(',');
                        // if this.prev is complément or this.next is complément 
                        // -- or this.prev is adj and this.prev.prev is complément --> si il y a un adverbe?
                        // -- or this.next is adj and this.next.next is complément --> si il y a un adverbe?
                        if ($($parentArray[key - 1]).find('.picto-div > .picto-mot-genre-complement').text()) {
                            $genreComplement = $($parentArray[key - 1]).find('.picto-div > .picto-mot-genre-complement').text();
                            mot = $arrayAdj[$genreComplement - 1];
                        }
                        if ($($parentArray[key + 1]).find('.picto-div > .picto-mot-genre-complement').text()) {
                            $genreComplement = $($parentArray[key + 1]).find('.picto-div > .picto-mot-genre-complement').text();
                            mot = $arrayAdj[$genreComplement - 1];
                        }
                    }

                    // ======Si au moins un verbe est placé====== 
                    // ======Et s'il n'a pas de chiffre utilisé======
                    // ====== On cherche les mot auto fill ======
                    if ($verbesArray.length > 0 && $numberUsed < 1) {
                        if ($verbesArray[1]) {
                            getNv3AutoFill($verbesArray[1], $(this).attr('motId'));
                        } else {
                            getNv3AutoFill($verbesArray[0], $(this).attr('motId'));
                        }
                    }


                    // Traitement de l'apostrophe
                    if (motPicto.substr(motPicto.length - 1) === '\'') {
                        $textString += mot;
                    }
                    else {
                        $textString += mot + ' ';
                    }

                });
                $textString = $textString.replace(/\s\s+/g, ' ');
                //console.log($textString);
                $textString = $.trim($textString);
                // ========  Je --> J'  ========
                // Si une verbe commence avec une voyelle
                var $verbe1voyelle = $.inArray($firstVerbe.split('')[0], $voyelles);
                // Si une verbe commence par un 'H' muet ensuite une voyelle: "habiter"
                var $verbe1HVoyelle = function() {
                    if ($.inArray($firstVerbe.split('')[0], ['h']) !== -1 && $.inArray($firstVerbe.split('')[1], $voyelles) !== -1) {
                        return true;
                    } else {
                        return false;
                    }
                };
                // Chenger ex. "je ai" à "J'ai" 
                if ($textString.indexOf('je ') && $verbe1voyelle !== -1
                        || $textString.indexOf('je ') && $verbe1HVoyelle === true
                        || $textString.indexOf('Je ') && $verbe1voyelle !== -1
                        || $textString.indexOf('Je ') && $verbe1HVoyelle === true) {
                    $textString = $textString.replace("je ", "J\'");
                }

                // Play sound notification
                playSimpleSound('sounds-1064-base.mp3');

                // traitement le texte des pictos
                $('#div-phrase-composee')
                        .find('p')
                        .html($textString);
            });
        });
        lireUnePrFemininSingulier('qdf');
    }


    /*
     * Fonction restorer / re-mettre les pictos dans la 1ere partie
     * @returns {undefined}
     */
    function restorerPicto($item, $playBravo) {
        var themeId = $item.find('div').attr('themeId');
        var $themeName = $item.find('div').attr('themeName');
        //console.log(themeId);
        $item.fadeOut(function() {
            // Si ce picto est le PREMIERE VERBE, enlever l'attribut "first-verbe"; remettre variable "$conjugue" à 0
            if ($item.find('.picto-unit').attr('first-verbe')) {
                $item.find('.picto-unit').removeAttr('first-verbe');
                $conjuguer = 0;
                // S'il y a un seconde verbe, le mettre comme le premier verbe
                if ($verbesArray[1]) {
                    secondVerbeId = $verbesArray[1];
                    $verbesArray = [];
                    $verbesArray[0] = secondVerbeId;
                }

            }
            // Si ce picto est le DEUXiEME VERBE, enlever l'attribut "second-verbe";
            if ($item.find('.picto-unit').attr('second-verbe')) {
                $item.find('.picto-unit').removeAttr('second-verbe');
                // Enlever le 2ème mot dans l'Array
                $verbesArray.pop();
            }
            // Si ce picto est un SUJET, remettre la variable "$personne" à 0;
            if ($item.find('.picto-unit').attr('picto-personne')) {
                $personne = 0;
            }
            // Si ce picto est un COMPLEMENT, remettre la variable "$genreComplement" à 0;
            if ($item.find('.picto-unit > .picto-div > .picto-mot-genre-complement').text()) {
                $genreComplement = 0;
            }

            // Append le picto to its original receiver
            $item
                    .appendTo($('#div-theme-toggle-' + themeId + ' ul'))
                    .fadeIn();

            // Recuperer tous les mots des items dans la box
            var $textString = '';
            $('#box > ul > li > .picto-unit').each(function(key, value) {
                var isAdjectif = $(this).find('.picto-div > .picto-mot-adj').text(),
                        $parentArray = $('#box > ul > li > .picto-unit');
                // Get le mot de ce picto
                var motPicto = $(this).find('.picto-div > .picto-mot').text();
                //$textString += ' ' + $(this).text();  

                // Si ce picto est le PREMIERE VERBE,conjuguer le selon la variable "personne"
                if ($(this).attr('first-verbe')) {
                    if ($personne !== 0) {
                        var $arrayConjugue = $(this).find('.picto-div > .picto-mot-conjugue').text().split(',');
                        // Recuperer la bonne forme conjuguée
                        mot = $arrayConjugue[$personne - 1];
                        // cast <spand> afin d'utiliser le bouton négation
                        mot = '<span id=\'spn-fv\'>' + mot + '</span>';
                    } else {
                        mot = $(this).find('.picto-div > .picto-mot').text();
                    }

                }
//                    {#//Si c'est un mot dans le niveau 3 avec proposition ou/et déterminant
//                    if ($(this).find('.picto-div > .picto-mot-nv3').text()) {
//                        mot = $(this).find('.picto-div > .picto-mot-nv3').text() + ' ' + motPicto;
//                    }#}
                else {
                    var mot = motPicto;
                }

                if (isAdjectif) {
                    var $arrayAdj = $(this).find('.picto-div > .picto-mot-adj').text().split(',');
                    // if this.prev is complément or this.next is complément 
                    // -- or this.prev is adj and this.prev.prev is complément --> si il y a un adverbe?
                    // -- or this.next is adj and this.next.next is complément --> si il y a un adverbe?
                    if ($($parentArray[key - 1]).find('.picto-div > .picto-mot-genre-complement').text()) {
                        $genreComplement = $($parentArray[key - 1]).find('.picto-div > .picto-mot-genre-complement').text();
                        mot = $arrayAdj[$genreComplement - 1];
                    }
                    if ($($parentArray[key + 1]).find('.picto-div > .picto-mot-genre-complement').text()) {
                        $genreComplement = $($parentArray[key + 1]).find('.picto-div > .picto-mot-genre-complement').text();
                        mot = $arrayAdj[$genreComplement - 1];
                    }
                }
                //$textString += ' ' + mot;
                // Traitement de l'apostrophe
                if (motPicto.substr(motPicto.length - 1) === '\'') {
                    $textString += mot;
                } else {
                    $textString += mot + ' ';
                }
            });
            $textString = $textString.replace(/\s\s+/g, ' ');
            //console.log($textString);

            // Re-activer le drag des pictos, 
            //if ($.inArray(themeId, ['1', '2', '3']) !== -1) {
            //    $('#div-theme-toggle-' + themeId + ' .gallery li').draggable('enable');
            //}

            // Re-activer le drag des pictos, si le theme est SUJET
            if ($.inArray($themeName, ['sujet', 'sujets', 'Sujet', 'Sujets']) !== -1) {
                $('#div-theme-toggle-' + themeId + ' .gallery li').draggable('enable');
            }
            // réactiver le drag des pictos, si deux Verbes Sentiment sont déposées
            if ($.inArray($themeName, ['sentiment', 'sentiments', 'Sentiment', 'Sentiments']) !== -1) {
                if ($verbeCounterS > 0) {
                    $verbeCounterS--;
                }
                if ($verbeCounterS < 2) {
                    $('#div-theme-toggle-' + themeId + ' .gallery li').draggable('enable');
                }
            }
            // réactiver le drag des pictos, si deux Verbes ACTION sont déposées
            if ($.inArray($themeName, ['action', 'actions', 'Action', 'Actions']) !== -1) {
                if ($verbeCounterA > 0) {
                    $verbeCounterA--;
                }
                if ($verbeCounterA < 2) {
                    $('#div-theme-toggle-' + themeId + ' .gallery li').draggable('enable');
                }
            }

            // desactiver le drag des pictos, si le theme est SUJET
            if ($.inArray($themeName, ['chiffre', 'chiffres', 'Chiffre', 'Chiffres']) !== -1) {
                $numberUsed = 0;
            }

            // ========  J' --> Je  ========                

            // ========  Je --> J'  ========                
            // Si une verbe commence avec une voyelle
            var $verbe1voyelle = $.inArray($firstVerbe.split('')[0], $voyelles);
            // Si une verbe commence par un 'H' muet ensuite une voyelle: "habiter"
            var $verbe1HVoyelle = function() {
                if ($.inArray($firstVerbe.split('')[0], ['h']) !== -1 && $.inArray($firstVerbe.split('')[1], $voyelles) !== -1) {
                    return true;
                } else {
                    return false;
                }
            };
            // Chenger ex. "je ai" à "J'ai" 
            if ($textString.indexOf('je ') && $verbe1voyelle !== -1
                    || $textString.indexOf('je ') && $verbe1HVoyelle === true
                    || $textString.indexOf('Je ') && $verbe1voyelle !== -1
                    || $textString.indexOf('Je ') && $verbe1HVoyelle === true) {
                $textString = $textString.replace("je ", "J\'");
                // Si ce mot qui est en train d'être restrauré commence par une voyelle
            }

            // Afficher le texte du picto
            $('#div-phrase-composee')
                    .find('p')
                    .html($textString);
            //$('#div-phrase-composee-corr').find('input').val($('#div-phrase-composee').find('p').text());

            if ($playBravo) {
                // Emettre "Bravo" si le picto est bien remis
                playSentence('Bravo!');
            }
        });
        // Clean text zone
//             $('#zone-text')
//                     .find('p')
//                     .html('');
    }

    // ============= Reading & highlight ===================
    $('#btn-play').click(function() {
        // Copier texte vers la zone de lecture
//             $('#zone-text')
//                     .find('p')
//                     .html($.trim($('#div-phrase-composee').find('p').text()));
        var myPhrase = $('#div-phrase-composee').find('p').text();
        // desactiv le boutton play si un mot existe
        if (myPhrase.length !== 0) {
            // disable btn lirePhrase
            $('#btn-play').attr('disabled', true);
            $('#btn-play-phrase').attr('disabled', true);
        }
        // Plan audio
        audioPlay();
    });
    // ============= Read entaire sentence ===================
    $('#btn-play-phrase').click(function() {
        // Copier texte vers la zone de lecture
//             $('#zone-text')
//                     .find('p')
//                     .html($.trim($('#div-phrase-composee').find('p').text()));
        var myPhrase = $('#div-phrase-composee').find('p').text();
        // desactiv le boutton play si un mot existe
        if (myPhrase.length !== 0) {
            // disable btn lirePhrase
            $('#btn-play').attr('disabled', true);
            $('#btn-play-phrase').attr('disabled', true);
        }

        // Do the replacement of number 1 with une
        myPhrase = lireUnePrFemininSingulier(myPhrase);
        // Play audio, phrase entière
        playSentence(myPhrase);
    });
    // ============= sauvgarder phrase composée ===================
    $('#btn-save').click(function() {
        //// Copier texte vers la zone de lecture
        var myPhrase = $.trim($('#div-phrase-composee').find('p').text());
        if (!myPhrase.length) {
            //alert('Veuillez composer une phrase.');
            return;
        }
        savePhrase(myPhrase);
    });
    // ============= Ranger tout pictogrammes ===================
    $('#btn-reset').click(function() {
        revertDraggable();
    });
    
    /*
     * Fonction de lire
     * @param {type} word
     * @returns {undefined}
     */
    function audioPlay() {
        // Do the replacement of number 1 with une
        var $texts = lireUnePrFemininSingulier($('#div-phrase-composee p').text());

        // Diviser la phrase dans le textarea en mots
        var textString = $texts.split(" ");

        // Sil na pas de texte disponible, lancer une alerte et sarreter
        if (!$.trim(textString).length) {
            //alert('Veuillez choisir un pictogramme');
            return;
        }
        //console.log(textString);
        // Initier lelement audio (pas de balise "<audio>")
        //var audio = document.createElement('audio');
        var audio = new Audio();
        // Initier lindex mot (0 pour le primer)
        var index = 0;
        // Initier la duree de temps
        var timeSpan = 0;
        /*
         * Boucle de traitement des mots: LIRE, mettre en SURBRILLANCE
         * @param {type} word
         * @returns {undefined}
         */
        function myLoop() {
            // Boucler les action avec une pause indeterminee

            // Effacer tout surbrillance
            removeHighLight();
            // Return when reaches the end of array
            if (index === textString.length) {
                //console.log('-- reading finished --');

                if ($('#btn-play').attr('disabled') === 'disabled') {
                    // active btn lirePhrase
                    $('#btn-play').removeAttr('disabled');
                }
                if ($('#btn-play-phrase').attr('disabled') === 'disabled') {
                    // active btn lirePhrase
                    $('#btn-play-phrase').removeAttr('disabled');
                }

                return;
            }

            // Recuperer un element(mot) de lArray
            var sWord = textString[index];
            // Recuperer le longueur du mot (pour definir le temps)
            var sWordLength = sWord.length;
            // Utiliser la methode de notre site, epecher denvoyer le referer header.
            audio.src = '/soundrequest?text_to_read=' + sWord;
            audio.type = 'audio/mpeg';
            // empecher denvoyer referer header
            audio.rel = 'noreferrer';
            audio.load();
            audio.play();
            // Mettre le mot actuel en surbrillance
            hightLightWord(sWord);
            // Quand un mot est lu, l'index s'incrémente, appele la function
            audio.onended = function() {
                // incrementer lindex
                index++;
                myLoop();
            };
        }
        myLoop();
    }


    // ====== fonctions trouvées dans la page twig =======
    // ===================================================

    /*
     * Mettre un mot en surbrillance
     * @param {type} word
     * @returns {undefined}
     */
    function hightLightWord(word) {
        // quand prononce 'une', hightlight '1'
        if (word === 'une' && $('#div-phrase-composee p').text().indexOf('1')) {
            word = '1';
        }
        // active the highlight
        $('#div-phrase-composee p').highlight(word, false);
    }

    /*
     * Effacer la surbrillance
     */
    function removeHighLight() {
        $('#div-phrase-composee p').removeHighlight();
    }


    // ============ level 2 ==============
    // vvvvvvvvvvvv ====== vvvvvvvvvvvvvv
    // Cacher le div pictos par default
    $('.theme-pictos').hide();
    var $visible = false;
    // Afficher les pictos quand cliquer sur un theme
    $("#ul-theme > li").click(function() {
        //$('#modal-pictos').modal('show');
        var currentThemeId = $(this).find('div').attr('itemid');
        //console.log('thème ' + currentThemeId);
        $('.theme-pictos').hide();
        $('#div-theme-toggle-' + currentThemeId).toggle('fast');
        return false;
    });

    // Bouton Négation
    var negation = 0;
    $('#btn-negation').click(function() {
        var verbeConjugue = $('#spn-fv').text(),
                textComposee = $('#div-phrase-composee').find('p').html();
        if (verbeConjugue.length <= 0) {
            return;
        }
        if (negation === 0) {
            // Si il existe "J'"
            if (textComposee.indexOf('J\'') !== -1) {
                $('#div-phrase-composee').find('p').html(textComposee.replace('J\'', 'Je '));
            }
            // Traiter le verbe qui commence par une voyelle
            if ($.inArray(verbeConjugue.split('')[0], $voyelles) !== -1) {
                //console.log($.inArray(verbeConjugue.split('')[0], $voyelles ));
                $('#spn-fv').text('n\'' + verbeConjugue + ' pas');
            } else {
                $('#spn-fv').text('ne ' + verbeConjugue + ' pas');
            }

            negation = 1;
            //$('#div-phrase-composee-corr').find('input').val($('#div-phrase-composee').find('p').text());
            return;
        } else {
            // Si il existe ex. "Je n'ai pas"
            if (textComposee.indexOf('Je') !== -1 && $.inArray($firstVerbe.split('')[0], $voyelles) !== -1) {
                $('#div-phrase-composee').find('p').html(textComposee.replace('Je ', 'J\''));
            }
            $('#spn-fv').text($firstVerbe);
            negation = 0;
            //$('#div-phrase-composee-corr').find('input').val($('#div-phrase-composee').find('p').text());
            return;
        }
    });

    /**
     * Lecture d'une phrase entière
     * @param {type} $phrase
     * @returns {undefined}          */
    function playSentence($phrase) {
        var audio = new Audio();
        audio.src = '/soundrequest?text_to_read=' + $phrase;
        audio.type = 'audio/mpeg';
        // empecher denvoyer referer header
        audio.rel = 'noreferrer';
        audio.load();
        audio.play();
        // end of audio play
        audio.onended = function() {
            // re-active btn Lecture
            if ($('#btn-play').attr('disabled') === 'disabled') {
                $('#btn-play').removeAttr('disabled');
            }
            if ($('#btn-play-phrase').attr('disabled') === 'disabled') {
                $('#btn-play-phrase').removeAttr('disabled');
            }
        };
    }
    ;

    /**
     * Function plays simple sound
     * @param {string} $soundFile
     * @returns {undefined}
     */
    function playSimpleSound($soundFile) {
        var audio = new Audio();
        audio.src = '/uploads/sounds/' + $soundFile;
        audio.type = 'audio/mpeg';
        // empecher denvoyer referer header
        audio.rel = 'noreferrer';
        audio.load();
        audio.play();
    }

    /**
     * Function replace 1 par une pour la lecture si le complément est un mot féminin singulier
     * @param {type} $phrase
     * @returns {unresolved}
     */
    function lireUnePrFemininSingulier($phrase) {
        // get drop-zone elements array
        var phrasePictoArray = $('#box > ul > li');
        $number1exist = 0;
        $needChange = 0;

        // Iterate all pictos in the drop-zone
        phrasePictoArray.each(function(key, value) {
            // dectect if there is numbre: 1
            if ($.inArray($(this).find('.picto-unit').attr('themename'), ['chiffre', 'chiffres', 'Chiffre', 'Chiffress']) !== -1
                    && $.trim($(this).find('p.picto-mot').text()) === '1') {
                $number1exist = 1;
            }
            // detect if there is a complément before or after number 1
            if ($(this).prev().find('p.picto-mot-genre-complement').text() === '2'
                    || $(this).next().find('p.picto-mot-genre-complement').text() === '2') {
                $needChange = 1;
            }
        });

        // return re-formed sentence
        if ($number1exist === 1 || $needChange === 1) {
            return $phrase.replace('1', 'une');
        } else {
            return $phrase;
        }
    }

    /**
     * function ranger tous
     * @returns {undefined}
     */
    function revertDraggable() {
        // Si il y en n'a pas de picto dans la drop-zone, we do noting
        if($('#box > ul > li.ui-draggable').length < 1) {
            return;
        }
        // Reset all pictos, and do not play the sound "Bravo"
        $('#box > ul > li.ui-draggable').each(function() {
            restorerPicto($(this), false);
        });
    }
});
