$(function() {
    // there's the gallery and the box
    var $gallery = $("#gallery"),
            $box = $("#box");


    // let the gallery items be draggable
    $("li", $gallery).draggable({
        cancel: "a.ui-icon", // clicking an icon won't initiate dragging
        revert: "invalid", // when not dropped, the item will revert back to its initial position
        containment: ".content",
        helper: "clone",
        cursor: "move",
        snap: true,
        snapMode: 'inner'
    });

    // make the box be droppable, accepting the gallery items
    $box.droppable({
        accept: "#gallery > li",
        activeClass: "ui-state-highlight",
        snap: true,
        snapMode: 'inner',
        drop: function(event, ui) {
            dropPicto(ui.draggable);
        }
    });

    // make the gallery be droppable as well, accepting items from the trash
    $gallery.droppable({
        accept: "#box li",
        activeClass: "ui-state-highlight",
        drop: function(event, ui) {
            restorerPicto(ui.draggable);
        }
    });

    /*
     * fonction mettre les pictos dans la box / la 2ere partie
     * @returns {undefined}
     */
    function dropPicto($item) {
        $item.fadeOut(function() {
            var $list = $("ol", $box).length ?
                    $("ol", $box) :
                    $("<ol class='gallery ui-helper-reset'  style='position:relative; z-index:1;'/>").appendTo($box);
            // Animation pour append
            $item.appendTo($list).fadeIn(function() {
                $item
                        .animate({width: "auto"})
                        .find("img")
                        .animate({height: "120px"});

                // desactiver le drag des pictos, 
                $("ul#gallery li").draggable('disable');

                // Recuperer tous les mots des items dans la box
                var $textArray = '';
                $('#box > ol > li > div > p').each(function() {
                    $textArray += $(this).text() + ' ';
                });
                $textArray = $.trim($textArray);

                // Play sound notification
                playSimpleSound('sounds-1064-base.mp3');
                
                // Afficher le texte des pictos
                $('#div-phrase-composee')
                        .find('p')
                        .html($textArray);
            });
        });
    }

    /*
     * Fonction restorer / re-mettre les pictos dans la 1ere partie
     * @returns {undefined}
     */
    function restorerPicto($item) {
        $item.fadeOut(function() {
            $item
                    .appendTo($gallery)
                    .fadeIn();

            // Recuperer tous les mots des items dans la box
            var $textArray = '';
            $('#box > ol > li > div > p').each(function() {
                $textArray += ' ' + $(this).text();
            });

            // re-activer le drag des pictos, 
            $("ul#gallery li").draggable('enable');
            // Afficher le texte du picto
            /*$('#zone-text')
             .find('p')
             .html($textArray);*/
            $('#div-phrase-composee')
                    .find('p')
                    .html($textArray);

            // activer le drag des pictos, 
            //$( "li", $gallery ).draggable('enable');

            // Clean text zone
//            $('#zone-text')
//                .find('p')
//                .html('');
        });
    }

    // ============= Reading & highlight ===================
    $('#btn-play').click(function() {
        var myPhrase = $.trim($('#div-phrase-composee').find('p').text());
        if (!myPhrase.length) {
            //alert('Veuillez composer une phrase.');
            return;
        }
        $('#btn-play').attr('disabled', true);

        // audioPlay();
        playSentence(myPhrase);
    });

    // ============= sauvgarder phrase composée ===================
    $('#btn-save').click(function() {
        //// Copier texte vers la zone de lecture
        var myPhrase = $.trim($('#div-phrase-composee').find('p').text());
        if (!myPhrase.length) {
            //alert('Veuillez composer une phrase.');
            return;
        }
        savePhrase(myPhrase);
    });


    /*
     * Fonction de lecture
     * @param {type} word
     * @returns {undefined}
     */
    function audioPlay() {
        // Diviser la phrase dans le textarea en mots
        var textArray = $('#div-phrase-composee p').text().split(" ");
        // Sil na pas de texte disponible, lancer une alerte et sarreter
        if (!$.trim(textArray).length) {
            //alert('Veuillez choisir un pictogramme');
            return;
        }
        //console.log(textArray);
        // Initier lelement audio (pas de balise "<audio>")
        //var audio = document.createElement('audio');
        var audio = new Audio();
        // Initier lindex mot (0 pour le primer)
        var index = 0;
        // Initier la duree de temps
        var timeSpan = 0;

        /*
         * Boucle de traitement des mots: LIRE, mettre en SURBRILLANCE
         * @param {type} word
         * @returns {undefined}
         */
        function myLoop() {
            // Boucler les action avec une pause indeterminee
            //setTimeout(function() {
            // Effacer tout surbrillance
            removeHighLight();
            // Return when reaches the end of array
            if (index === textArray.length) {
                // console.log('-- reading finished --');
                return;
            }

            // Recuperer un element(mot) de lArray
            var sWord = textArray[index];

            // Utiliser la methode de notre site, epecher denvoyer le referer header.
            audio.src = '/soundrequest?text_to_read=' + sWord;
            audio.type = 'audio/mpeg';
            // empecher denvoyer referer header
            audio.rel = 'noreferrer';
            audio.load();
            audio.play();

            // Mettre le mot actuel en surbrillance
            hightLightWord(sWord);

            // Quand un mot est lu, l'index s'incrémente, appele la function
            audio.onended = function() {
                // incrementer lindex
                index++;
                myLoop();
            };
            /*// incrementer lindex
             index++;
             myLoop();
             }, timeSpan);*/

        }
        myLoop();
    }

    /**
     * Lire une phrase entière
     * @param {type} $phrase
     * @returns {undefined}         */
    function playSentence($phrase) {
        var audio = new Audio();
        audio.src = '/soundrequest?text_to_read=' + $phrase;
        audio.type = 'audio/mpeg';
        // empecher denvoyer referer header
        audio.rel = 'noreferrer';
        audio.load();
        audio.play();
        
        // Mettre le mot actuel en surbrillance
        hightLightWord($phrase);
        
        // end of audio play
        audio.onended = function() {
            // Effacer tout surbrillance
            removeHighLight();
            // re-active btn Lecture
            if ($('#btn-play').attr('disabled') === 'disabled') {
                $('#btn-play').removeAttr('disabled');
            }
        };
    }

    /**
     * Function plays simple sound
     * @param {string} $soundFile
     * @returns {undefined}
     */
    function playSimpleSound($soundFile) {
        var audio = new Audio();
        audio.src = '/uploads/sounds/' + $soundFile;
        audio.type = 'audio/mpeg';
        // empecher denvoyer referer header
        audio.rel = 'noreferrer';
        audio.load();
        audio.play();
    }
//    /**
//     * Sauvgarder la phrase composée
//     * @param {type} myPhrase
//     * @returns {undefined}         */
//    function savePhrase(myPhrase) {
//        $.ajax({
//            url: "{{path('communication_save_phrase')}}",
//            data: {
//                my_phrase: myPhrase,
//            },
//            dataType: 'json',
//            success: function(data)
//            {
//                if (data.result === 'success') {
//                    $('div#top-zone').prepend(
//                            "<div class=\"alert alert-info fade in\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>Phrase enregistrée à {{'now'|date('H:i')}}.</div>"
//                            );
//                }
//
//            }
//        });
//    }

    /*
     * Mettre un mot en surbrillance
     * @param {type} word
     * @returns {undefined}
     */
    function hightLightWord(word) {
        // active the highlight
        $('#div-phrase-composee p').highlight(word, false);
    }

    /*
     * Effacer la surbrillance
     */
    function removeHighLight() {
        $('#div-phrase-composee p').removeHighlight();
    }

});
