{include file="header.tpl" title="Ajouter utilisateur" Name="Objis" css="default"}
<script type="text/javascript">
window.onload = function()
{
    document.getElementById('role').onchange = function()
    {
        if (this.value == 2) {
            document.getElementById('entreprise').disabled = true;
            document.getElementById('entreprise').style.display = 'none';
        } else {
            document.getElementById('entreprise').disabled = false;
            document.getElementById('entreprise').style.display = 'block';
        }
    }
}
</script>

<form class='form-signin form-horizontal' id="register" action="" method="post">
    <fieldset>
        <legend>Ajouter utilisateur</legend>
        <label class="control-label" for="role_add_user">Rôle d'utilisateur: </label>
        <div class="controls">
            {if $utilisateur}
                {html_options options=$role_outputs id=role name=type_id required=true selected=$utilisateur->type_id}
            {else}
                {html_options options=$role_outputs id=role name=type_id required=true}
            {/if}
        </div>
        <label class="control-label" id="label_ent_add_user" for="role">Raison sociale: </label>
        <div class="controls" id="combo_ent_add_user">
            {if $utilisateur}
                {html_options options=$entreprise_outputs id=entreprise name=entreprise_id selected=$utilisateur->entreprise_id}
            {else}
                {html_options options=$entreprise_outputs id=entreprise name=entreprise_id}
            {/if}
        </div>
        <div class="control-group">
            <label class="control-label" for="nom">Nom: </label>
            <div class="controls">
                <input type="text" onkeyup='this.value=this.value.toUpperCase()'  placeholder="Nom" id="nom" name="nom" required="true" {if $utilisateur}value='{$utilisateur->nom}'{/if}/>
            </div>
            <label class="control-label" for="prenom">Prénom: </label>
            <div class="controls">
                <input type="text"  placeholder="Prénom" id="prenom" name="prenom" {if $utilisateur}value='{$utilisateur->prenom}'{/if} />
            </div>
            <label class="control-label" for="login">Login: </label>
            <div class="controls">
                <input type="text"  placeholder="Login" id="login" required='true' name="login" {if $utilisateur}value='{$utilisateur->login}'{/if} />
            </div>
            <label class="control-label" for="mdp">Mot de passe: </label>
            <div class="controls">
                <input type=password  placeholder="Mot de passe" id="mdp" {if ! $utilisateur}required='true'{/if} name="password" />
            </div>
        </div>
        <button class="btn btn-large btn-primary" type="submit" name="btnSave">Enregistrer</button>
    </fieldset>
</form>