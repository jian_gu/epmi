<?php

namespace TerrePlurielle\Bundle\BoBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class PhraseComposeeAdmin extends Admin {

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                ->add('contenu', 'textarea')
                ->add('refEnfant', 'entity', array('label' => 'Enfnat', 'class' => 'TerrePlurielle\Bundle\FoBundle\Entity\Enfant'))
                ->add('refNiveau', 'entity', array('label' => 'Niveau', 'class' => 'TerrePlurielle\Bundle\FoBundle\Entity\Niveau'))
//                ->add('dateSaved', 'datetime', array('widget' => 'single_text', 'input' => 'datetime', 'format' => 'ddMyyyy His', 'widget' => 'choice'))
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('refEnfant')
                ->add('refNiveau')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->addIdentifier('contenu')
                ->addIdentifier('refEnfant')
                ->addIdentifier('refNiveau')
                ->add('dateSaved', 'datetime', array('label'=>'Date enregistrer', 'format'=>'d-m-Y, à H:i:s'))
        ;
    }

}
