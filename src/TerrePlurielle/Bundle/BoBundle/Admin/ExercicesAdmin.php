<?php

namespace TerrePlurielle\Bundle\BoBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class ExercicesAdmin extends Admin
{
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('objectif', 'textarea', array('attr' => array('rows'=>5)))
            ->add('question', 'text')
            ->add('reponse', 'text')
            ->add('refPictogramme', 'entity', array('label' => 'Pictogramme', 'property'=>'refMot.titre', 'class' => 'TerrePlurielle\Bundle\FoBundle\Entity\Pictogramme'))
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('question')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('question')
        ;
    }
}