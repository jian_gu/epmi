<?php

namespace TerrePlurielle\Bundle\BoBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class Nv3AutoFillAdmin extends Admin {

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                ->add('refVerbe', 'entity', array('label' => 'Verbe', 'property' => 'titre', 'class' => 'TerrePlurielle\Bundle\FoBundle\Entity\Mot'))
                ->add('autoFill', 'text', array('label' => 'Chemin du pictogramme', 'required' => TRUE))
                ->add('refComplement', 'entity', array('label' => 'Pour complement', 'property' => 'titre', 'class' => 'TerrePlurielle\Bundle\FoBundle\Entity\Mot'))
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('refVerbe')
                ->add('autoFill')
                ->add('refComplement')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->add('refVerbe')
                ->addIdentifier('autoFill')
                ->add('refComplement')
        ;
    }

}

