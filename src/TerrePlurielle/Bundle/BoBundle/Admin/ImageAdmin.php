<?php

namespace TerrePlurielle\Bundle\BoBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class ImageAdmin extends Admin {

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                ->add('file', 'file', array('label' => 'Chemin du image', 'required' => FALSE))
                ->add('refEnfant', 'entity', array('label' => 'Enfant', 'class' => 'TerrePlurielle\Bundle\FoBundle\Entity\Enfant'))
                ->add('refPictogramme', 'entity', array('label' => 'Pour Mot', 'property' => 'refMot.titre', 'class' => 'TerrePlurielle\Bundle\FoBundle\Entity\Pictogramme'))
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('url')
                ->add('refEnfant')
                ->add('refPictogramme')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->addIdentifier('url')
                ->add('refEnfant')
                ->add('refPictogramme')
        ;
    }

    public function prePersist($image) {
        $this->saveFile($image);
    }

    public function preUpdate($image) {
        $this->saveFile($image);
    }

    public function saveFile($image) {
        $basepath = $this->getRequest()->getBasePath();
        $image->upload($basepath);
    }

}
