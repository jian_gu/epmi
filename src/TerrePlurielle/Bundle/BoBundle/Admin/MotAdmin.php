<?php

namespace TerrePlurielle\Bundle\BoBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class MotAdmin extends Admin {

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                ->with('Général')
                ->add('titre', 'text', array('label' => 'Titre du mot'))
//                ->add('categorie', 'text', array('label' => 'Categorie du thème',
//                    'required' => false))
                ->add('refTheme', 'sonata_type_model', array('label' => 'Thème', 'class' => 'TerrePlurielle\Bundle\FoBundle\Entity\Theme'))
//                ->add('niveau', 'choice', array('choices' => array('1' => 'Niveau I',
//                        '2' => 'Niveau II',
//                        '3' => 'Niveau III',
//                        '4' => 'Niveau IV'),
//                    'required' => true))
                ->add('niveau', 'entity', array('label' => 'Niveau', 'class' => 'TerrePlurielle\Bundle\FoBundle\Entity\Niveau'))
                ->end()
                ->with('Sujet')
                ->add('personne', 'choice', array('choices' => array('1' => '\'je\'',
                        '2' => '\'tu\'',
                        '3' => '\'il / elle\'',
                        '4' => '\'nous\'',
                        '5' => '\'vous\'',
                        '6' => '\'ils / elles\''),
                    'required' => FALSE))
                ->end()
                ->with('Verbe (Conjugaison)')
                ->add('pps', 'text', array('label' => '\'je\'', 'required' => false))
                ->add('dps', 'text', array('label' => '\'tu\'', 'required' => false))
                ->add('tps', 'text', array('label' => '\'il / elle\'', 'required' => false))
                ->add('ppp', 'text', array('label' => '\'nous\'', 'required' => false))
                ->add('dpp', 'text', array('label' => '\'vous\'', 'required' => false))
                ->add('tpp', 'text', array('label' => '\'ils / elles\'', 'required' => false))
                ->add('pc', 'text', array('label' => 'Passé composé', 'required' => false))
                ->end()
                ->with("Complément")
                ->add('genre', 'choice', array('choices' => array('1' => 'Masculin singulier',
                        '2' => 'Masculin pluriel',
                        '3' => 'Féminiin singulier',
                        '4' => 'Féminin plurielle'),
                    'required' => false))
                ->add('plur', 'text', array('label' => 'Format pluriel', 'required' => false))
                ->end()
                ->with('Adjectif')
                ->add('ms', 'text', array('label' => 'Masculin singulier', 'required' => false))
                ->add('mp', 'text', array('label' => 'Masculin pluriel', 'required' => false))
                ->add('fs', 'text', array('label' => 'Féminin singulier', 'required' => false))
                ->add('fp', 'text', array('label' => 'Féminin pluriel', 'required' => false))
                ->end()
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('titre')
//                ->add('categorie')
                ->add('refTheme')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->addIdentifier('titre')
//                ->addIdentifier('categorie')
                ->add('refTheme',null,array('label' => 'Thème'))
        ;
    }

}
