<?php

namespace TerrePlurielle\Bundle\BoBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class EnfantAdmin extends Admin {

// Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper) {
//        $qry_get_educateurs = $this->modelManager->getEntityManager('TerrePlurielle\Bundle\BoBundle\Entity\User')->createQuery('SELECT u FROM BoBundle:User u');


        $formMapper
                ->add('nom', 'text')
                ->add('prenom', 'text')
                ->add('dateDeNaissance', 'date', array('label' => 'Date de Naissance', 'input' => 'datetime', 'format' => 'ddMyyyy', 'widget' => 'choice',))
                ->add('observation', 'text', array('required' => false))
                ->add('refNiveau', 'entity', array('label' => 'Niveau référent', 'class' => 'TerrePlurielle\Bundle\FoBundle\Entity\Niveau', 'required' => false))
                ->add('file', 'file', array('label' => 'Avatar', 'required' => false))
//                ->add('avatar', 'entity', array('label' => 'Pictogramme référent', 'property' => 'refMot.titre', 'class' => 'TerrePlurielle\Bundle\FoBundle\Entity\Pictogramme', 'required' => false))
                ->add('refParent', 'entity', array('label' => 'Parent référent',
                    'class' => 'TerrePlurielle\Bundle\BoBundle\Entity\User',
                    'required' => false,
                    'query_builder' => function(\Doctrine\ORM\EntityRepository $er) {
                return $er->createQueryBuilder('p')->where('p.roles LIKE \'%ROLE_PARENT%\'');
            }
                ))
                ->add('refEducateur', 'entity', array('label' => 'Educateur référent',
                    'class' => 'TerrePlurielle\Bundle\BoBundle\Entity\User',
                    'required' => false,
                    'query_builder' => function(\Doctrine\ORM\EntityRepository $er) {
                return $er->createQueryBuilder('p')->where('p.roles LIKE \'%ROLE_EDUCATEUR%\'');
            }
                ))
//                ->add('refEducateur', 'sonata_type_model', array('required' => false, 'query' => $qry_get_educateurs))
//                ->add('refPhraseComposee', 'sonata_type_collection', array('label' => 'Phrase composee'))
// ->add('refExercice', 'sonata_type_collection', array('label' => 'Exercice'))
        ;
    }

// Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('nom')
                ->add('prenom')
                ->add('refNiveau')
        ;
    }

// Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->addIdentifier('nom')
                ->addIdentifier('prenom')
                ->addIdentifier('refNiveau')
                ->addIdentifier('refEducateur')
                ->add('avatar')
        ;
    }

    public function prePersist($enfant) {
        $this->saveFile($enfant);
    }

    public function preUpdate($enfant) {
        $this->saveFile($enfant);
    }

    public function saveFile($enfant) {
        $basepath = $this->getRequest()->getBasePath();
        $enfant->upload($basepath);
    }

}
