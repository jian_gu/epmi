<?php

namespace TerrePlurielle\Bundle\BoBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class PictogrammeAdmin extends Admin {

    public $supportsPreviewMode = true;

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                ->add('pictoFile', 'file', array('label' => 'Chemin du pictogramme', 'required' => FALSE))
                ->add('refMot', 'entity', array('label' => 'Mot référent', 'property' => 'titre', 'class' => 'TerrePlurielle\Bundle\FoBundle\Entity\Mot'))
                ->add('refPhrase', 'entity', array('label' => 'Phrase référent', 'class' => 'TerrePlurielle\Bundle\FoBundle\Entity\PhraseProposee', 'required' => FALSE))
//                ->add('refTheme', 'entity', array('label' => 'Thème', 'class' => 'TerrePlurielle\Bundle\FoBundle\Entity\Theme', 'required' => FALSE))
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('refMot.titre')
                ->add('pictoUrl')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->add('refMot.titre')
                ->addIdentifier('pictoUrl')
        ;
    }

    public function prePersist($pictogramme) {
        $this->saveFile($pictogramme);
    }

    public function preUpdate($pictogramme) {
        $this->saveFile($pictogramme);
    }

    public function saveFile($pictogramme) {
        $basepath = $this->getRequest()->getBasePath();
        $pictogramme->upload($basepath);
    }

}
