<?php

namespace TerrePlurielle\Bundle\BoBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class ActiviteAdmin extends Admin {

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper
                ->add('titre', 'text', array('label' => 'Titre'))
//            ->add('dateDebut', 'date', array( 'widget' => 'single_text', 'input' => 'datetime','format' => 'yyyy/MM/dd', 'attr' => array('class' => 'date'),))
//            ->add('dateFin', 'date', array( 'widget' => 'single_text', 'input' => 'datetime','format' => 'yyyy/MM/dd','attr' => array('class' => 'date'),))
		->add('dateDebut', 'datetime', array('input' => 'datetime', 'date_format' => 'ddMyyyy','date_widget' => 'choice', 'time_widget' => 'choice','years' => range(Date('Y'), Date('Y') + 2)))
                ->add('dateFin', 'datetime', array('input' => 'datetime', 'date_format' => 'ddMyyyy', 'date_widget' => 'choice', 'time_widget' => 'choice', 'years' => range(Date('Y'), Date('Y') + 2)))
                ->add('statut', 'checkbox', array('label' => 'Activité Indisponible', 'required' => false,))
                ->add('refPictogramme', 'entity', array('label' => 'Pictogramme référent', 'property' => 'refMot.titre', 'class' => 'TerrePlurielle\Bundle\FoBundle\Entity\Pictogramme'))
                ->add('refEnfant', 'sonata_type_model', array('label' => 'Pour Enfant(s)', 'property' => 'nameDOB', 'class' => 'TerrePlurielle\Bundle\FoBundle\Entity\Enfant', 'expanded' => FALSE, 'multiple' => true, 'required' => FALSE))
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper
                ->add('titre')
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper) {
        $listMapper
                ->addIdentifier('titre')
                ->addIdentifier('dateDebut', 'datetime', array('format' => 'd/m/Y'))
                ->addIdentifier('dateFin', 'datetime', array('format' => 'd/m/Y'))
                ->add('statut',null, array('label'=>'Fini?','editable' => true))
        ;
    }

}
