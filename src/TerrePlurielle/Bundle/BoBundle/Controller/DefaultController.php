<?php

namespace TerrePlurielle\Bundle\BoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('BoBundle:Default:index.html.twig', array('name' => $name));
    }
}
