<?php 
namespace TerrePlurielle\Bundle\BoBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use FOS\UserBundle\Form\Type\RegistrationFormType as BaseType;

class RegistrationFormType extends BaseType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('Firstname')
            ->add('Lastname')
           ->add('roles', 'collection', array(
                   'type' => 'choice',
                   'options' => array(
                       'choices' => array('ROLE_PARENT' => 'Parent', 'ROLE_EDUCATEUR' => 'Educateur'),
                       'label' => false,
                       'attr' => array('style' => 'width:300px', 'customattr' => 'customdata'),
                   )
               )
           )
       ;
    }

    public function getName()
    {
        return 'tp_user_registration';
    }
}