<?php

namespace TerrePlurielle\Bundle\BoBundle\Entity;

use Sonata\UserBundle\Entity\BaseUser as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 *
 * @ORM\Entity
 * @ORM\Table(name="fos_user_user")
 */
class User extends BaseUser
{

    /**
     * @var integer $id
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


     
    // protected $roles;

    /**
     * Get id
     *
     * @return integer $id
     */
    public function getId()
    {
        return $this->id;
    }

    
    // /**
    //  * @param string $role
    //  */
    // public function setrole($role)
    // {
    //     $this->role = $role;
    // }

    // /**
    //  * @return string
    //  */
    // public function getrole()
    // {
    //     return $this->role;
    // }

}
