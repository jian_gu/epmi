<?php

namespace TerrePlurielle\Bundle\FoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use TerrePlurielle\Bundle\FoBundle\Entity\Enfant;
use TerrePlurielle\Bundle\FoBundle\Form\Type\EnfantFormType;

/**
 * Description of GestionController
 *
 * @author HugoG
 */
class GestionController extends Controller {

    public function indexAction() {
        $session = $this->getRequest()->getSession();
        $loggedUser = $this->getLoggedUser();
        if ($loggedUser && $loggedUser != "anon.") {
            // user connected
            if ($session->get('enfant')) {
                // child is selected and saved in the session
                return $this->render('FoBundle:Gestion:index.html.twig');
            } else {
                // we call function to let the user choose a child
                return $this->redirect($this->generateUrl('fo_homepage'));
            }

            // return $this->render('FoBundle:Home:choose.html.twig');
        } else {
            // no user connected
//            return $this->render('FoBundle:Home:index.html.twig');
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
    }

    /**
     * get logged user
     * @return null
     */
    private function getLoggedUser() {
        $token = $this->get('security.context')->getToken();
        if ($token->getUser()) {
            return $token->getUser();
        } else {
            return NULL;
        }
    }

}
