<?php

namespace TerrePlurielle\Bundle\FoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use TerrePlurielle\Bundle\FoBundle\Entity\Pictogramme;
use TerrePlurielle\Bundle\FoBundle\Form\Type\PictogrammeType;
use TerrePlurielle\Bundle\FoBundle\Form\Type\PictogrammeInAllType;

/**
 * Pictogramme controller.
 *
 */
class PictogrammeController extends Controller {

    /**
     * Lists all Pictogramme entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('FoBundle:Pictogramme')->findAll();

        // Pagination
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $entities, $this->get('request')->query->get('page', 1)/* page number */, 20/* limit per page */
        );
        
        return $this->render('FoBundle:Pictogramme:index.html.twig', array(
                    // 'entities' => $entities,
                    'pagination' => $pagination,
        ));
    }

    /**
     * Creates a new Pictogramme entity.
     *
     */
    public function createAction(Request $request) {
        $entity = new Pictogramme();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity->upload('');
            $em->persist($entity);
            $em->flush();

            // Flash message
            $this->get('session')->getFlashBag()->add(
                    'success', 'Pictogramme ajouté.'
            );

            return $this->redirect($this->generateUrl('pictogramme_show', array('id' => $entity->getId())));
        }

        return $this->render('FoBundle:Pictogramme:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Pictogramme entity.
     *
     * @param Pictogramme $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Pictogramme $entity) {
        $form = $this->createForm(new PictogrammeInAllType(), $entity, array(
            'action' => $this->generateUrl('pictogramme_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Pictogramme entity.
     *
     */
    public function newAction() {
        $entity = new Pictogramme();

        // add embeded entities
        $entity->setRefMot(new \TerrePlurielle\Bundle\FoBundle\Entity\Mot());
        $entity->setRefPhrase(new \TerrePlurielle\Bundle\FoBundle\Entity\PhraseProposee());

        $form = $this->createCreateForm($entity);

        return $this->render('FoBundle:Pictogramme:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Pictogramme entity.
     *
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('FoBundle:Pictogramme')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Pictogramme entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('FoBundle:Pictogramme:show.html.twig', array(
                    'entity' => $entity,
                    'delete_form' => $deleteForm->createView(),));
    }

    /**
     * Displays a form to edit an existing Pictogramme entity.
     *
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('FoBundle:Pictogramme')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Pictogramme entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('FoBundle:Pictogramme:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a Pictogramme entity.
     *
     * @param Pictogramme $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Pictogramme $entity) {
        $form = $this->createForm(new PictogrammeType(), $entity, array(
            'action' => $this->generateUrl('pictogramme_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing Pictogramme entity.
     *
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('FoBundle:Pictogramme')->find($id);

        $entity->setRefMot(new \TerrePlurielle\Bundle\FoBundle\Entity\Mot());
        $entity->setRefPhrase(new \TerrePlurielle\Bundle\FoBundle\Entity\PhraseProposee());

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Pictogramme entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('pictogramme_edit', array('id' => $id)));
        }

        return $this->render('FoBundle:Pictogramme:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Pictogramme entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('FoBundle:Pictogramme')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Pictogramme entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('pictogramme'));
    }

    /**
     * Creates a form to delete a Pictogramme entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('pictogramme_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }

}

