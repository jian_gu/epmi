<?php

namespace TerrePlurielle\Bundle\FoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use TerrePlurielle\Bundle\FoBundle\Entity\Enfant;

class HistoriqueController extends Controller {

    public function indexAction() {
        $session = $this->getRequest()->getSession();
        $enfant = new Enfant();
        $enfant_repo = $this->getDoctrine()->getManager()->getRepository('FoBundle:Enfant');
        $currentUser = $this->userLogged();


        $session->set('current_user', $currentUser);
        $form = $this->createFormBuilder($enfant)
                ->add('id', 'entity', array(
                    'class' => 'FoBundle:Enfant',
                    'property' => 'nameDOB',
                    'choices' => $enfant_repo->getChildrenByLoggedUser($currentUser->getRealRoles(), $currentUser->getId())
                        )
                )
                ->getForm();


        $request = $this->get('request');

        // On vérifie qu'elle est de type POST
        if ($request->getMethod() == 'POST') {
            $formPost = $request->get('form');
            $currentEnfantArray = $enfant_repo->findBy(array('id' => $formPost['id']));
            $currentEnfant = $currentEnfantArray[0];

            $session->set('enfant', $currentEnfant);

            if ($currentEnfant->getRefNiveau()) {
                $session->set('currentNiveau', $currentEnfant->getRefNiveau()->getTitre());
            } else {
                // child has no level
                $this->get('session')->getFlashBag()->add(
                        'notice', 'Attention l\'enfant ne possède pas un niveau!'
                );
            }
        }

        // session variable "enfant" existe
        if ($session->get('enfant') && $session->get('enfant')->getId()) {
            // Envoie vers la page demandée
            return $this->redirect($this->getRequest()->headers->get('referer'));
        } else {
            //throw $this->createNotFoundException('Veuillez vérifier que l\'enfant existe');
        }

        return $this->render('FoBundle:Histoire:index.html.twig', array(
                    'form' => $form->createView(),
                        )
        );
    }

    /**
     * clean variable enfant dans la session
     * @return type
     */
    public function resetEnfantAction() {
        if ($this->getRequest()->getSession()->get('enfant') || $this->getRequest()->getSession()->get('currentNiveau')) {
            $this->getRequest()->getSession()->remove('enfant');
            $this->getRequest()->getSession()->remove('currentNiveau');
        }
        return $this->indexAction();
    }

    /**
     * function get communication history by session enfant 
     * @return type
     */
    public function commuAction() {
        $session = $this->getRequest()->getSession();

        // Si variable "enfant" n'est pas dans la session
        if (!$session->get("enfant")) {
            return $this->indexAction('commu');
        }

        // Si session variable "enfant" existe
        $phraseComposeeRepo = $this->getDoctrine()->getRepository('FoBundle:PhraseComposee');
        $enfantId = $session->get('enfant')->getId();
        $listHisto = $phraseComposeeRepo->getResultByEnfantid($enfantId);
        
        return $this->render('FoBundle:Histoire:commu.html.twig', array(
            'list_his_commu' => $listHisto,
                        )
        );
    }

    public function socialAction() {
        $session = $this->getRequest()->getSession();

        // Si variable "enfant" n'est pas dans la session
        if (!$session->get("enfant")) {
            return $this->indexAction('social');
        }

        // Si session variable "enfant" existe
        $enfantExercicesRepo = $this->getDoctrine()->getRepository('FoBundle:EnfantExercices');
        $enfantId = $session->get('enfant')->getId();
        $listSocial = $enfantExercicesRepo->getResultByEnfantid($enfantId);

        return $this->render('FoBundle:Histoire:social.html.twig', array(
            'list_his_social' => $listSocial,
                        )
        );
    }

    /**
     * Get current logged user context
     * @return type
     */
    public function userLogged() {
        return $this->get('security.context')->getToken()->getUser();
    }

}
