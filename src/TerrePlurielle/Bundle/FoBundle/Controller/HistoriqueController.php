<?php

namespace TerrePlurielle\Bundle\FoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use TerrePlurielle\Bundle\FoBundle\Entity\Enfant;

class HistoriqueController extends Controller {

    public function indexAction() {
        $session = $this->getRequest()->getSession();

        if (!$session->get('enfant')) {
            // we call function to let the user choose a child
            return $this->redirect($this->generateUrl('fo_homepage'));
        } else {
            $listHisto = $this->commu();
            $listSocial = $this->social();
            return $this->render('FoBundle:Historique:index.html.twig', array(
                        'list_his_commu' => $listHisto,
                        'list_his_social' => $listSocial
                            )
            );
        }
    }

    /**
     * function get communication history by session enfant 
     * @return type
     */
    public function commu() {
        $session = $this->getRequest()->getSession();

//        // Si variable "enfant" n'est pas dans la session
//        if (!$session->get("enfant")) {
//            return $this->indexAction('commu');
//        }

        // Si session variable "enfant" existe
        $phraseComposeeRepo = $this->getDoctrine()->getRepository('FoBundle:PhraseComposee');
        $enfantId = $session->get('enfant')->getId();
        $listHisto = $phraseComposeeRepo->getResultByEnfantid($enfantId);

        // Pagination
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $listHisto, $this->get('request')->query->get('page', 1)/* page number */, 20/* limit per page */
        );

        return $pagination;
    }

    public function social() {
        $session = $this->getRequest()->getSession();

//        // Si variable "enfant" n'est pas dans la session
//        if (!$session->get("enfant")) {
//            return $this->indexAction('social');
//        }
       
        // Si session variable "enfant" existe
        $enfantExercicesRepo = $this->getDoctrine()->getRepository('FoBundle:EnfantExercices');
        $enfantId = $session->get('enfant')->getId();
        $listSocial = $enfantExercicesRepo->getResultByEnfantid($enfantId);

        // Pagination
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $listSocial, $this->get('request')->query->get('page', 1)/* page number */, 20/* limit per page */
        );

        return $pagination;
    }

}
