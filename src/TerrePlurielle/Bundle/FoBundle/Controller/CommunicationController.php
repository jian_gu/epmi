<?php

namespace TerrePlurielle\Bundle\FoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use TerrePlurielle\Bundle\FoBundle\Entity\Enfant;
use Symfony\Component\HttpFoundation\Response;

class CommunicationController extends Controller {
    /*
     * Fonction rendre page
     */

    public function indexAction() {
        $session = $this->getRequest()->getSession();

        if (!$session->get('enfant') && !$session->get('currentNiveau')) {
            // we call function to let the user choose a child
            return $this->redirect($this->generateUrl('fo_homepage'));
        } else {
            $sessionNiveau = $session->get('currentNiveau');
//            $snEnfant = $sessionEnfant;
            if ($sessionNiveau != '') {
                $nv = $sessionNiveau;
                switch ($nv) {
                    case '1':
                        return $this->niveauAction(1, 25);
                        break;
                    case '2':
                        return $this->niveauAction(2, 50);
                        break;
                    case '3':
                        return $this->niveauAction(3, 75);
                        break;
                    case '4':
                        return $this->niveauAction(4, 100);
                        break;

                    default:
                        break;
                }
            } else {
                // Flash message child has no level
                $this->get('session')->getFlashBag()->add(
                        'warning', 'Donnez l\'enfant un niveau!'
                );
                return $this->redirect($this->generateUrl('fo_homepage'));
                //throw $this->createNotFoundException('Veuillez associer un niveau à l\'enfant');
            }
        }
    }

    /**
     * 
     * @param integer $niveau
     * @return type
     */
    public function niveauAction($niveau, $progres) {
        $theme_repo = $this->getDoctrine()->getRepository('FoBundle:Theme');
        $picto_repo = $this->getDoctrine()->getRepository('FoBundle:Pictogramme');
        $img_repo = $this->getDoctrine()->getRepository('FoBundle:Image');
        $enfantId = $this->getRequest()->getSession()->get('enfant')->getId();
        $pictos;

        //$themes = $theme_repo->findAll();
        $themes = $theme_repo->getThemesByNiveau($niveau);
        $pictos_proto = $picto_repo->getPictosByNiveau($niveau);
        $images = $img_repo->getImagesByEnfantIdNiveau($enfantId, $niveau);

        // No pictos
        if (!$pictos_proto) {
            // Flash message child has no level
            $this->get('session')->getFlashBag()->add(
                    'warning', 'Aucun pictogramme disponible!'
            );
            return $this->redirect($this->generateUrl('fo_homepage'));
        }

        foreach ($pictos_proto as $key => $picto) {
            if ($images) {
                $pictos[] = $picto;
                foreach ($images as $image) {
                    if ($picto->getRefMot()->getId() == $image->getRefPictogramme()->getRefMot()->getId()) {
                        unset($pictos[$key]);
                        $pictos[$key] = $image;
                    }
                }
            } else {
                $pictos[] = $picto;
            }
        }

        return $this->render('FoBundle:Communication:niveau' . $niveau . '.html.twig', array(
                    "themes" => $themes,
                    "pictos" => $pictos,
                    "pptg" => $progres
                        )
        );
    }

    /*
     * Fonction recuperer les pictos d'un theme
     */

    function getPictosByThemeAction() {
        $themeId = $this->getRequest()->get('theme_id');
        $picto_repo = $this->getDoctrine()->getRepository('FoBundle:Pictogramme');
        $pictos = $picto_repo->getPictosByTheme($themeId);
        //$pictosJsonfy = json_encode($pictos);

        return $this->render('FoBundle:Communication:__pictosByTheme.html.twig', array(
                    "pictos" => $pictos
                        )
        );
    }

    /*
     * Fonction traiter la requete vers google tts
     */

    public function soundRequestAction() {
        $texts = $this->get('Request')->get('text_to_read');
        $qs = http_build_query(array("ie" => "utf-8", "tl" => "fr", "q" => $texts));
        $ctx = stream_context_create(array("http" => array("method" => "GET", "header" => "Referer: \r\n")));
        // fetch the sound file content
        $soundfile = file_get_contents("http://translate.google.com/translate_tts?" . $qs, false, $ctx);

        header("Content-type: audio/mpeg");
        header("Content-Transfer-Encoding: binary");
        header('Pragma: no-cache');
        header('Expires: 0');

        echo $soundfile;
        die();
    }

    public function savePhraseAction() {
        $reponse = '';
        $phrase = $this->getRequest()->get('my_phrase');
        if (!$phrase) {
            // Flash message
            $this->get('session')->getFlashBag()->add(
                    'notice', 'Phrase vide!'
            );
            return;
        }
        $newPhrase = new \TerrePlurielle\Bundle\FoBundle\Entity\PhraseComposee();
        //$sessionEnfant = new Enfant();
        $sessionEnfant = $this->getRequest()->getSession()->get('enfant');
        //$sessionNiveau = new \TerrePlurielle\Bundle\FoBundle\Entity\Niveau();
        $sessionNiveau = $this->getRequest()->getSession()->get('enfant')->getRefNiveau();

        $e = $this->getDoctrine()->getManager()->getRepository('FoBundle:Enfant')->findBy(array('id' => $sessionEnfant->getId()));
        $n = $this->getDoctrine()->getManager()->getRepository('FoBundle:Niveau')->findBy(array('id' => $sessionNiveau));

        $newPhrase->setContenu($phrase);
        $newPhrase->setRefEnfant($e[0]);
        $newPhrase->setRefNiveau($n[0]);

        $em = $this->getDoctrine()->getManager();
        $em->persist($newPhrase);
        $em->flush();

        // Flash message? non on fait on ajax
//        $this->get('session')->getFlashBag()->add(
//                'notice', 'Phrase composée enregistrée.'
//        );
        // Ou on utilise la réponse ajax/json
        $reponse['result'] = "success";

        return new Response(json_encode($reponse));
    }

    /**
     * Get current logged user context
     * @return type
     */
    public function userLogged() {
        return $this->get('security.context')->getToken()->getUser();
    }

    /**
     * Function return une phrase proposée, pour particulièrement le niveau 3,
     * called by ajax, 
     * parameter: array des mots placés par l'enfant
     * 
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function phraseSimilaireAction() {
        $reponse;
        $arrayMots = $this->getRequest()->get('param_arrayMots');
        if (!$arrayMots) {
            return;
        }
        
        $pp = new \TerrePlurielle\Bundle\FoBundle\Entity\PhraseProposee();
        $pp = $this->getDoctrine()->getManager()->getRepository('FoBundle:PhraseProposee')->getOnePhraseSimilaire($arrayMots);
        
        if ($pp == null) {
            $reponse['result'] = 'fail';
        } else {
            $reponse['result'] = 'success';
            $reponse['content'] = $pp[0]->getContenu();
        }
        
        return new Response(json_encode($reponse));
    }
    
    /**
     * Function return mot(s) auto fill pour niveau 3
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function nv3AutoFillAction() {
        $reponse;
        $id1 = $this->getRequest()->get('param_verbeId');
        $id2 = $this->getRequest()->get('param_complementId');
        
        $af = $this->getDoctrine()->getManager()->getRepository('FoBundle:Nv3AutoFill')->getNv3AutoFill($id1, $id2);
        
        if ($af == null) {
            $reponse['result'] = 'fail';
        } else {
            $reponse['result'] = 'success';
            // get the auto-fill word
            $reponse['afw'] = $af[0]->getAutoFill();
            // get the complément word
            $reponse['complement'] = $af[0]->getRefComplement()->getTitre();
        }
        
        return new Response(json_encode($reponse));
    }

}

