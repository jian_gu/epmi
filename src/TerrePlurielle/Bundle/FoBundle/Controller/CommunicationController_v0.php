<?php

namespace TerrePlurielle\Bundle\FoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use TerrePlurielle\Bundle\FoBundle\Entity\Enfant;
use Symfony\Component\HttpFoundation\Response;

class CommunicationController extends Controller {
    /*
     * Fonction rendre page
     */

    public function indexAction() {
        $session = $this->getRequest()->getSession();
        $enfant = new Enfant();
        $enfant_repo = $this->getDoctrine()->getManager()->getRepository('FoBundle:Enfant');
        $currentUser = $this->userLogged();


        $session->set('current_user', $currentUser);

//        $form = $this->createFormBuilder($enfant)
//                ->add('id', 'entity', array(
//                    'class' => 'FoBundle:Enfant',
//                    'property' => 'nameDOB',
//                    'query_builder' => function(EntityRepository $er) {
//                return $er->createQueryBuilder('e')
//                        ->where($this->whereClause($this->userLogged()))
//                        ->orderBy('e.nom', 'ASC');
//            },
//                        )
//                )
//                ->getForm();
        // On recupere une liste d'enfants qui appartient à l'utilisateur connecté
        // function dans Entity repository Enfant utilisée
        $form = $this->createFormBuilder($enfant)
                ->add('id', 'entity', array(
                    'class' => 'FoBundle:Enfant',
                    'property' => 'nameDOB',
                    'choices' => $enfant_repo->getChildrenByLoggedUser($currentUser->getRealRoles(), $currentUser->getId())
                        )
                )
                ->getForm();


        $request = $this->get('request');

        // On vérifie qu'elle est de type POST
        if ($request->getMethod() == 'POST') {
            $formPost = $request->get('form');
            $currentEnfantArray = $enfant_repo->findBy(array('id' => $formPost['id']));
            $currentEnfant = $currentEnfantArray[0];

            $session->set('enfant', $currentEnfant);

            if ($currentEnfant->getRefNiveau()) {
                $session->set('currentNiveau', $currentEnfant->getRefNiveau()->getTitre());
            } else {
                // Flash message child has no level
                $this->get('session')->getFlashBag()->add(
                        'notice', 'Donnez l\'enfant un niveau!'
                );
            }
        }

        if ($session->get('enfant') && $session->get('currentNiveau')) {
            $sessionNiveau = $session->get('currentNiveau');
//            $snEnfant = $sessionEnfant;
            if ($sessionNiveau != '') {
                $nv = $sessionNiveau;
                switch ($nv) {
                    case '1':
                        return $this->niveauAction(1);
                        break;
                    case '2':
                        return $this->niveauAction(2);
                        break;
                    case '3':
                        return $this->niveauAction(3);
                        break;
                    case '4':
                        return $this->niveauAction(4);
                        break;

                    default:
                        break;
                }
            } else {
                throw $this->createNotFoundException('Veuillez associer un niveau à l\'enfant');
            }
        }

        return $this->render('FoBundle:Communication:index.html.twig', array(
                    'form' => $form->createView(),
                        )
        );
    }

    protected function whereClause($currentUser) {
//        $enfant_repo = $this->getDoctrine()->getRepository('FoBundle:Enfant');
//        $currentUser = $this->userLogged();
        $currentUserRoleArray = $currentUser->getRealRoles();
        $cUId = $currentUser->getId();

        foreach ($currentUserRoleArray as $value) {
            switch ($value) {
                case 'ROLE_EDUCATEUR':
                    return 'e.refEducateur = \'' . $cUId . '\'';
                    break;
                case 'ROLE_PARENT':
                    return 'e.refParent = \'' . $cUId . '\'';
                    break;
                case 'ROLE_SUPER_ADMIN':
                    return 'NOT e.id=0';
                    break;

                default:
                    break;
            }
        }
    }

    /*
     * enlever variable enfant dans la session
     */

    public function cleanSessionEnfantAction() {
        if ($this->getRequest()->getSession()->get('enfant') || $this->getRequest()->getSession()->get('currentNiveau')) {
            $this->getRequest()->getSession()->remove('enfant');
            $this->getRequest()->getSession()->remove('currentNiveau');
        }
        return $this->indexAction();
    }


    /**
     * 
     * @param integer $niveau
     * @return type
     */
    public function niveauAction($niveau) {
        $theme_repo = $this->getDoctrine()->getRepository('FoBundle:Theme');
        $picto_repo = $this->getDoctrine()->getRepository('FoBundle:Pictogramme');
        $img_repo = $this->getDoctrine()->getRepository('FoBundle:Image');
        $enfantId = $this->getRequest()->getSession()->get('enfant')->getId();
        $pictos;

        //$themes = $theme_repo->findAll();
        $themes = $theme_repo->getThemesByNiveau($niveau);
        $pictos_proto = $picto_repo->getPictosByNiveau($niveau);
        $images = $img_repo->getImagesByEnfantIdNiveau($enfantId, $niveau);

        foreach ($pictos_proto as $key => $picto) {
            if ($images) {
                $pictos[] = $picto;
                foreach ($images as $image) {
                    if ($picto->getRefMot()->getId() == $image->getRefPictogramme()->getRefMot()->getId()) {
                        unset($pictos[$key]);
                        $pictos[$key] = $image;
                    }
                }
            } else {
                $pictos[] = $picto;
            }
        }

        return $this->render('FoBundle:Communication:niveau'.$niveau.'.html.twig', array(
                    "themes" => $themes,
                    "pictos" => $pictos
                        )
        );
    }

    
    /*
     * Fonction recuperer les pictos d'un theme
     */

    function getPictosByThemeAction() {
        $themeId = $this->getRequest()->get('theme_id');
        $picto_repo = $this->getDoctrine()->getRepository('FoBundle:Pictogramme');
        $pictos = $picto_repo->getPictosByTheme($themeId);
        //$pictosJsonfy = json_encode($pictos);

        return $this->render('FoBundle:Communication:__pictosByTheme.html.twig', array(
                    "pictos" => $pictos
                        )
        );
    }

    /*
     * Fonction traiter la requete vers google tts
     */

    public function soundRequestAction() {
        $texts = $this->get('Request')->get('text_to_read');
        $qs = http_build_query(array("ie" => "utf-8", "tl" => "fr", "q" => $texts));
        $ctx = stream_context_create(array("http" => array("method" => "GET", "header" => "Referer: \r\n")));
        // fetch the sound file content
        $soundfile = file_get_contents("http://translate.google.com/translate_tts?" . $qs, false, $ctx);

        header("Content-type: audio/mpeg");
        header("Content-Transfer-Encoding: binary");
        header('Pragma: no-cache');
        header('Expires: 0');

        echo $soundfile;
        die();
    }

    public function savePhraseAction() {
        $reponse = '';
        $phrase = $this->getRequest()->get('my_phrase');
        if (!$phrase) {
            // Flash message
            $this->get('session')->getFlashBag()->add(
                    'notice', 'Phrase vide!'
            );
            return;
        }
        $newPhrase = new \TerrePlurielle\Bundle\FoBundle\Entity\PhraseComposee();
        //$sessionEnfant = new Enfant();
        $sessionEnfant = $this->getRequest()->getSession()->get('enfant');
        //$sessionNiveau = new \TerrePlurielle\Bundle\FoBundle\Entity\Niveau();
        $sessionNiveau = $this->getRequest()->getSession()->get('enfant')->getRefNiveau();

        $e = $this->getDoctrine()->getManager()->getRepository('FoBundle:Enfant')->findBy(array('id' => $sessionEnfant->getId()));
        $n = $this->getDoctrine()->getManager()->getRepository('FoBundle:Niveau')->findBy(array('id' => $sessionNiveau));

        $newPhrase->setContenu($phrase);
        $newPhrase->setRefEnfant($e[0]);
        $newPhrase->setRefNiveau($n[0]);

        $em = $this->getDoctrine()->getManager();
        $em->persist($newPhrase);
        $em->flush();

        // Flash message? non on fait on ajax
//        $this->get('session')->getFlashBag()->add(
//                'notice', 'Phrase composée enregistrée.'
//        );
        // Ou on utilise la réponse ajax/json
        $reponse['result'] = "success";

        return new Response(json_encode($reponse));
    }

    /**
     * Get current logged user context
     * @return type
     */
    public function userLogged() {
        return $this->get('security.context')->getToken()->getUser();
    }

}
