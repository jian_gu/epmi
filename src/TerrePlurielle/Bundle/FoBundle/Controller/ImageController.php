<?php

namespace TerrePlurielle\Bundle\FoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use TerrePlurielle\Bundle\FoBundle\Entity\Image;
use TerrePlurielle\Bundle\FoBundle\Form\Type\ImageType;

/**
 * Image controller.
 *
 */
class ImageController extends Controller {

    /**
     * Return enfant dans la session
     * @return type
     */
    private function getSessionEnfant() {
        return $this->get('session')->get('enfant');
    }

    /**
     * Lists all Image entities.
     *
     */
    public function indexAction() {
        // Return si l'enfant n'est pas choisi
        if (!$this->getSessionEnfant()) {
            // we call function to let the user choose a child
            return $this->redirect($this->generateUrl('fo_homepage'));
        } else {
            $em = $this->getDoctrine()->getManager();
            $enfant = $this->getSessionEnfant();

            $entities = $em->getRepository('FoBundle:Image')->getImagesByEnfantId($enfant->getId());
//        $entities = $em->getRepository('FoBundle:Image')->findAll();
            // Pagination
            $paginator = $this->get('knp_paginator');
            $pagination = $paginator->paginate(
                    $entities, $this->get('request')->query->get('page', 1)/* page number */, 20/* limit per page */
            );

            return $this->render('FoBundle:Image:index.html.twig', array(
                        'entities' => $pagination,
            ));
        }
    }

    /**
     * Creates a new Image entity.
     *
     */
    public function createAction(Request $request) {
        // Return si l'enfant n'est pas choisi
        if (!$this->getSessionEnfant()) {
            // we call function to let the user choose a child
            return $this->redirect($this->generateUrl('fo_homepage'));
        }
        $entity = new Image();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            // upload file
            $entity->upload('');
            $em->persist($entity);
            $em->flush();

            // Flash message
            $this->get('session')->getFlashBag()->add(
                    'success', 'Image ajoutée.'
            );

            return $this->redirect($this->generateUrl('image_show', array('id' => $entity->getId())));
        }

        return $this->render('FoBundle:Image:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Image entity.
     *
     * @param Image $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Image $entity) {
        $form = $this->createForm(new ImageType($this->getSessionEnfant()->getId()), $entity, array(
            'action' => $this->generateUrl('image_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Créer'));

        return $form;
    }

    /**
     * Displays a form to create a new Image entity.
     *
     */
    public function newAction() {
        // Return si l'enfant n'est pas choisi
        if (!$this->getSessionEnfant()) {
            // we call function to let the user choose a child
            return $this->redirect($this->generateUrl('fo_homepage'));
        }
        $entity = new Image();
        $form = $this->createCreateForm($entity);

        return $this->render('FoBundle:Image:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Image entity.
     *
     */
    public function showAction($id) {
        // Return si l'enfant n'est pas choisi
        if (!$this->getSessionEnfant()) {
            // we call function to let the user choose a child
            return $this->redirect($this->generateUrl('fo_homepage'));
        }
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('FoBundle:Image')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Image entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('FoBundle:Image:show.html.twig', array(
                    'entity' => $entity,
                    'delete_form' => $deleteForm->createView(),));
    }

    /**
     * Displays a form to edit an existing Image entity.
     *
     */
    public function editAction($id) {
        // Return si l'enfant n'est pas choisi
        if (!$this->getSessionEnfant()) {
            // we call function to let the user choose a child
            return $this->redirect($this->generateUrl('fo_homepage'));
        }
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('FoBundle:Image')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Image entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('FoBundle:Image:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a Image entity.
     *
     * @param Image $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Image $entity) {
        $form = $this->createForm(new ImageType($this->getSessionEnfant()->getId()), $entity, array(
            'action' => $this->generateUrl('image_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Mettre à jour'));

        return $form;
    }

    /**
     * Edits an existing Image entity.
     *
     */
    public function updateAction(Request $request, $id) {
        // Return si l'enfant n'est pas choisi
        if (!$this->getSessionEnfant()) {
            // we call function to let the user choose a child
            return $this->redirect($this->generateUrl('fo_homepage'));
        }
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('FoBundle:Image')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Image entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            // upload file
            $entity->upload('');
            $em->flush();

            // Flash message
            $this->get('session')->getFlashBag()->add(
                    'success', 'Image mise à jour.'
            );

            return $this->redirect($this->generateUrl('image_edit', array('id' => $id)));
        }

        return $this->render('FoBundle:Image:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Image entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        // Return si l'enfant n'est pas choisi
        if (!$this->getSessionEnfant()) {
            // we call function to let the user choose a child
            return $this->redirect($this->generateUrl('fo_homepage'));
        }
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('FoBundle:Image')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Image entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        // Flash message
        $this->get('session')->getFlashBag()->add(
                'success', 'Image supprimée.'
        );

        return $this->redirect($this->generateUrl('image'));
    }

    /**
     * Creates a form to delete a Image entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('image_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Supprimer'))
                        ->getForm()
        ;
    }

}
