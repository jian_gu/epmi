<?php

namespace TerrePlurielle\Bundle\FoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use TerrePlurielle\Bundle\FoBundle\Entity\Enfant;
use TerrePlurielle\Bundle\FoBundle\Form\Type\EnfantType;

class HomeController extends Controller {

    /**
     * 
     * @return type
     */
    public function indexAction() {
        $session = $this->getRequest()->getSession();
        $loggedUser = $this->getLoggedUser();
        if ($loggedUser && $loggedUser != "anon.") {
            // user connected
            if ($session->get('enfant')) {
                // child is selected and saved in the session
                return $this->enfantAction($session->get('enfant'));
            } else {
                // we call function to let the user choose a child
                return $this->chooseAction();
            }

            // return $this->render('FoBundle:Home:choose.html.twig');
        } else {
            // no user connected
//            return $this->render('FoBundle:Home:index.html.twig');
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
    }

    /**
     * Selectionner un enfant
     * @return type
     */
    public function chooseAction() {
        $session = $this->getRequest()->getSession();
        $enfant = new Enfant();
        $enfant_repo = $this->getDoctrine()->getManager()->getRepository('FoBundle:Enfant');
        $currentUser = $this->getLoggedUser();

        $session->set('current_user', $currentUser);

        // On recupere une liste d'enfants qui appartient à l'utilisateur connecté
        // function dans Entity repository Enfant utilisée
        $formSelect = $this->createFormBuilder($enfant)
                ->add('id', 'entity', array(
                    'class' => 'FoBundle:Enfant',
                    'property' => 'nameDOB',
                    'expanded' => FALSE,
                    'multiple' => FALSE,
                    'choices' => $enfant_repo->getChildrenByLoggedUser($currentUser->getRealRoles(), $currentUser->getId())
                        )
                )
                ->getForm();


        $request = $this->get('request');

        // On vérifie qu'elle est de type POST
        if ($request->getMethod() == 'POST') {
            $formPost = $request->get('form');
            $currentEnfantArray = $enfant_repo->findBy(array('id' => $formPost['id']));
            $currentEnfant = $currentEnfantArray[0];

            $session->set('enfant', $currentEnfant);

            if ($currentEnfant->getRefNiveau()) {
                $session->set('currentNiveau', $currentEnfant->getRefNiveau()->getTitre());
            } else {
                // Flash message child has no level
                $this->get('session')->getFlashBag()->add(
                        'warning', 'Donnez l\'enfant un niveau!'
                );
            }
        }

        // Si l'enfant est déjà sélectionné
        if (isset($currentEnfant)) {
            return $this->enfantAction($currentEnfant);
        } else {
            // otherwise, select a child
            return $this->render('FoBundle:Home:choose.html.twig', array(
                        'formSelect' => $formSelect->createView(),
                            )
            );
        }
    }

    public function addEnfantAction() {
        $usr = $this->getLoggedUser();
        $usrRole = $usr->getRealRoles();

        // On crée un objet Enfant
        $enfant = new Enfant();
        // On crée le formulaire
        $form = $this->createCreateForm($enfant);

        // Détecter le rôle d'utilisateur connecté
        if ($usrRole[0] === 'ROLE_EDUCATEUR') {
            $enfant->setRefEducateur($usr);
            $form->remove('refEducateur');
        }

        if ($usrRole[0] == 'ROLE_PARENT') {
            $enfant->setRefParent($usr);
            $form->remove('refParent');
        }

        // On récupère la requête
        $request = $this->get('request');
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            // upload file
            $enfant->upload('');
            $em->persist($enfant);
            $em->flush();

            // Flash msg
            $this->get('session')->getFlashBag()->add(
                    'success', 'Enfant ajouté avec succès!'
            );

            return $this->redirect($this->generateUrl('home_add_enfant'));
        }

        return $this->render('FoBundle:Home:addEnfant.html.twig', array(
                    'formAdd' => $form->createView(),
                        )
        );
    }

    /**
     * Creates a form to create a Enfant entity.
     *
     * @param Enfant $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Enfant $entity) {
        $form = $this->createForm(new EnfantType(), $entity, array(
            'action' => $this->generateUrl('home_add_enfant'),
            'method' => 'POST',
        ));

         //$form->add('submit', 'submit', array('label' => 'Créer'));

        return $form;
    }

    public function modEnfantAction() {
        $session = $this->getRequest()->getSession();
        $currentEnfant = $session->get('enfant');
        // On récupère la requête
        $request = $this->get('request');
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('FoBundle:Enfant')->find($currentEnfant->getId());
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            // upload file
            $entity->upload('');
            $em->flush();

            // Reset current enfant et currentNiveau in session
            if ($session->get('enfant')) {
                if ($entity->getId() == $session->get('enfant')->getId()) {
                    $session->remove('enfant');
                    $session->remove('currentNiveau');
                    $session->set('enfant', $entity);
                    $session->set('currentNiveau', $entity->getRefNiveau());
                }
            }

            // Flash message
            $this->get('session')->getFlashBag()->add(
                    'success', 'Modification éffectuée.'
            );
            return $this->redirect($this->generateUrl('home_mod_enfant'));
        }

        return $this->render('FoBundle:Home:modEnfant.html.twig', array(
                    //'entity' => $currentEnfant,
                    'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a Enfant entity.
     *
     * @param Enfant $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Enfant $entity) {
        $form = $this->createForm(new EnfantType(), $entity, array(
            'action' => $this->generateUrl('home_mod_enfant', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Mettre à jour'));

        return $form;
    }

    /**
     * Enfant sélectionné, affiche le
     * @param type $enfant
     * @return type
     */
    public function enfantAction($enfant) {
		if($enfant->getRefNiveau() == null) {
			
			return $this->render('FoBundle:Home:enfant.html.twig', array(
						'enfant' => $enfant,
						'pptg' => 0,
							)
			);
		} else {
			$nv = $enfant->getRefNiveau()->getTitre();
			$ptgNiveau = intval($nv)*25;
			// goto enfant infos
			return $this->render('FoBundle:Home:enfant.html.twig', array(
						'enfant' => $enfant,
						'pptg' => $ptgNiveau,
							)
			);
		}
        
    }

    /*
     * enlever variable enfant dans la session
     */

    public function cleanSessionEnfantAction() {
        if ($this->getRequest()->getSession()->get('enfant') || $this->getRequest()->getSession()->get('currentNiveau')) {
            $this->getRequest()->getSession()->remove('enfant');
            $this->getRequest()->getSession()->remove('currentNiveau');
        }
        return $this->chooseAction();
    }

    /**
     * get logged user
     * @return null
     */
    private function getLoggedUser() {
        $token = $this->get('security.context')->getToken();
        if ($token->getUser()) {
            return $token->getUser();
        } else {
            return NULL;
        }
    }

}
