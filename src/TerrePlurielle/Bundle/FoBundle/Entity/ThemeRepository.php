<?php

namespace TerrePlurielle\Bundle\FoBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * ThemeRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ThemeRepository extends EntityRepository {

    function getThemesByNiveau($niveau) {
        $query = $this->getEntityManager()->createQuery(
                        'SELECT t
                         FROM TerrePlurielle\Bundle\FoBundle\Entity\Theme t
                         JOIN TerrePlurielle\Bundle\FoBundle\Entity\Mot m
                         WHERE m.refTheme = t.id
                         AND m.niveau <= :niveau
                         ORDER BY t.id ASC'
                )->setParameter('niveau', $niveau);

        return $query->getResult();
    }

}
