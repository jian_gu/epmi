<?php

namespace TerrePlurielle\Bundle\FoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Theme
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="TerrePlurielle\Bundle\FoBundle\Entity\ThemeRepository")
 */
class Theme
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Titre", type="string", length=255)
     */
    private $titre;

    /**
     * @var string
     *
     * @ORM\Column(name="PictoUrl", type="string", length=255)
     */
    private $pictoUrl;
    
    /**
     *  @Assert\File( maxSize="20M")
     */
    protected $pictoFile;


    public function __toString()
    {
        return $this->titre;
    }

    // ===================
    // == Saving files ===
    // ===================
    /**
     * Set pictoFile.
     *
     * @param UploadedFile $pictoFile
     */
    public function setPictoFile(UploadedFile $pictoFile = null) {
        $this->pictoFile = $pictoFile;
    }

    /**
     * Get pictoFile.
     *
     * @return UploadedFile
     */
    public function getPictoFile() {
        return $this->pictoFile;
    }

    //    ========== File uploading ==========
    public function getAbsolutePathPicto() {
        return null === $this->pictoUrl ? null : $this->getUploadRootDir() . '/theme/' . $this->pictoUrl;
    }

    public function getWebPathPicto() {
        return null === $this->pictoUrl ? null : $this->getUploadDir() . '/theme/' . $this->pictoUrl;
    }

    protected function getUploadRootDir($basepath) {
        // the absolute directory path where uploaded documents should be saved
        return $basepath . $this->getUploadDir();
    }

    protected function getUploadDir() {
        // get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
        return 'uploads/images/theme';
    }

    public function upload($basepath) {
        // the file property can be empty if the field is not required
        if (null === $this->pictoFile) {
            return;
        }

        if (null === $basepath) {
            return;
        }

        // we use the original file name here but you should
        // sanitize it at least to avoid any security issues
        // move takes the target directory and then the target filename to move to
        $this->pictoFile->move($this->getUploadRootDir($basepath), $this->pictoFile->getClientOriginalName());

        // set the path property to the filename where you'ved saved the file
        $this->setPictoUrl($this->pictoFile->getClientOriginalName());

        // clean up the file property as you won't need it anymore
        $this->pictoFile = null;
    }
    // ===================
    // =Saving files fin==
    // ===================

    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre
     *
     * @param string $titre
     * @return Theme
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string 
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set pictoUrl
     *
     * @param string $pictoUrl
     * @return Theme
     */
    public function setPictoUrl($pictoUrl)
    {
        $this->pictoUrl = $pictoUrl;

        return $this;
    }

    /**
     * Get pictoUrl
     *
     * @return string 
     */
    public function getPictoUrl()
    {
        return $this->pictoUrl;
    }
}
