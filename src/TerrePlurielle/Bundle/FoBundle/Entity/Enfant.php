<?php

namespace TerrePlurielle\Bundle\FoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Enfant
 *
 * @ORM\Table(name="Enfant")
 * @ORM\Entity(repositoryClass="TerrePlurielle\Bundle\FoBundle\Entity\EnfantRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Enfant {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="Prenom", type="string", length=255)
     */
    private $prenom;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DateDeNaissance", type="datetime")
     */
    private $dateDeNaissance;

    /**
     * @var string
     *
     * @ORM\Column(name="Observation", type="text", nullable=true)
     */
    private $observation;

    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="Niveau")
     * @ORM\JoinColumn(name="ref_niveau", referencedColumnName="id", nullable=true)
     */
    private $refNiveau;

    /**
     * @var string 
     *
     * @ORM\Column(name="avatar", type="string", length=255, nullable=true)
     *
     */
    private $avatar;

    /**
     * @Assert\File(maxSize="4M")
     */
    protected $file;

    /**
     * @ORM\ManyToOne(targetEntity="TerrePlurielle\Bundle\BoBundle\Entity\User",cascade={"persist"})
     * @ORM\JoinColumn(name="ref_parent", referencedColumnName="id")
     */
    private $refParent;

    /**
     * @ORM\ManyToOne(targetEntity="TerrePlurielle\Bundle\BoBundle\Entity\User",cascade={"persist"})
     * @ORM\JoinColumn(name="ref_educateur", referencedColumnName="id")
     */
    private $refEducateur;

//    /**
//     * 
//     * @ORM\ManyToMany(targetEntity="PhraseComposee",cascade={"persist", "remove"})
//     * @ORM\JoinTable(name="Enfant_PhraseComposees",
//    *      joinColumns={@ORM\JoinColumn(name="EnfantId", referencedColumnName="id")},
//     *      inverseJoinColumns={@ORM\JoinColumn(name="PhraseComposeeId", referencedColumnName="id",unique=true)}
//     *      )
//     */
//    private $refPhraseComposee;
    // *
    //  * 
    //  * @ORM\ManyToMany(targetEntity="Exercices",cascade={"persist", "remove"})
    //  * @ORM\JoinTable(name="Enfant_Exercices",
    //  *      joinColumns={@ORM\JoinColumn(name="EnfantId", referencedColumnName="id")},
    //  *      inverseJoinColumns={@ORM\JoinColumn(name="ExerciceId", referencedColumnName="id",unique=true)}
    //  *      )
    // private $refExercice;

    public function __construct() {
        // $this->refExercice = new \Doctrine\Common\Collections\ArrayCollection();
        //$this->refPhraseComposee = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString() {
        return $this->nom . " " . $this->prenom;
    }

    public function getNameDOB() {
        return $this->nom . " " . $this->prenom . ' ' . $this->dateDeNaissance->format('d/m/Y');
    }

//    /**
//     * Add refPhraseComposee
//     *
//     * @param \TerrePlurielle\Bundle\FoBundle\Entity\PhraseComposee $refPhraseComposee
//     * @return Enfant
//     */
//    public function addRefPhraseComposee(\TerrePlurielle\Bundle\FoBundle\Entity\PhraseComposee $refPhraseComposee) {
//        $this->refPhraseComposee[] = $refPhraseComposee;
//        return $this;
//    }
    //   /**
    //    * Remove refPhraseComposee
//     *
//     * @param \TerrePlurielle\Bundle\FoBundle\Entity\PhraseComposee $refPhraseComposee
//     */
//    public function removeRefPhraseComposee(\TerrePlurielle\Bundle\FoBundle\Entity\PhraseComposee $refPhraseComposee) {
//       $this->refPhraseComposee->removeElement($refPhraseComposee);
//    }

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null) {
        $this->file = $file;
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile() {
        return $this->file;
    }

    ///////////////////////////////////////////
    /*     * ********* UPLOAD FILE ***************** */
    ///////////////////////////////////////////

    public function getAbsolutePathAvatar() {
        return null === $this->avatar ? null : $this->getUploadRootDir() . '/enfant/' . $this->avatar;
    }

    public function getWebPathAvatar() {
        return null === $this->avatar ? null : $this->getUploadDir() . '/enfant/' . $this->avatar;
    }

    protected function getUploadRootDir($basepath) {
        // the absolute directory path where uploaded documents should be saved
        return $basepath . $this->getUploadDir();
    }

    protected function getUploadDir() {
        // get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
        return 'uploads/images/enfant';
    }

    public function upload($basepath) {
        // the file property can be empty if the field is not required
        if (null === $this->file) {
            return;
        }

        if (null === $basepath) {
            return;
        }

        // we use the original file name here but you should
        // sanitize it at least to avoid any security issues
        // move takes the target directory and then the target filename to move to
        $this->file->move(
                $this->getUploadRootDir($basepath), 
                $this->file->getClientOriginalName());

        // set the path property to the filename where you'ved saved the file
        $this->setAvatar($this->file->getClientOriginalName());

        // clean up the file property as you won't need it anymore
        $this->file = null;
    }

    ////////////////////////////
    //** Getters & setters **//
    ////////////////////////////
//    /**
//     * Get refPhraseComposee
//    *
//    * @return \Doctrine\Common\Collections\Collection 
//    */
//    public function getRefPhraseComposee() {
//        return $this->refPhraseComposee;
//    }

    


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Enfant
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     * @return Enfant
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string 
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set dateDeNaissance
     *
     * @param \DateTime $dateDeNaissance
     * @return Enfant
     */
    public function setDateDeNaissance($dateDeNaissance)
    {
        $this->dateDeNaissance = $dateDeNaissance;

        return $this;
    }

    /**
     * Get dateDeNaissance
     *
     * @return \DateTime 
     */
    public function getDateDeNaissance()
    {
        return $this->dateDeNaissance;
    }

    /**
     * Set observation
     *
     * @param string $observation
     * @return Enfant
     */
    public function setObservation($observation)
    {
        $this->observation = $observation;

        return $this;
    }

    /**
     * Get observation
     *
     * @return string 
     */
    public function getObservation()
    {
        return $this->observation;
    }

    /**
     * Set avatar
     *
     * @param string $avatar
     * @return Enfant
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;

        return $this;
    }

    /**
     * Get avatar
     *
     * @return string 
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * Set refNiveau
     *
     * @param \TerrePlurielle\Bundle\FoBundle\Entity\Niveau $refNiveau
     * @return Enfant
     */
    public function setRefNiveau(\TerrePlurielle\Bundle\FoBundle\Entity\Niveau $refNiveau = null)
    {
        $this->refNiveau = $refNiveau;

        return $this;
    }

    /**
     * Get refNiveau
     *
     * @return \TerrePlurielle\Bundle\FoBundle\Entity\Niveau 
     */
    public function getRefNiveau()
    {
        return $this->refNiveau;
    }

    /**
     * Set refParent
     *
     * @param \TerrePlurielle\Bundle\BoBundle\Entity\User $refParent
     * @return Enfant
     */
    public function setRefParent(\TerrePlurielle\Bundle\BoBundle\Entity\User $refParent = null)
    {
        $this->refParent = $refParent;

        return $this;
    }

    /**
     * Get refParent
     *
     * @return \TerrePlurielle\Bundle\BoBundle\Entity\User 
     */
    public function getRefParent()
    {
        return $this->refParent;
    }

    /**
     * Set refEducateur
     *
     * @param \TerrePlurielle\Bundle\BoBundle\Entity\User $refEducateur
     * @return Enfant
     */
    public function setRefEducateur(\TerrePlurielle\Bundle\BoBundle\Entity\User $refEducateur = null)
    {
        $this->refEducateur = $refEducateur;

        return $this;
    }

    /**
     * Get refEducateur
     *
     * @return \TerrePlurielle\Bundle\BoBundle\Entity\User 
     */
    public function getRefEducateur()
    {
        return $this->refEducateur;
    }
}
