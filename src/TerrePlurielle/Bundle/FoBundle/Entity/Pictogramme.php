<?php

namespace TerrePlurielle\Bundle\FoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Pictogramme
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="TerrePlurielle\Bundle\FoBundle\Entity\PictogrammeRepository")
 */
class Pictogramme {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="PictoUrl", type="string", length=255, nullable=false)
     */
    protected $pictoUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="ImgUrl", type="string", length=255, nullable=true)
     */
    private $imgUrl;

    /**
     * @ORM\OneToOne(targetEntity="Mot", cascade={"persist"})
     * 
     * @ORM\JoinColumn(name="ref_mot", referencedColumnName="id")
     */
    private $refMot;

    /**
     * @ORM\OneToOne(targetEntity="PhraseProposee", cascade={"persist"})
     * 
     * @ORM\JoinColumn(name="ref_phrase", referencedColumnName="id", nullable=true)
     */
    private $refPhrase;

//    /**
//     * @var string
//     * @ORM\ManyToOne(targetEntity="Theme")
//     * @ORM\JoinColumn(name="ref_theme", referencedColumnName="id", nullable=true)
//     */
//    private $refTheme;

    /**
     *  @Assert\File( maxSize="1M")
     */
    protected $pictoFile;

    // ===================
    // == Saving files ===
    // ===================
    //    ========== File uploading ==========
    public function getAbsolutePathPicto() {
        return null === $this->pictoUrl ? null : $this->getUploadRootDir() . '/picto/' . $this->pictoUrl;
    }

    public function getWebPathPicto() {
        return null === $this->pictoUrl ? null : $this->getUploadDir() . '/picto/' . $this->pictoUrl;
    }

    protected function getUploadRootDir($basepath) {
        // the absolute directory path where uploaded documents should be saved
        return $basepath . $this->getUploadDir();
    }

    protected function getUploadDir() {
        // get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
        return 'uploads/images/picto';
    }

    public function upload($basepath) {
        // the file property can be empty if the field is not required
        if (null === $this->pictoFile) {
            return;
        }

        if (null === $basepath) {
            return;
        }

        // we use the original file name here but you should
        // sanitize it at least to avoid any security issues
        // move takes the target directory and then the target filename to move to
        $this->pictoFile->move($this->getUploadRootDir($basepath), $this->pictoFile->getClientOriginalName());

        // set the path property to the filename where you'ved saved the file
        $this->setPictoUrl($this->pictoFile->getClientOriginalName());

        // clean up the file property as you won't need it anymore
        $this->pictoFile = null;
    }

    /**
     * Set pictoFile.
     *
     * @param UploadedFile $pictoFile
     */
    public function setPictoFile(UploadedFile $pictoFile = null) {
        $this->pictoFile = $pictoFile;
    }

    /**
     * Get pictoFile.
     *
     * @return UploadedFile
     */
    public function getPictoFile() {
        return $this->pictoFile;
    }

    // ===================
    // =Saving files fin==
    // ===================

    public function __toString() {
        return $this->pictoUrl;
    }

    // ===================
    // =Getters & setters==
    // ===================

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set pictoUrl
     *
     * @param string $pictoUrl
     * @return Pictogramme
     */
    public function setPictoUrl($pictoUrl) {
        $this->pictoUrl = $pictoUrl;

        return $this;
    }

    /**
     * Get pictoUrl
     *
     * @return string 
     */
    public function getPictoUrl() {
        return $this->pictoUrl;
    }

    /**
     * Set imgUrl
     *
     * @param string $imgUrl
     * @return Pictogramme
     */
    public function setImgUrl($imgUrl) {
        $this->imgUrl = $imgUrl;

        return $this;
    }

    /**
     * Get imgUrl
     *
     * @return string 
     */
    public function getImgUrl() {
        return $this->imgUrl;
    }

    /**
     * Set refMot
     *
     * @param \TerrePlurielle\Bundle\FoBundle\Entity\Mot $refMot
     * @return Pictogramme
     */
    public function setRefMot(\TerrePlurielle\Bundle\FoBundle\Entity\Mot $refMot = null) {
        $this->refMot = $refMot;

        return $this;
    }

    /**
     * Get refMot
     *
     * @return \TerrePlurielle\Bundle\FoBundle\Entity\Mot 
     */
    public function getRefMot() {
        return $this->refMot;
    }

    /**
     * Set refPhrase
     *
     * @param \TerrePlurielle\Bundle\FoBundle\Entity\PhraseProposee $refPhrase
     * @return Pictogramme
     */
    public function setRefPhrase(\TerrePlurielle\Bundle\FoBundle\Entity\PhraseProposee $refPhrase = null) {
        $this->refPhrase = $refPhrase;

        return $this;
    }

    /**
     * Get refPhrase
     *
     * @return \TerrePlurielle\Bundle\FoBundle\Entity\PhraseProposee 
     */
    public function getRefPhrase() {
        return $this->refPhrase;
    }


//    /**
//     * Set refTheme
//     *
//     * @param \TerrePlurielle\Bundle\FoBundle\Entity\Theme $refTheme
//     * @return Pictogramme
//     */
//    public function setRefTheme(\TerrePlurielle\Bundle\FoBundle\Entity\Theme $refTheme = null)
//    {
//        $this->refTheme = $refTheme;
//
//        return $this;
//    }
//
//    /**
//     * Get refTheme
//     *
//     * @return \TerrePlurielle\Bundle\FoBundle\Entity\Theme 
//     */
//    public function getRefTheme()
//    {
//        return $this->refTheme;
//    }
}

