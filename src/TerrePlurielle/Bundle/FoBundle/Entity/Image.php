<?php

namespace TerrePlurielle\Bundle\FoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Image
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="TerrePlurielle\Bundle\FoBundle\Entity\ImageRepository")
 */
class Image {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Url", type="string", length=255)
     */
    private $url;

    /**
     * @var string
     * @ORM\ManyToOne(targetEntity="Enfant")
     * @ORM\JoinColumn(name="ref_enfant", referencedColumnName="id", nullable=true)
     */
    private $refEnfant;

    /**
     * @var string
     * @ORM\ManyToOne(targetEntity="Pictogramme")
     * @ORM\JoinColumn(name="ref_mot", referencedColumnName="id", nullable=true)
     */
    private $refPictogramme;

    /**
     *  @Assert\File( maxSize="4M")
     */
    protected $file;

    // ===================
    // == Saving files ===
    // ===================
    //    ========== File uploading ==========
    public function getAbsolutePathPicto() {
        return null === $this->url ? null : $this->getUploadRootDir() . '/img-perso/' . $this->url;
    }

    public function getWebPathPicto() {
        return null === $this->url ? null : $this->getUploadDir() . '/img-perso/' . $this->url;
    }

    protected function getUploadRootDir($basepath) {
        // the absolute directory path where uploaded documents should be saved
        return $basepath . $this->getUploadDir();
    }

    protected function getUploadDir() {
        // get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
        return 'uploads/images/img-perso';
    }

    public function upload($basepath) {
        // the file property can be empty if the field is not required
        if (null === $this->file) {
            return;
        }

        if (null === $basepath) {
            return;
        }

        // we use the original file name here but you should
        // sanitize it at least to avoid any security issues
        // move takes the target directory and then the target filename to move to
        $this->file->move($this->getUploadRootDir($basepath), $this->file->getClientOriginalName());

        // set the path property to the filename where you'ved saved the file
        $this->setUrl($this->file->getClientOriginalName());

        // clean up the file property as you won't need it anymore
        $this->file = null;
    }

    /**
     * Set file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null) {
        $this->file = $file;
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile() {
        return $this->file;
    }

    // ===================
    // =Saving files fin==
    // ===================


    public function __toString() {
        return $this->url;
    }



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return Image
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set refEnfant
     *
     * @param \TerrePlurielle\Bundle\FoBundle\Entity\Enfant $refEnfant
     * @return Image
     */
    public function setRefEnfant(\TerrePlurielle\Bundle\FoBundle\Entity\Enfant $refEnfant = null)
    {
        $this->refEnfant = $refEnfant;

        return $this;
    }

    /**
     * Get refEnfant
     *
     * @return \TerrePlurielle\Bundle\FoBundle\Entity\Enfant 
     */
    public function getRefEnfant()
    {
        return $this->refEnfant;
    }

    /**
     * Set refPictogramme
     *
     * @param \TerrePlurielle\Bundle\FoBundle\Entity\Pictogramme $refPictogramme
     * @return Image
     */
    public function setRefPictogramme(\TerrePlurielle\Bundle\FoBundle\Entity\Pictogramme $refPictogramme = null)
    {
        $this->refPictogramme = $refPictogramme;

        return $this;
    }

    /**
     * Get refPictogramme
     *
     * @return \TerrePlurielle\Bundle\FoBundle\Entity\Pictogramme 
     */
    public function getRefPictogramme()
    {
        return $this->refPictogramme;
    }
}
