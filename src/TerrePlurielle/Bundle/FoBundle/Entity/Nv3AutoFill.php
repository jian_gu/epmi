<?php

namespace TerrePlurielle\Bundle\FoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AutoFillNv3
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="TerrePlurielle\Bundle\FoBundle\Entity\Nv3AutoFillRepository")
 */
class Nv3AutoFill
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Mot")
     * @ORM\JoinColumn(name="ref_verbe", referencedColumnName="id")
     */
    private $refVerbe;

    /**
     *
     * @var string 
     * @ORM\Column(name="AutoFill", type="string", length=255)
     */
    private $autoFill;
    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Mot")
     * @ORM\JoinColumn(name="ref_complement", referencedColumnName="id")
     */
    private $refComplement;

    public function __toString()
    {
        return $this->autoFill;
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set autoFill
     *
     * @param string $autoFill
     * @return AutoFillNv3
     */
    public function setAutoFill($autoFill)
    {
        $this->autoFill = $autoFill;

        return $this;
    }

    /**
     * Get autoFill
     *
     * @return string 
     */
    public function getAutoFill()
    {
        return $this->autoFill;
    }

    /**
     * Set refVerbe
     *
     * @param \TerrePlurielle\Bundle\FoBundle\Entity\Mot $refVerbe
     * @return AutoFillNv3
     */
    public function setRefVerbe(\TerrePlurielle\Bundle\FoBundle\Entity\Mot $refVerbe = null)
    {
        $this->refVerbe = $refVerbe;

        return $this;
    }

    /**
     * Get refVerbe
     *
     * @return \TerrePlurielle\Bundle\FoBundle\Entity\Mot 
     */
    public function getRefVerbe()
    {
        return $this->refVerbe;
    }

    /**
     * Set refComplement
     *
     * @param \TerrePlurielle\Bundle\FoBundle\Entity\Mot $refComplement
     * @return AutoFillNv3
     */
    public function setRefComplement(\TerrePlurielle\Bundle\FoBundle\Entity\Mot $refComplement = null)
    {
        $this->refComplement = $refComplement;

        return $this;
    }

    /**
     * Get refComplement
     *
     * @return \TerrePlurielle\Bundle\FoBundle\Entity\Mot 
     */
    public function getRefComplement()
    {
        return $this->refComplement;
    }
}
