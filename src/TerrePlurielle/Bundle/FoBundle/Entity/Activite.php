<?php

namespace TerrePlurielle\Bundle\FoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Activite
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="TerrePlurielle\Bundle\FoBundle\Entity\ActiviteRepository")
 */
class Activite
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @var string
     *
     * @ORM\Column(name="Titre", type="string", length=255)
     */
    private $titre;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DateDebut", type="datetime")
     */
    private $dateDebut;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DateFin", type="datetime")
     */
    private $dateFin;

    /**
     * @var boolean
     *
     * @ORM\Column(name="Statut", type="boolean")
     */
    private $statut;

    /**
     * @ORM\ManyToOne(targetEntity="Pictogramme")
     * @ORM\JoinColumn(name="RefPictogramme", referencedColumnName="id")
     */
    private $refPictogramme;

    /**
     *
     * @ORM\ManyToMany(targetEntity="Enfant",cascade={"persist"})
     * @ORM\JoinTable(name="Activite_Enfant",
     *      joinColumns={@ORM\JoinColumn(name="activite_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="enfant_id", referencedColumnName="id")}
     *      )
     */
    private $refEnfant;

    /////////////////
    ///getter & setter///
    /////////////////
    
   
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->refEnfant = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre
     *
     * @param string $titre
     * @return Activite
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string 
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set dateDebut
     *
     * @param \DateTime $dateDebut
     * @return Activite
     */
    public function setDateDebut($dateDebut)
    {
        $this->dateDebut = $dateDebut;

        return $this;
    }

    /**
     * Get dateDebut
     *
     * @return \DateTime 
     */
    public function getDateDebut()
    {
        return $this->dateDebut;
    }

    /**
     * Set dateFin
     *
     * @param \DateTime $dateFin
     * @return Activite
     */
    public function setDateFin($dateFin)
    {
        $this->dateFin = $dateFin;

        return $this;
    }

    /**
     * Get dateFin
     *
     * @return \DateTime 
     */
    public function getDateFin()
    {
        return $this->dateFin;
    }

    /**
     * Set statut
     *
     * @param boolean $statut
     * @return Activite
     */
    public function setStatut($statut)
    {
        $this->statut = $statut;

        return $this;
    }

    /**
     * Get statut
     *
     * @return boolean 
     */
    public function getStatut()
    {
        return $this->statut;
    }

    /**
     * Set refPictogramme
     *
     * @param \TerrePlurielle\Bundle\FoBundle\Entity\Pictogramme $refPictogramme
     * @return Activite
     */
    public function setRefPictogramme(\TerrePlurielle\Bundle\FoBundle\Entity\Pictogramme $refPictogramme = null)
    {
        $this->refPictogramme = $refPictogramme;

        return $this;
    }

    /**
     * Get refPictogramme
     *
     * @return \TerrePlurielle\Bundle\FoBundle\Entity\Pictogramme 
     */
    public function getRefPictogramme()
    {
        return $this->refPictogramme;
    }

    /**
     * Add refEnfant
     *
     * @param \TerrePlurielle\Bundle\FoBundle\Entity\Enfant $refEnfant
     * @return Activite
     */
    public function addRefEnfant(\TerrePlurielle\Bundle\FoBundle\Entity\Enfant $refEnfant)
    {
        $this->refEnfant[] = $refEnfant;

        return $this;
    }

    /**
     * Remove refEnfant
     *
     * @param \TerrePlurielle\Bundle\FoBundle\Entity\Enfant $refEnfant
     */
    public function removeRefEnfant(\TerrePlurielle\Bundle\FoBundle\Entity\Enfant $refEnfant)
    {
        $this->refEnfant->removeElement($refEnfant);
    }

    /**
     * Get refEnfant
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRefEnfant()
    {
        return $this->refEnfant;
    }
}
