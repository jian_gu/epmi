<?php

namespace TerrePlurielle\Bundle\FoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EnfantExercices
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="TerrePlurielle\Bundle\FoBundle\Entity\EnfantExercicesRepository")
 */
class EnfantExercices {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="Enfant")
     * @ORM\JoinColumn(name="ref_enfant", referencedColumnName="id")
     */
    private $refEnfant;

    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="Exercices")
     * @ORM\JoinColumn(name="ref_exercices", referencedColumnName="id")
     */
    private $refExercice;

    /**
     * @var integer
     * 
     * @ORM\Column(name="evaluation", type="integer")
     */
    private $evaluation;

    /**
     * @var string
     *
     * @ORM\Column(name="observation", type="text")
     */
    private $observation;

    /**
     *
     * @var \DateTime
     *
     * @ORM\Column(name="DateSaved", type="datetime")
     */
    private $dateSaved;

    
    public function __construct() {
        $this->dateSaved = new \DateTime('now');
        $this->dateSaved->format('Y-m-d HH');;
    }
    
    //////////////////////
    /// getters & setters ///
    //////////////////////
    
  


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set evaluation
     *
     * @param integer $evaluation
     * @return EnfantExercices
     */
    public function setEvaluation($evaluation)
    {
        $this->evaluation = $evaluation;

        return $this;
    }

    /**
     * Get evaluation
     *
     * @return integer 
     */
    public function getEvaluation()
    {
        return $this->evaluation;
    }

    /**
     * Set observation
     *
     * @param string $observation
     * @return EnfantExercices
     */
    public function setObservation($observation)
    {
        $this->observation = $observation;

        return $this;
    }

    /**
     * Get observation
     *
     * @return string 
     */
    public function getObservation()
    {
        return $this->observation;
    }

    /**
     * Set dateSaved
     *
     * @param \DateTime $dateSaved
     * @return EnfantExercices
     */
    public function setDateSaved($dateSaved)
    {
        $this->dateSaved = $dateSaved;

        return $this;
    }

    /**
     * Get dateSaved
     *
     * @return \DateTime 
     */
    public function getDateSaved()
    {
        return $this->dateSaved;
    }

    /**
     * Set refEnfant
     *
     * @param \TerrePlurielle\Bundle\FoBundle\Entity\Enfant $refEnfant
     * @return EnfantExercices
     */
    public function setRefEnfant(\TerrePlurielle\Bundle\FoBundle\Entity\Enfant $refEnfant = null)
    {
        $this->refEnfant = $refEnfant;

        return $this;
    }

    /**
     * Get refEnfant
     *
     * @return \TerrePlurielle\Bundle\FoBundle\Entity\Enfant 
     */
    public function getRefEnfant()
    {
        return $this->refEnfant;
    }

    /**
     * Set refExercice
     *
     * @param \TerrePlurielle\Bundle\FoBundle\Entity\Exercices $refExercice
     * @return EnfantExercices
     */
    public function setRefExercice(\TerrePlurielle\Bundle\FoBundle\Entity\Exercices $refExercice = null)
    {
        $this->refExercice = $refExercice;

        return $this;
    }

    /**
     * Get refExercice
     *
     * @return \TerrePlurielle\Bundle\FoBundle\Entity\Exercices 
     */
    public function getRefExercice()
    {
        return $this->refExercice;
    }
}
