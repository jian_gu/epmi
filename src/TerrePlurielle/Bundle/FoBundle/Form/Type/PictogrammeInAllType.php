<?php

namespace TerrePlurielle\Bundle\FoBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use TerrePlurielle\Bundle\FoBundle\Form\Type\MotType;
use TerrePlurielle\Bundle\FoBundle\Form\Type\PhraseProposeeType;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;

class PictogrammeInAllType extends AbstractType {
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('pictoFile')
                ->add('pictoUrl', null, array('required' => TRUE,
                    'attr' => array('readonly' => 'readonly')))
                ->add('refMot', new MotType())
                ->add('refPhrase', new PhraseProposeeType())
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'TerrePlurielle\Bundle\FoBundle\Entity\Pictogramme'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'terreplurielle_bundle_fobundle_pictogramme';
    }

}

