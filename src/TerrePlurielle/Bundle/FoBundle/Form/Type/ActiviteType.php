<?php

namespace TerrePlurielle\Bundle\FoBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class ActiviteType extends AbstractType {

    private $childId;

    public function __construct($childId) {
        $this->childId = $childId;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $enfantId = $this->childId;

        $builder
                ->add('titre', 'text', array('label' => 'Titre'))
                ->add('dateDebut', 'datetime', array('input' => 'datetime', 'date_format' => 'ddMyyyy', 'date_widget' => 'choice', 'time_widget' => 'choice', 'years' => range(Date('Y'), Date('Y') + 2)))
                ->add('dateFin', 'datetime', array('input' => 'datetime', 'date_format' => 'ddMyyyy', 'date_widget' => 'choice', 'time_widget' => 'choice', 'years' => range(Date('Y'), Date('Y') + 2)))
                ->add('statut', 'checkbox', array('label' => 'Activité fini ?', 'required' => false,))
                ->add('refPictogramme', 'entity', array('label' => 'Pictogramme référent', 'property' => 'refMot.titre',
                    'class' => 'TerrePlurielle\Bundle\FoBundle\Entity\Pictogramme',
                    'query_builder' => function(EntityRepository $er) {
                return $er->createQueryBuilder('p')
                        ->join('p.refMot', 'm')
                        ->orderBy('m.titre', 'ASC');
            },
                ))
                ->add('refEnfant', 'entity', array('label' => 'Pour Enfant', 'property' => 'nameDOB',
                    'class' => 'TerrePlurielle\Bundle\FoBundle\Entity\Enfant',
                    'query_builder' => function(EntityRepository $er) use ($enfantId) {
                return $er->createQueryBuilder('e')
                        ->where('e.id = :id')
                        ->setParameter('id', $enfantId);
            },
                    'expanded' => FALSE, 'multiple' => TRUE, 'required' => TRUE,
                    'attr' => array('class' => 'option-selected'),
                ))

        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'TerrePlurielle\Bundle\FoBundle\Entity\Activite'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'terreplurielle_bundle_fobundle_activite';
    }

}
