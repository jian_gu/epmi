<?php

namespace TerrePlurielle\Bundle\FoBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class ImageType extends AbstractType {

    private $childId;

    public function __construct($childId) {
        $this->childId = $childId;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $enfantId = $this->childId;

        $builder
                ->add('refEnfant', 'entity', array(
                    'label' => 'Enfant',
                    'class' => 'TerrePlurielle\Bundle\FoBundle\Entity\Enfant',
                    'query_builder' => function(EntityRepository $er) use ($enfantId) {
                return $er->createQueryBuilder('e')
                        ->where('e.id = :id')
                        ->setParameter('id', $enfantId);
            },
                    'attr' => array('readonly' => 'readonly'),
                ))
                ->add('refPictogramme', 'entity', array('label' => 'Pour Mot', 'property' => 'refMot.titre', 'class' => 'TerrePlurielle\Bundle\FoBundle\Entity\Pictogramme'))
                ->add('file', 'file', array('label' => 'Fichier de l\'image', 'required' => FALSE))
                ->add('url', null, array('label' => 'Nom de l\'image', 'required' => TRUE,
                    'attr' => array('readonly' => 'readonly')
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'TerrePlurielle\Bundle\FoBundle\Entity\Image'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'terreplurielle_bundle_fobundle_image';
    }

}
