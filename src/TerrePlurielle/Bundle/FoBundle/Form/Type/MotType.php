<?php

namespace TerrePlurielle\Bundle\FoBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class MotType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                
                
                ->add('titre', 'text', array('label' => 'Titre du mot'))
                
                ->add('refTheme', 'entity', array('label' => 'Thème', 'class' => 'TerrePlurielle\Bundle\FoBundle\Entity\Theme'))

                ->add('niveau', 'entity', array('label' => 'Niveau', 'class' => 'TerrePlurielle\Bundle\FoBundle\Entity\Niveau'))
                
                
                ->add('personne', 'choice', array('choices' => array('1' => '\'je\'',
                        '2' => '\'tu\'',
                        '3' => '\'il / elle\'',
                        '4' => '\'nous\'',
                        '5' => '\'vous\'',
                        '6' => '\'ils / elles\''),
                    'required' => FALSE))
                
                
                ->add('pps', 'text', array('label' => '\'je\'', 'required' => false))
                ->add('dps', 'text', array('label' => '\'tu\'', 'required' => false))
                ->add('tps', 'text', array('label' => '\'il / elle\'', 'required' => false))
                ->add('ppp', 'text', array('label' => '\'nous\'', 'required' => false))
                ->add('dpp', 'text', array('label' => '\'vous\'', 'required' => false))
                ->add('tpp', 'text', array('label' => '\'ils / elles\'', 'required' => false))
                ->add('pc', 'text', array('label' => 'passé composé', 'required' => false))
                
                
                ->add('genre', 'choice', array('choices' => array('1' => 'Masculin singulier',
                        '2' => 'Féminiin singulier',
                        '3' => 'Masculin pluriel',
                        '4' => 'Féminin plurielle'),
                    'required' => false))
                ->add('plur', 'text', array('label' => 'format pluriel', 'required' => false))
                
                ->add('ms', 'text', array('label' => 'Masculin singulier', 'required' => false))
                ->add('mp', 'text', array('label' => 'Masculin pluriel', 'required' => false))
                ->add('fs', 'text', array('label' => 'Féminin singulier', 'required' => false))
                ->add('fp', 'text', array('label' => 'Féminin pluriel', 'required' => false))
                
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'TerrePlurielle\Bundle\FoBundle\Entity\Mot'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'terreplurielle_bundle_fobundle_mot';
    }

}

