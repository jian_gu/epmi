<?php

namespace TerrePlurielle\Bundle\FoBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PictogrammeType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('pictoUrl')
//            ->add('refMot')
//            ->add('refPhrase')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'TerrePlurielle\Bundle\FoBundle\Entity\Pictogramme'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'terreplurielle_bundle_fobundle_pictogramme';
    }
}

