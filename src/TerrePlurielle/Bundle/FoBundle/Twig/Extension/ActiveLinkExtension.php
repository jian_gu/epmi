<?php

namespace TerrePlurielle\Bundle\FoBundle\Twig\Extension;
 
class ActiveLinkExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            'active_link' => new \Twig_Filter_Method($this, 'activeLinkFilter'),
        );
    }

    public function activeLinkFilter($link_ref, $link_test)
    {

        $r = ($link_ref == $link_test) ? "active" : "" ;

        return $r;
    }

    public function getName()
    {
        return 'active_link_extension';
    }
}