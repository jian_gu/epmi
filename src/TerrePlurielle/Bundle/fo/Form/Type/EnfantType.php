<?php

namespace TerrePlurielle\Bundle\FoBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EnfantType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder
                ->add('nom', 'text')
                ->add('prenom', 'text')
                ->add('dateDeNaissance', 'date', array('label' => 'Date de Naissance', 'input' => 'datetime', 'format' => 'ddMyyyy', 'widget' => 'choice',
                    'empty_value' => NULL,
                    'widget' => 'choice',
                    'format' => 'dd MM yyyy',
                    'years' => range(Date('Y'), Date('Y') - 20),
					'required' => true)
                )
                ->add('observation', 'text', array('required' => false))
                ->add('refNiveau', 'entity', array('label' => 'Niveau référent', 'empty_value' => NULL,
                    'class' => 'TerrePlurielle\Bundle\FoBundle\Entity\Niveau', 'required' => true))
                ->add('file', 'file', array('label' => 'Avatar', 'required' => false))
//                ->add('avatar', 'entity', array('label' => 'Pictogramme référent', 'property' => 'refMot.titre', 'class' => 'TerrePlurielle\Bundle\FoBundle\Entity\Pictogramme', 'required' => false))
                ->add('refParent', 'entity', array('label' => 'Parent référent', 'empty_value' => NULL,
                    'class' => 'TerrePlurielle\Bundle\BoBundle\Entity\User',
                    'required' => false,
                    'query_builder' => function(\Doctrine\ORM\EntityRepository $er) {
                return $er->createQueryBuilder('p')->where('p.roles LIKE \'%ROLE_PARENT%\'');
            }
                ))
                ->add('refEducateur', 'entity', array('label' => 'Educateur référent', 'empty_value' => NULL,
                    'class' => 'TerrePlurielle\Bundle\BoBundle\Entity\User',
                    'required' => false,
                    'query_builder' => function(\Doctrine\ORM\EntityRepository $er) {
                return $er->createQueryBuilder('p')->where('p.roles LIKE \'%ROLE_EDUCATEUR%\'');
            }
                ))

        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'TerrePlurielle\Bundle\FoBundle\Entity\Enfant'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'terreplurielle_bundle_fobundle_activite';
    }

    public function prePersist($enfant) {
        $this->saveFile($enfant);
    }

    public function preUpdate($enfant) {
        $this->saveFile($enfant);
    }

    public function saveFile($enfant) {
        $basepath = $this->getRequest()->getBasePath();
        $enfant->upload($basepath);
    }

}
