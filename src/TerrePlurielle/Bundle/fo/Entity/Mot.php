<?php

namespace TerrePlurielle\Bundle\FoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Mot
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="TerrePlurielle\Bundle\FoBundle\Entity\MotRepository")
 */
class Mot
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Titre", type="string", length=255)
     */
    private $titre;

    /**
     * @var string
     *
     * @ORM\Column(name="titre_phrase", type="string", length=255, nullable=true)
     */
    private $titrePhrase;

    /**
     * @var string
     *
     * @ORM\Column(name="Categorie", type="string", length=255, nullable=true)
     */
    private $categorie;

    /**
     * @var string
     * @ORM\ManyToOne(targetEntity="Theme")
     * @ORM\JoinColumn(name="ref_theme", referencedColumnName="id")
     */
    private $refTheme;

    /**
     * @var integer
     * @ORM\ManyToOne(targetEntity="Niveau")
     * @ORM\JoinColumn(name="ref_niveau", referencedColumnName="id")
     */
    private $niveau;

    /**
     * @var integer
     *
     * @ORM\Column(name="personne", type="integer", nullable=true)
     */
    private $personne;

    /**
     * @var integer
     *
     * @ORM\Column(name="genre", type="integer", nullable=true)
     */
    private $genre;

    /**
     * masculin ou féminin
     * @var string
     *
     * @ORM\Column(name="pps", type="string", nullable=true)
     */
    private $pps;

    /**
     * deuxière personne du singulier
     * @var string
     *
     * @ORM\Column(name="dps", type="string", nullable=true)
     */
    private $dps;

    /**
     * troisième personne du singulier
     * @var string
     *
     * @ORM\Column(name="tps", type="string", nullable=true)
     */
    private $tps;

    /**
     * première personne du pluriel
     * @var string
     *
     * @ORM\Column(name="ppp", type="string", nullable=true)
     */
    private $ppp;

    /**
     * deuxième personne du pluriel
     * @var string
     *
     * @ORM\Column(name="dpp", type="string", nullable=true)
     */
    private $dpp;

    /**
     * troisième personne du pluriel
     * @var string
     *
     * @ORM\Column(name="tpp", type="string", nullable=true)
     */
    private $tpp;

    /**
     * Pour adjectif: masculin singulier
     * @var string
     *
     * @ORM\Column(name="ms", type="string", nullable=true)
     */
    private $ms;

    /**
     * Pour adjectif: masculin pluriel
     * @var string
     *
     * @ORM\Column(name="mp", type="string", nullable=true)
     */
    private $mp;

    /**
     * Pour adjectif: féminin singulier
     * @var string
     *
     * @ORM\Column(name="fs", type="string", nullable=true)
     */
    private $fs;

    /**
     * Pour adjectif: féminin pluriel
     * @var string
     *
     * @ORM\Column(name="fp", type="string", nullable=true)
     */
    private $fp;

    public function __toString() {
        return $this->titre;
    }
    
    ///////////////////////////////
    ///***Getters & setters***/////
    ///////////////////////////////

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre
     *
     * @param string $titre
     * @return Mot
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string 
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set titrePhrase
     *
     * @param string $titrePhrase
     * @return Mot
     */
    public function setTitrePhrase($titrePhrase)
    {
        $this->titrePhrase = $titrePhrase;

        return $this;
    }

    /**
     * Get titrePhrase
     *
     * @return string 
     */
    public function getTitrePhrase()
    {
        return $this->titrePhrase;
    }

    /**
     * Set categorie
     *
     * @param string $categorie
     * @return Mot
     */
    public function setCategorie($categorie)
    {
        $this->categorie = $categorie;

        return $this;
    }

    /**
     * Get categorie
     *
     * @return string 
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    /**
     * Set personne
     *
     * @param integer $personne
     * @return Mot
     */
    public function setPersonne($personne)
    {
        $this->personne = $personne;

        return $this;
    }

    /**
     * Get personne
     *
     * @return integer 
     */
    public function getPersonne()
    {
        return $this->personne;
    }

    /**
     * Set genre
     *
     * @param integer $genre
     * @return Mot
     */
    public function setGenre($genre)
    {
        $this->genre = $genre;

        return $this;
    }

    /**
     * Get genre
     *
     * @return integer 
     */
    public function getGenre()
    {
        return $this->genre;
    }

    /**
     * Set pps
     *
     * @param string $pps
     * @return Mot
     */
    public function setPps($pps)
    {
        $this->pps = $pps;

        return $this;
    }

    /**
     * Get pps
     *
     * @return string 
     */
    public function getPps()
    {
        return $this->pps;
    }

    /**
     * Set dps
     *
     * @param string $dps
     * @return Mot
     */
    public function setDps($dps)
    {
        $this->dps = $dps;

        return $this;
    }

    /**
     * Get dps
     *
     * @return string 
     */
    public function getDps()
    {
        return $this->dps;
    }

    /**
     * Set tps
     *
     * @param string $tps
     * @return Mot
     */
    public function setTps($tps)
    {
        $this->tps = $tps;

        return $this;
    }

    /**
     * Get tps
     *
     * @return string 
     */
    public function getTps()
    {
        return $this->tps;
    }

    /**
     * Set ppp
     *
     * @param string $ppp
     * @return Mot
     */
    public function setPpp($ppp)
    {
        $this->ppp = $ppp;

        return $this;
    }

    /**
     * Get ppp
     *
     * @return string 
     */
    public function getPpp()
    {
        return $this->ppp;
    }

    /**
     * Set dpp
     *
     * @param string $dpp
     * @return Mot
     */
    public function setDpp($dpp)
    {
        $this->dpp = $dpp;

        return $this;
    }

    /**
     * Get dpp
     *
     * @return string 
     */
    public function getDpp()
    {
        return $this->dpp;
    }

    /**
     * Set tpp
     *
     * @param string $tpp
     * @return Mot
     */
    public function setTpp($tpp)
    {
        $this->tpp = $tpp;

        return $this;
    }

    /**
     * Get tpp
     *
     * @return string 
     */
    public function getTpp()
    {
        return $this->tpp;
    }

    /**
     * Set ms
     *
     * @param string $ms
     * @return Mot
     */
    public function setMs($ms)
    {
        $this->ms = $ms;

        return $this;
    }

    /**
     * Get ms
     *
     * @return string 
     */
    public function getMs()
    {
        return $this->ms;
    }

    /**
     * Set mp
     *
     * @param string $mp
     * @return Mot
     */
    public function setMp($mp)
    {
        $this->mp = $mp;

        return $this;
    }

    /**
     * Get mp
     *
     * @return string 
     */
    public function getMp()
    {
        return $this->mp;
    }

    /**
     * Set fs
     *
     * @param string $fs
     * @return Mot
     */
    public function setFs($fs)
    {
        $this->fs = $fs;

        return $this;
    }

    /**
     * Get fs
     *
     * @return string 
     */
    public function getFs()
    {
        return $this->fs;
    }

    /**
     * Set fp
     *
     * @param string $fp
     * @return Mot
     */
    public function setFp($fp)
    {
        $this->fp = $fp;

        return $this;
    }

    /**
     * Get fp
     *
     * @return string 
     */
    public function getFp()
    {
        return $this->fp;
    }

    /**
     * Set refTheme
     *
     * @param \TerrePlurielle\Bundle\FoBundle\Entity\Theme $refTheme
     * @return Mot
     */
    public function setRefTheme(\TerrePlurielle\Bundle\FoBundle\Entity\Theme $refTheme = null)
    {
        $this->refTheme = $refTheme;

        return $this;
    }

    /**
     * Get refTheme
     *
     * @return \TerrePlurielle\Bundle\FoBundle\Entity\Theme 
     */
    public function getRefTheme()
    {
        return $this->refTheme;
    }

    /**
     * Set niveau
     *
     * @param \TerrePlurielle\Bundle\FoBundle\Entity\Niveau $niveau
     * @return Mot
     */
    public function setNiveau(\TerrePlurielle\Bundle\FoBundle\Entity\Niveau $niveau = null)
    {
        $this->niveau = $niveau;

        return $this;
    }

    /**
     * Get niveau
     *
     * @return \TerrePlurielle\Bundle\FoBundle\Entity\Niveau 
     */
    public function getNiveau()
    {
        return $this->niveau;
    }
}
