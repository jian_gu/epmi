<?php

namespace TerrePlurielle\Bundle\FoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PhraseProposee
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="TerrePlurielle\Bundle\FoBundle\Entity\PhraseProposeeRepository")
 */
class PhraseProposee
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Contenu", type="string", length=255)
     */
    private $contenu;

//    /**
//     * @ORM\OneToOne(targetEntity="Pictogramme")
//     * @ORM\JoinColumn(name="ref_pictogramme", referencedColumnName="id")
//     */
//    private $refPictogramme;


    public function __toString()
    {
        return $this->contenu;
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set contenu
     *
     * @param string $contenu
     * @return PhraseProposee
     */
    public function setContenu($contenu)
    {
        $this->contenu = $contenu;

        return $this;
    }

    /**
     * Get contenu
     *
     * @return string 
     */
    public function getContenu()
    {
        return $this->contenu;
    }

//    /**
//     * Set refPictogramme
//     *
//     * @param string $refPictogramme
//     * @return PhraseProposee
//     */
//    public function setRefPictogramme($refPictogramme)
//    {
//        $this->refPictogramme = $refPictogramme;
//
//        return $this;
//    }
//
//    /**
//     * Get refPictogramme
//     *
//     * @return string 
//     */
//    public function getRefPictogramme()
//    {
//        return $this->refPictogramme;
//    }
}
