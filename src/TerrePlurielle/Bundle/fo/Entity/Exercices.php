<?php

namespace TerrePlurielle\Bundle\FoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Exercices
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="TerrePlurielle\Bundle\FoBundle\Entity\ExercicesRepository")
 */
class Exercices
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Objectif", type="text")
     */
    private $objectif;

    /**
     * @var string
     *
     * @ORM\Column(name="Question", type="string", length=255)
     */
    private $question;

    /**
     * @var string
     *
     * @ORM\Column(name="Reponse", type="string", length=255)
     */
    private $reponse;

    /**
     * @var string
     *
     * @ORM\Column(name="RefPictogramme", type="string", length=255)
     */
    private $refPictogramme;

    // *
    //  * @var string
    //  *
    //  * @ORM\Column(name="RefEnfant", type="string", length=255)
     
    // private $refEnfant;


    public function __toString()
    {
        return $this->question;
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set objectif
     *
     * @param string $objectif
     * @return Exercices
     */
    public function setObjectif($objectif)
    {
        $this->objectif = $objectif;

        return $this;
    }

    /**
     * Get objectif
     *
     * @return string 
     */
    public function getObjectif()
    {
        return $this->objectif;
    }

    
    // /**
    //  * Set refEnfant
    //  *
    //  * @param string $refEnfant
    //  * @return Image
    //  */
    // public function setRefEnfant($refEnfant)
    // {
    //     $this->refEnfant = $refEnfant;

    //     return $this;
    // }

    // /**
    //  * Get refEnfant
    //  *
    //  * @return string 
    //  */
    // public function getRefEnfant()
    // {
    //     return $this->refEnfant;
    // }

    /**
     * Set question
     *
     * @param string $question
     * @return Exercices
     */
    public function setQuestion($question)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question
     *
     * @return string 
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Set reponse
     *
     * @param string $reponse
     * @return Exercices
     */
    public function setReponse($reponse)
    {
        $this->reponse = $reponse;

        return $this;
    }

    /**
     * Get reponse
     *
     * @return string 
     */
    public function getReponse()
    {
        return $this->reponse;
    }

    /**
     * Set refPictogramme
     *
     * @param string $refPictogramme
     * @return Exercices
     */
    public function setRefPictogramme($refPictogramme)
    {
        $this->refPictogramme = $refPictogramme;

        return $this;
    }

    /**
     * Get refPictogramme
     *
     * @return string 
     */
    public function getRefPictogramme()
    {
        return $this->refPictogramme;
    }
}
