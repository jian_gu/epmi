<?php

namespace TerrePlurielle\Bundle\FoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PhraseComposee
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="TerrePlurielle\Bundle\FoBundle\Entity\PhraseComposeeRepository")
 */
class PhraseComposee
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Contenu", type="string", length=255)
     */
    private $contenu;

    /**
     * @ORM\ManyToOne(targetEntity="Enfant")
     * @ORM\JoinColumn(name="ref_enfant", referencedColumnName="id")
     */
    private $refEnfant;

    /**
     * @ORM\ManyToOne(targetEntity="Niveau")
     * @ORM\JoinColumn(name="ref_niveau", referencedColumnName="id")
     */
    private $refNiveau;

    /**
     *
     * @var \DateTime
     *
     * @ORM\Column(name="DateSaved", type="datetime")
     */
    private $dateSaved;

    public function __toString()
    {
        return $this->contenu;
    }

    public function __construct() {
        $this->dateSaved = new \DateTime('now');
        $this->dateSaved->format('Y-m-d HH');;
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set contenu
     *
     * @param string $contenu
     * @return PhraseComposee
     */
    public function setContenu($contenu)
    {
        $this->contenu = $contenu;

        return $this;
    }

    /**
     * Get contenu
     *
     * @return string 
     */
    public function getContenu()
    {
        return $this->contenu;
    }

    /**
     * Set dateSaved
     *
     * @param \DateTime $dateSaved
     * @return PhraseComposee
     */
    public function setDateSaved($dateSaved)
    {
        $this->dateSaved = $dateSaved;

        return $this;
    }

    /**
     * Get dateSaved
     *
     * @return \DateTime 
     */
    public function getDateSaved()
    {
        return $this->dateSaved;
    }

    /**
     * Set refEnfant
     *
     * @param \TerrePlurielle\Bundle\FoBundle\Entity\Enfant $refEnfant
     * @return PhraseComposee
     */
    public function setRefEnfant(\TerrePlurielle\Bundle\FoBundle\Entity\Enfant $refEnfant = null)
    {
        $this->refEnfant = $refEnfant;

        return $this;
    }

    /**
     * Get refEnfant
     *
     * @return \TerrePlurielle\Bundle\FoBundle\Entity\Enfant 
     */
    public function getRefEnfant()
    {
        return $this->refEnfant;
    }

    /**
     * Set refNiveau
     *
     * @param \TerrePlurielle\Bundle\FoBundle\Entity\Niveau $refNiveau
     * @return PhraseComposee
     */
    public function setRefNiveau(\TerrePlurielle\Bundle\FoBundle\Entity\Niveau $refNiveau = null)
    {
        $this->refNiveau = $refNiveau;

        return $this;
    }

    /**
     * Get refNiveau
     *
     * @return \TerrePlurielle\Bundle\FoBundle\Entity\Niveau 
     */
    public function getRefNiveau()
    {
        return $this->refNiveau;
    }
}
