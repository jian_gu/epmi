<?php

namespace TerrePlurielle\Bundle\FoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Doctrine\ORM\EntityRepository;
use TerrePlurielle\Bundle\FoBundle\Entity\Enfant;
use Symfony\Component\HttpFoundation\Request;

class PlanningController extends Controller {

    public function indexAction() {
        $session = $this->getRequest()->getSession();

        if (!$session->get('enfant')) {
            // we call function to let the user choose a child
            return $this->redirect($this->generateUrl('fo_homepage'));
        } else {
            $enfantId = $session->get('enfant')->getId();
            return $this->activitiesAction("week", null);
        }


        return $this->render('FoBundle:Planning:index.html.twig', array(
                    'form' => $form->createView(),
                        )
        );
    }

    /**
     * 	@Template()
     */
    public function activitiesAction($limite_planning, $interval_planning) {
        $activite_repo = $this->getDoctrine()->getRepository('FoBundle:Activite');
        $session = $this->getRequest()->getSession();
        $list_activites;

        // Récupération de chaine des jours séléctionnés dans l'url si elle existe et la conretie en tableau
        $selected_days_interval_ordered = $selected_days_interval = array();
        if ($interval_planning != null) {
            $selected_days_interval = ($interval_planning != "") ? explode('_', $interval_planning) : null;
            // On fait correspondre la valeur date avec le bon numéro de la semaine
            foreach ($selected_days_interval as $key => $value) {
                $selected_days_interval_ordered[date("w", strtotime($value))] = $value;
            }
        }

        $now = new \DateTime(date('Y-m-d'));
        $limite_planning_message = "";
        $rowActivities = $rowActivities_v2 = array();
        if ($session->get('enfant') && $session->get('enfant')->getId()) {
            $enfantId = $session->get('enfant')->getId();
            $list_activites = $activite_repo->getActivityByDay($enfantId);
        } else {
            $list_activites = $activite_repo->getActivityByDay(null);
        }

        // Parcours de toutes les activité
        foreach ($list_activites as $key => $activite) {

            $i_week = $activite->getDateDebut()->format('W');
            $i_month = $activite->getDateDebut()->format('n');
            $i_day = $activite->getDateDebut()->format('j');

            $date = $activite->getDateDebut()->format('Y-m-d');

            // Remplissage du tableau en fonction de l'intervalle (jur, week, mois) pour l'affichage
            switch ($limite_planning) {
                case 'jour':
                    $limite_planning_message = "de la journée";
//                    if ($i_day <= $now->format('j')) {
                    if ($i_week <= $now->format('W')) {
                        $rowActivities_v2[($activite->getDateDebut()->format('Y-n-j'))][$activite->getId()] = $activite;
                    }
                    break;
                case 'week':
                    $limite_planning_message = "de la semaine";
                    if ($i_week <= $now->format('W')) {
                        $rowActivities_v2[($activite->getDateDebut()->format('Y-n-j'))][$activite->getId()] = $activite;
                    }
                    break;
                case 'mois':
                    $limite_planning_message = "du mois";
                    if ($i_month <= $now->format('n')) {
                        $rowActivities_v2[($activite->getDateDebut()->format('Y-n-j'))][$activite->getId()] = $activite;
                    }
                    break;

                default:
                    $limite_planning_message = "";
                    break;
            }
        }


        // Si la limit du planning est définie,
        // Sinon on la défini à 1 semaine
        $timestamp = ($limite_planning == "jour") ? strtotime('now') : strtotime('next Monday');
        // On récupère le début et la fin de l'interval passé en param
        // On les convertie pour avoir le numero du jour dans la semaine : Mon -> 1, Sun -> 0
        // $start_i = date("w", strtotime($selected_days_interval));
        //On cré un tableau avec les jours de la semaine
        $_days_list = array();
        for ($i = 0; $i <= 7; $i++) {
            $_days_list[str_replace("0", "7", strftime('%w', $timestamp))] = strftime('%a', $timestamp);
            $_days_list_complet[str_replace("0", "7", strftime('%w', $timestamp))] = strftime('%a', $timestamp);
            $timestamp = strtotime('+1 day', $timestamp);
        }
        // On réorganise du tableau pour l'avoir dans l'ordre des jours
        ksort($_days_list);

        $_days_list2 = array();
        if ($interval_planning != null) {
            $_days_list2 = $selected_days_interval_ordered;
        }

        //getAllDate() return days of current month of current year
        $dates = $activite_repo->getAllDate($limite_planning, $selected_days_interval_ordered);

        return $this->render('FoBundle:Planning:activities.html.twig', array(
                    // "activites" => $rowActivities,
                    "activites_v2" => $rowActivities_v2,
                    'limite_planning_message' => $limite_planning_message,
                    'limite_planning' => $limite_planning,
                    'interval_planning' => $interval_planning,
                    'selected_days_interval' => $selected_days_interval,
                    'dates' => $dates,
                    'days_list' => $_days_list,
                    'days_list2' => $_days_list2,
                    'months_repo' => $activite_repo->months,
                        )
        );
    }

    public function addActiviteAction() {
        $enfantId = $this->get('session')->get('enfant')->getId();
        $activite = new \TerrePlurielle\Bundle\FoBundle\Entity\Activite();


        // On crée le FormBuilder grâce à la méthode du contrôleur
        $formAdd = $this->createFormBuilder($activite)

                // On ajoute les champs de l'entité que l'on veut à notre formulaire
                ->add('titre', 'text', array('label' => 'Titre'))->add('dateDebut', 'date', array('widget' => 'single_text', 'input' => 'datetime', 'format' => 'ddMyyyy', 'widget' => 'choice'))
                ->add('dateFin', 'date', array('widget' => 'single_text', 'input' => 'datetime', 'format' => 'ddMyyyy', 'widget' => 'choice'))
                ->add('statut', 'checkbox', array('label' => 'Activité fini ?', 'required' => false,))
                ->add('refPictogramme', 'entity', array('label' => 'Pictogramme référent', 'property' => 'refMot.titre', 'class' => 'TerrePlurielle\Bundle\FoBundle\Entity\Pictogramme'))
                ->add('refEnfant', 'entity', array('label' => 'Pour Enfant', 'property' => 'nameDOB',
            'class' => 'TerrePlurielle\Bundle\FoBundle\Entity\Enfant',
            'query_builder' => function(EntityRepository $er) use ($enfantId) {
        return $er->createQueryBuilder('e')
                ->where('e.id = :id')
                ->setParameter('id', $enfantId);
    },
            'expanded' => FALSE, 'multiple' => TRUE, 'required' => TRUE,
            'attr' => array('class' => 'option-selected'),
        ))
        ->add('submit', 'submit', array('label' => 'Créer'))
        // À partir du formBuilder, on génère le formulaire
        ->getForm();

        // On récupère la requête
        $request = $this->get('request');

        // On vérifie qu'elle est de type POST
        if ($request->getMethod() == 'POST') {
            // On fait le lien Requête <-> Formulaire
            // À partir de maintenant, la variable $entity contient les valeurs entrées dans le formulaire par le visiteur
            $formAdd->bind($request);

            // On vérifie que les valeurs entrées sont correctes
            // (Nous verrons la validation des objets en détail dans le prochain chapitre)
            if ($formAdd->isValid()) {
                // On l'enregistre notre objet $entity dans la base de données
                $em = $this->getDoctrine()->getManager();
                $em->persist($activite);
                $em->flush();
                // Flash message
                $this->get('session')->getFlashBag()->add(
                        'success', 'Activité ajoutée avec succès.'
                );
            }
        }

        return $this->render('FoBundle:Planning:addActivite.html.twig', array(
                    'formAdd' => $formAdd->createView(),
                        )
        );
    }

    /**
     * Get current logged user
     * @return type
     */
    public function userLogged() {
        return $this->get('security.context')->getToken()->getUser();
    }

}
