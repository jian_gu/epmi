<?php

namespace TerrePlurielle\Bundle\FoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class CommunicationController extends Controller {
    /*
     * Fonction rendre page
     */

    public function niveau1Action() {
        $picto_repo = $this->getDoctrine()->getRepository('FoBundle:Pictogramme');

//        $pictos = $picto_repo->findAll();
        $pictos = $picto_repo->getPictosByNiveau(1);

        return $this->render('FoBundle:Communication:niveau1.html.twig', array(
                    "pictos" => $pictos
                        )
        );
    }

    /*
     * Fonction rendre page
     */

    public function niveau2Action() {
        $theme_repo = $this->getDoctrine()->getRepository('FoBundle:Theme');
        $picto_repo = $this->getDoctrine()->getRepository('FoBundle:Pictogramme');

        //$themes = $theme_repo->findAll();
        $themes = $theme_repo->getThemesByNiveau(2);
        $pictos = $picto_repo->getPictosByNiveau(2);

        return $this->render('FoBundle:Communication:niveau2.html.twig', array(
                    "themes" => $themes,
                    "pictos" => $pictos
                        )
        );
    }

    /*
     * Fonction rendre page
     */

    public function niveau3Action() {
        $theme_repo = $this->getDoctrine()->getRepository('FoBundle:Theme');
        $picto_repo = $this->getDoctrine()->getRepository('FoBundle:Pictogramme');

        //$themes = $theme_repo->findAll();
        $themes = $theme_repo->getThemesByNiveau(3);
        $pictos = $picto_repo->getPictosByNiveau(3);

        return $this->render('FoBundle:Communication:niveau3.html.twig', array(
                    "themes" => $themes,
                    "pictos" => $pictos
                        )
        );
    }

    /*
     * Fonction recuperer les pictos d'un theme
     */
    function getPictosByThemeAction() {
        $themeId = $this->getRequest()->get('theme_id');
        $picto_repo = $this->getDoctrine()->getRepository('FoBundle:Pictogramme');
        $pictos = $picto_repo->getPictosByTheme($themeId);
        //$pictosJsonfy = json_encode($pictos);
        
        return $this->render('FoBundle:Communication:__pictosByTheme.html.twig', array(
                    "pictos" => $pictos
                        )
        );
    }

    /*
     * Fonction traiter la requete vers google tts
     */

    public function soundRequestAction() {
        $texts = $this->get('Request')->get('text_to_read');
        $qs = http_build_query(array("ie" => "utf-8", "tl" => "fr", "q" => $texts));
        $ctx = stream_context_create(array("http" => array("method" => "GET", "header" => "Referer: \r\n")));
        // fetch the sound file content
        $soundfile = file_get_contents("http://translate.google.com/translate_tts?" . $qs, false, $ctx);

        header("Content-type: audio/mpeg");
        header("Content-Transfer-Encoding: binary");
        header('Pragma: no-cache');
        header('Expires: 0');

        echo $soundfile;
	die();
    }

}
