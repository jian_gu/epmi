<?php

namespace TerrePlurielle\Bundle\FoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use TerrePlurielle\Bundle\FoBundle\Entity\Enfant;
use TerrePlurielle\Bundle\FoBundle\Entity\EnfantExercices;

class SocialisationController extends Controller {

    /**
     * 	@Template()
     */
    public function indexAction() {

        $usr = $this->userLogged();

        // On crée un objet Enfant
        $enfant = new Enfant();

//		$enfant->setRefEducateur($usr->getUsername());
        $enfant->setRefEducateur($usr);

        // On crée le FormBuilder grâce à la méthode du contrôleur
        $form = $this->createFormBuilder($enfant)

                // On ajoute les champs de l'entité que l'on veut à notre formulaire
                ->add('nom', 'text')
                ->add('prenom', 'text')
                ->add('dateDeNaissance', 'date', array(
                    'empty_value' => array('year' => 'Année', 'month' => 'Mois', 'day' => 'Jour'),
                    'widget' => 'choice',
                    'format' => 'dd MM yyyy',
                    'years' => range(Date('Y'), Date('Y') - 100),
                ))
                // À partir du formBuilder, on génère le formulaire
                ->getForm();

        // On récupère la requête
        $request = $this->get('request');

        // On vérifie qu'elle est de type POST
        if ($request->getMethod() == 'POST') {
            // On fait le lien Requête <-> Formulaire
            // À partir de maintenant, la variable $entity contient les valeurs entrées dans le formulaire par le visiteur
            $form->bind($request);

            // On vérifie que les valeurs entrées sont correctes
            // (Nous verrons la validation des objets en détail dans le prochain chapitre)
            if ($form->isValid()) {
                // On l'enregistre notre objet $entity dans la base de données
                $em = $this->getDoctrine()->getManager();
                $em->persist($enfant);
                $em->flush();
                // Flash message
                $this->get('session')->getFlashBag()->add(
                        'notice', 'Enfant ajouté avec succès.'
                );
            }
        }

        $enfant_repo = $this->getDoctrine()->getRepository('FoBundle:Enfant');
//		$list_educateur_enfant = $enfant_repo->findBy(array( 'refEducateur' => $usr->getUsername() ));
        $list_educateur_enfant = $enfant_repo->findBy(array('refEducateur' => $usr));

        return array(
            'form' => $form->createView(),
            'list_educateur_enfant' => $list_educateur_enfant,
        );
    }

    /**
     *
     * @Template
     */
    public function updateAction($enfant_id) {
        $enfant = new Enfant();
        $enfant = $this->getDoctrine()->getRepository('FoBundle:Enfant')->find($enfant_id);
        $niveaux = $this->getDoctrine()->getRepository('FoBundle:Niveau')->findAll();
        $parents = $this->getDoctrine()->getRepository('BoBundle:User')->findAll();
        $users = $this->getDoctrine()->getRepository('BoBundle:User')->findAll();

        // Start array to 1, not to 0
        array_unshift($niveaux, "");
        unset($niveaux[0]);

        // Séléect only user have role = parent
        foreach ($parents as $parent) {

            foreach ($parent->getRoles() as $role) {
                if ($role == "ROLE_PARENT") {

//                       $educateurs[ $user->getUsername() ] = $user;
                    $parents1[$parent->getUsername()] = $parent;
                }
            }
        }

        // Séléect only user have role = educateur
        foreach ($users as $user) {

            foreach ($user->getRoles() as $role) {
                if ($role == "ROLE_EDUCATEUR") {

//                       $educateurs[ $user->getUsername() ] = $user;
                    $educateurs[$user->getUsername()] = $user;
                }
            }
        }

        // On crée le FormBuilder grâce à la méthode du contrôleur
        $form = $this->createFormBuilder($enfant)

                // On ajoute les champs de l'entité que l'on veut à notre formulaire
                ->add('nom', 'text')
                ->add('prenom', 'text')
                ->add('dateDeNaissance', 'date', array(
                    'empty_value' => array('year' => 'Année', 'month' => 'Mois', 'day' => 'Jour'),
                    'widget' => 'choice',
                    'format' => 'dd MM yyyy',
                    'years' => range(Date('Y'), Date('Y') - 100),
                ))
                ->add('observation', 'textarea', array('required' => false))
                ->add('file', 'file', array('required' => false))
                ->add('refNiveau', 'entity', array('choices' => $niveaux, 'class' => 'FoBundle:Niveau'))
                ->add('refParent', 'entity', array('choices' => $parents1, 'class' => 'BoBundle:User'))
                ->add('refEducateur', 'entity', array('choices' => $educateurs, 'class' => 'BoBundle:User'))

                // À partir du formBuilder, on génère le formulaire
                ->getForm();

        // On récupère la requête
        $request = $this->get('request');

        // On vérifie qu'elle est de type POST
        if ($request->getMethod() == 'POST') {
            // On fait le lien Requête <-> Formulaire
            // À partir de maintenant, la variable $entity contient les valeurs entrées dans le formulaire par le visiteur
            $form->bind($request);

            // On vérifie que les valeurs entrées sont correctes
            // (Nous verrons la validation des objets en détail dans le prochain chapitre)
            if ($form->isValid()) {

                // On l'enregistre notre objet $entity dans la base de données
                $em = $this->getDoctrine()->getManager();
                $em->persist($enfant);
                $em->flush();

                // Flash message
                $this->get('session')->getFlashBag()->add(
                        'notice', 'Modification réussite.'
                );
                return $this->redirect($this->generateUrl("socialisation"));
            }
        }

        return array(
            'form' => $form->createView(),
            'enfant' => $enfant,
        );
    }

    /**
     * 	@Template()
     */
    public function sceanceAction($enfant_id, $exercice_id) {

        $enfant_repo = $this->getDoctrine()->getRepository('FoBundle:Enfant');

        $exercices_repo = $this->getDoctrine()->getRepository('FoBundle:Exercices');
        $exercices_do_repo = $this->getDoctrine()->getRepository('FoBundle:EnfantExercices');

        $enfant = $enfant_repo->findOneById($enfant_id);

        $exercice = null;
        if ($exercice_id != null) {
            $exercice = $exercices_repo->findOneById($exercice_id);
        }

        $list_exercices = $exercices_repo->findAll();
        $list_exercices_do = $exercices_do_repo->findBy(array('refEnfant' => $enfant_id));

        // Si l'exercice est fait on ajoute un champs is_done avec la valeur true sinon false 
        foreach ($list_exercices as $key => $value) {
            $value->is_done = false;
            foreach ($list_exercices_do as $key_do => $value_do) {
                if ($value->getId() == $value_do->getRefExercice()->getId()) {
                    $value->is_done = true;
                }
            }
        }

        if (!$enfant) {
            throw $this->createNotFoundException(
                    'Aucun enfant trouvé pour cet id : ' . $enfant_id
            );
        }

        // Avec l'in de l'enfant et l'id de l'exercice on récupère l'enregistrement qui correspond
        $enfant_exercices = $exercices_do_repo->findOneBy(array('refExercice' => $exercice_id, 'refEnfant' => $enfant_id));

        // Si on a pas d'enregistrement on init notre formulaire avec un nouvel objet 
        if (empty($enfant_exercices)) {
            $enfant_exercices = new EnfantExercices();
        }
//        $enfant_exercices->setRefExercice($exercice_id);
        $enfant_exercices->setRefExercice($exercice);
//        $enfant_exercices->setRefEnfant($enfant_id);
        $enfant_exercices->setRefEnfant($enfant);

        // On crée le FormBuilder grâce à la méthode du contrôleur
        $form = $this->createFormBuilder($enfant_exercices)

                // On ajoute les champs de l'entité que l'on veut à notre formulaire
                ->add('evaluation', 'choice', array('choices' =>
                    array(0 => 'AB', 1 => 'B', 2 => 'TB',),
                    'attr' => array('label' => ''),
                    'multiple' => false,
                    'expanded' => true,
                    'required' => false,
                ))
                ->add('observation')

                // À partir du formBuilder, on génère le formulaire
                ->getForm();

        // On récupère la requête
        $request = $this->get('request');

        // On vérifie qu'elle est de type POST
        if ($request->getMethod() == 'POST') {
            // On fait le lien Requête <-> Formulaire
            // À partir de maintenant, la variable $Enfant_Exercices contient les valeurs entrées dans le formulaire par le visiteur
            $form->bind($request);

            // On vérifie que les valeurs entrées sont correctes
            // (Nous verrons la validation des objets en détail dans le prochain chapitre)
            if ($form->isValid()) {
                // On l'enregistre notre objet $Enfant_Exercices dans la base de données
                $em = $this->getDoctrine()->getManager();
                $em->persist($enfant_exercices);
                $em->flush();

                return $this->redirect($this->generateUrl("socialisation_sceance", array("enfant_id" => $enfant_id, 'exercice_id' => null)));
            }
        }

        return array(
            'form' => $form->createView(),
            'enfant_id' => $enfant_id,
            'exercice_id' => $exercice_id,
            'enfant' => $enfant,
            'exercice' => $exercice,
            'list_exercices' => $list_exercices,
        );
    }

    public function userLogged() {
        return $this->get('security.context')->getToken()->getUser();
    }

}
