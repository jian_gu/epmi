<?php

namespace TerrePlurielle\Bundle\FoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use TerrePlurielle\Bundle\FoBundle\Entity\Activite;
use TerrePlurielle\Bundle\FoBundle\Form\Type\ActiviteType;
use Doctrine\ORM\EntityRepository;

/**
 * Activite controller.
 *
 */
class ActiviteController extends Controller {

    /**
     * Lists all Activite entities.
     *
     */
    public function indexAction() {
        $session = $this->getRequest()->getSession();

        $em = $this->getDoctrine()->getManager();
        $currentEnfant = $session->get('enfant');

//        $entities = $em->getRepository('FoBundle:Activite')->findAll();
        $entities = $em->getRepository('FoBundle:Activite')->getActivityByChildId($session->get('enfant')->getId());

        return $this->render('FoBundle:Activite:index.html.twig', array(
                    'entities' => $entities,
        ));
    }

    /**
     * Creates a new Activite entity.
     *
     */
    public function createAction(Request $request) {
        $entity = new Activite();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {


            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            // Flash message
            $this->get('session')->getFlashBag()->add(
                    'success', 'Activité ajoutée.'
            );
            return $this->redirect($this->generateUrl('activite_show', array('id' => $entity->getId())));
        }

        return $this->render('FoBundle:Activite:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Activite entity.
     *
     * @param Activite $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Activite $entity) {
        $form = $this->createForm(new ActiviteType($this->getRequest()->getSession()->get('enfant')->getId()), $entity, array(
            'action' => $this->generateUrl('activite_create'),
            'method' => 'POST',
        ));


        $form->add('submit', 'submit', array('label' => 'Créer'));
        
        return $form;
    }

    /**
     * Displays a form to create a new Activite entity.
     *
     */
    public function newAction() {
        $entity = new Activite();
        $form = $this->createCreateForm($entity);

        return $this->render('FoBundle:Activite:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Activite entity.
     *
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('FoBundle:Activite')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Activite entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('FoBundle:Activite:show.html.twig', array(
                    'entity' => $entity,
                    'delete_form' => $deleteForm->createView(),));
    }

    /**
     * Displays a form to edit an existing Activite entity.
     *
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('FoBundle:Activite')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Activite entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('FoBundle:Activite:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a Activite entity.
     *
     * @param Activite $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Activite $entity) {
        $form = $this->createForm(new ActiviteType($this->getRequest()->getSession()->get('enfant')->getId()), $entity, array(
            'action' => $this->generateUrl('activite_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));


        
        $form->add('submit', 'submit', array('label' => 'Mettre à jour'));
        
        return $form;
    }

    /**
     * Edits an existing Activite entity.
     *
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('FoBundle:Activite')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Activite entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {

            $em->flush();

            // Flash message
            $this->get('session')->getFlashBag()->add(
                    'success', 'Activité mise à jour.'
            );
            return $this->redirect($this->generateUrl('activite_edit', array('id' => $id)));
        }

        return $this->render('FoBundle:Activite:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Activite entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('FoBundle:Activite')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Activite entity.');
            }


            $em->remove($entity);
            $em->flush();
        }

        // Flash message
        $this->get('session')->getFlashBag()->add(
                'success', 'Activité supprimée.'
        );
        return $this->redirect($this->generateUrl('activite'));
    }

    /**
     * Creates a form to delete a Activite entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('activite_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Supprimer'))
                        ->getForm()
        ;
    }

    /**
     * get logged user
     * @return null
     */
    private function getLoggedUser() {
        $token = $this->get('security.context')->getToken();
        if ($token->getUser()) {
            return $token->getUser();
        } else {
            return NULL;
        }
    }

}
