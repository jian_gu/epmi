<?php

namespace TerrePlurielle\Bundle\FoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use TerrePlurielle\Bundle\FoBundle\Entity\Enfant;
use TerrePlurielle\Bundle\FoBundle\Form\Type\EnfantType;

/**
 * Enfant controller.
 *
 */
class EnfantController extends Controller {

    /**
     * Lists all Enfant entities.
     *
     */
    public function indexAction() {
        $currentUser = $this->getLoggedUser();
        $em = $this->getDoctrine()->getManager();

//        $entities = $em->getRepository('FoBundle:Enfant')->findAll();
        $entities = $em->getRepository('FoBundle:Enfant')->getChildrenByLoggedUser($currentUser->getRealRoles(), $currentUser->getId());

        return $this->render('FoBundle:Enfant:index.html.twig', array(
                    'entities' => $entities,
        ));
    }

    /**
     * Creates a new Enfant entity.
     *
     */
    public function createAction(Request $request) {
        $entity = new Enfant();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            // upload file
            $entity->upload('');
            $em->persist($entity);
            $em->flush();

            // Flash msg
            $this->get('session')->getFlashBag()->add(
                    'success', 'Enfant ajouté avec succès!'
            );

            return $this->redirect($this->generateUrl('enfant_show', array('id' => $entity->getId())));
        }

        return $this->render('FoBundle:Enfant:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Enfant entity.
     *
     * @param Enfant $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Enfant $entity) {
        $form = $this->createForm(new EnfantType(), $entity, array(
            'action' => $this->generateUrl('enfant_create'),
            'method' => 'POST',
        ));

        // $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Enfant entity.
     *
     */
    public function newAction() {
        $usr = $this->getLoggedUser();
        $usrRole = $usr->getRealRoles();
        $entity = new Enfant();
        $form = $this->createCreateForm($entity);
        
        // Détecter le rôle d'utilisateur connecté
        if ($usrRole[0] === 'ROLE_EDUCATEUR') {
            $entity->setRefEducateur($usr);
            $form->remove('refEducateur');
        }

        if ($usrRole[0] == 'ROLE_PARENT') {
            $entity->setRefParent($usr);
            $form->remove('refParent');
        }

        return $this->render('FoBundle:Enfant:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Enfant entity.
     *
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('FoBundle:Enfant')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Enfant entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('FoBundle:Enfant:show.html.twig', array(
                    'entity' => $entity,
                    'delete_form' => $deleteForm->createView(),));
    }

    /**
     * Displays a form to edit an existing Enfant entity.
     *
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('FoBundle:Enfant')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Enfant entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('FoBundle:Enfant:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a Enfant entity.
     *
     * @param Enfant $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Enfant $entity) {
        $form = $this->createForm(new EnfantType(), $entity, array(
            'action' => $this->generateUrl('enfant_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Mettre à jour'));

        return $form;
    }

    /**
     * Edits an existing Enfant entity.
     *
     */
    public function updateAction(Request $request, $id) {
        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('FoBundle:Enfant')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Enfant entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            // upload file
            $entity->upload('');
            $em->flush();

            // Reset current enfant et currentNiveau in session
            if ($session->get('enfant')) {
                if ($entity->getId() == $session->get('enfant')->getId()) {
                    $session->remove('enfant');
                    $session->remove('currentNiveau');
                    $session->set('enfant', $entity);
                    $session->set('currentNiveau', $entity->getRefNiveau());
                }
            }

            // Flash message
            $this->get('session')->getFlashBag()->add(
                    'success', 'Modification éffectuée.'
            );
            return $this->redirect($this->generateUrl('enfant_edit', array('id' => $id)));
        }

        return $this->render('FoBundle:Enfant:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Enfant entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('FoBundle:Enfant')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Enfant entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        // Flash message
        $this->get('session')->getFlashBag()->add(
                'success', 'Suppression éffectuée.'
        );
        return $this->redirect($this->generateUrl('enfant'));
    }

    /**
     * Creates a form to delete a Enfant entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('enfant_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Supprimer'))
                        ->getForm()
        ;
    }

    /**
     * get logged user
     * @return null
     */
    private function getLoggedUser() {
        $token = $this->get('security.context')->getToken();
        if ($token->getUser()) {
            return $token->getUser();
        } else {
            return NULL;
        }
    }

}
